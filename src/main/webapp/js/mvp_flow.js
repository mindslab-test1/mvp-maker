var workingFlow = new Flow('');     // FLOW 이름 설정

let workingFlowStatus = {
    _flowUpdated: false,
    execInProg: false,
    get flowUpdated() {
        return this._flowUpdated
    },
    set flowUpdated(val) {
        if (val) {
            $('#saveFlow_btn').removeClass('disable').prop('disabled', false);
            $('#Button_MVPExec').removeClass('disable').prop('disabled', false);
            if (!this._flowUpdated) {
                $('#saveFlow_btn').on('click', onSaveFlow);
            }

        } else {
            $('#saveFlow_btn').addClass('disable').prop('disabled', true);
            $('#saveFlow_btn').off('click', onSaveFlow);
        }

        this._flowUpdated = val;
    }
};

let flow_request = null;

let title_cache = null;
let description_cache = null;

$(document).ready(function () {

    // '설정 초기화' 버튼 클릭
    $('#resetFlow').on('click', function () {
        //console.log("click remove all btn");
        workingFlow.nodeList = [];

        delete_appLinkBtn();
        $('#bot_area').empty();
        //hideFlowName();
        $('.cont_api_select').empty();
        $('.record_desc').show().html(messages.mvp_flow.record_desc);
        $('#Button_MVPExec').html("<em class=\"fas fa-project-diagram\"></em>" + messages.common.exec);
        resetEngineStatus(null);

        if (template == "ai_humanSpeech" && flow_request != null) {
            flow_request = null;
            callAppFlow("ai_humanSpeech");
        }

        if (workingFlowStatus.execInProg) {
            reqAbort = true;
            controlFlow();
        }
    });

    // '불러오기' 버튼 클릭
    $('#callFlow_btn').on('click', function () {
        // console.log("불러오기 버튼 클릭");
        delete_appLinkBtn();

        /* 임시 user_id 변수 설정 ==> 0*/
        var user_id = 0;

        //console.log(user_no);
        callFlowList(user.UserNo); // ajax call
    });

    // '저장'버튼 클릭
    $('#saveFlow_btn').on('click', onSaveFlow);

    $('#save_flow').on('click', function () {

        if (workingFlow.flowId == 0) saveNewFlow();
        else modifyUserFlow(true);
    });

    $('#cancel_saveFlow').on('click', function () {
        $('#api_save').css('display', 'none');
    });

    $('#cancel_deleteFlow').on('click', function () {
        $('#api_delete_confirm').css('display', 'none');
    });

    // pop-up close 버튼
    $('.ico_close').on('click', function () {
        $(this).parent().parent().parent().parent().css('display', 'none');
        $('.pageing').empty();
    });

    // 불러오기 후, 선택된 flow 삭제
    $('.btn_delete').on('click', function () {
        console.log("선택 flow 삭제");

        var flow_id = {};
        var arr = [];

        $('.sub_checkBox:checked').each(function () {

            var checkedFlowId = $(this).attr('id');
            var flowIdNum = parseInt(checkedFlowId.split('flow_id_')[1]);

            arr.push(flowIdNum);

        });
        flow_id.flow_id = arr;
        console.log(flow_id);

        if (flow_id.flow_id.length != 0) {

            $('#api_delete_confirm').css('display', 'block'); //삭제 확인 창

            $('#delete_confirm').on('click', function () {
                deleteFlow(flow_id);
            });
        }
    });
});

// 체크박스 전체 선택
function checkAll() {

    if ($('#check_all').is(':checked'))
        $('.sub_checkBox').prop("checked", true);
    else
        $('.sub_checkBox').prop("checked", false);
}

// 체크박스 하나 선택
function checkOne(selectCheckBox, pageNum) {
    var checkBoxId = "#flow_id_" + selectCheckBox;

    if (!$(checkBoxId).is(':checked')) // 전체 선택 후 하나라도 비활성화 시, check_all checkBox 비활성화
        $('#check_all').prop("checked", false);
    else {
        var cnt = 0;
        $('.sub_checkBox:checked').each(function () {
            cnt++;
        });

        if (cnt == pageNum) //해당 page의 flow 수와 같으면 check_all checkBox 활성화
            $('#check_all').prop('checked', true);
    }
}

// Flow 저장시
function onSaveFlow() {
    if (workingFlow.flowId <= 0) {
        showPopup_Message(messages.mvp_flow.popup_missingFlowNode);
        return;
    }
    modifyUserFlow(true);
}

/* ===================================================================================================================== */
// FLOW NODE 객체
/* ===================================================================================================================== */
function Flow(name, desc) {
    this.flowId = 0;
    this.userNo = 0;
    this.name = name;
    this.description = desc;
    this.displayOrder = -1;
    this.nodeList = new Array();
}

function FlowNode(engine_id) {
    this.flowNodeId = 0;
    this.flowId = 0;
    this.engineId = engine_id;
    this.engineName = '';
    this.paramList = new Array();
    this.inputMethod = 'flow';      // text, upload, mic, camera, flow
}

function Param(name, value) {
    this.name = name;
    this.value = value;
}

/* ===================================================================================================================== */
// FLOW 등록 관리
/* ===================================================================================================================== */
workingFlow = new Flow('engine1');

function addEngineObj(engine) {
    var flow_node = new FlowNode(engine.engineId);
    var layerId = "#Layer_Engine_Param" + (workingFlow.nodeList.length + 1);
    flow_node.inputMethod = engine.inputType;
    if (engine.engineId == 1076) {
        let selectAvatar = $(layerId + " #avatar_selected img").attr('data-id');
        let avatarParam = new Param('model', selectAvatar);
        flow_node.paramList.push(avatarParam);
        let selectBg = $(layerId + " #bg_selected img").attr('data-id');
        let avatarBgParam = new Param('model', selectBg);
        flow_node.paramList.push(avatarBgParam);
    } else {
        // param select가 있을 때
        if ($(layerId).find('.select')) {
            var selectValue = $(layerId + " .select option:selected").val();
            var paramName = $(layerId + " .select option:selected").attr('name');
            var param = new Param(paramName, selectValue);

            console.log("@ Flow.addEngineObj >> select param >> ", paramName, " = ", selectValue);
            if (typeof selectValue != 'undefined') flow_node.paramList.push(param);
        }
        // textarea가 있을 때
        if ($(layerId).children().children().hasClass('inputParam')) {
            var inputValue = $(layerId + " .inputParam").val();
            var paramName = $(layerId + " .inputParam").attr('name');

            console.log("@ Flow.addEngineObj >> textarea param");
            var param = new Param(paramName, inputValue);
            flow_node.paramList.push(param);
        }
    }
    workingFlow.nodeList.push(flow_node);
    //TODO: 내보내기 버튼 활성화
    showExportBtn();
}

// 사용자 별 저장된 flow 불러오기
function callFlowList(user_id) {
    $.ajax({
        method: 'get',
        contentType: 'application/json',
        dataType: 'json',
        url: '/mvp/api/Flows/User/' + user_id + "?lang=" + lang_code,
        timeout: TIMEOUT___MVP_API,
        success: function (result) {
            // console.log("불러오기 ========> OK");
            console.dir(result);
            console.log(user_id);

            if (result.length == 0) {
                showPopup_Message(messages.mvp_flow.popup_missingFlow);
                $('#api_loading').css('display', 'none');
                return;
            }

            $('.pageing').empty();
            $('#api_loading').css('display', 'block'); // pop-up 창 show

            // page number 추가
            var page_num = parseInt(result.length / 5);
            if (result.length % 5 != 0) page_num++;

            for (var i = 1; i <= page_num; i++) {

                var pagingTarget = "";

                if (i == 1) pagingTarget = "<a target='_self' href='#' class='prev'><em class='fas fa-chevron-left'></em></a>" +
                    "                       <a target='_self' href='#' class='api_call_paging' id='api_load_page_" + i + "'><strong>" + i + "</strong></a>";
                else pagingTarget = "<a target='_self' href='#' class='api_call_paging' id='api_load_page_" + i + "'>" + i + "</a>";

                $('#api_loading .popWrap .pop_bd .pageing').append(pagingTarget);
            }
            $('#api_loading .popWrap .pop_bd .pageing').append("<a target='_self' href='#' class='next'><em class='fas fa-chevron-right'></em>" + messages.mvp_flow.next_page + "</a>");

            // 페이지 클릭 시, 그에 따른 값 뿌려주기
            $('.api_call_paging').on('click', function () {

                $('strong').contents().unwrap(); // strong 속성 초기화
                $(this).html("<strong id='clickPage'>" + parseInt(this.id.split('api_load_page_')[1]) + "</strong>");

                $('#api_list_table').empty();

                // 페이지 제목 설정
                var initTable = "<li class='clearfix'>" +
                    "               <div class='checkbox'>" +
                    "               <input type='checkbox' id='check_all' onclick='checkAll();'>" +
                    "                   <label for='check_all'></label>" +
                    "               </div>" +
                    "               <span>" + messages.mvp_flow.saved_files + "</span>" +
                    "           </li>";
                $('#api_list_table').append(initTable);

                var start_arr_num = 5 * (parseInt(this.id.split('api_load_page_')[1]) - 1);  //가져올 arr 시작 위치 파악
                var last_arr_num = 5;

                //console.log(start_arr_num  + " *********** " + last_arr_num);

                // 처음이나 마지막 페이지 클릭 시, append하는 table 갯수 수정
                if ((parseInt(this.id.split('api_load_page_')[1]) == 1) && (result.length < 5))   // 첫 페이지
                    last_arr_num = result.length;
                else if ((parseInt(this.id.split('api_load_page_')[1]) == page_num) && (result.length % 5 != 0))   // 마지막 페이지
                    last_arr_num = (result.length % 5);


                //console.log("--------> start_arr_num = ", start_arr_num, " >> last_arr_num = ", last_arr_num, " >> length = ", result.length);

                for (var i = 0; i < last_arr_num; i++) {
                    //console.log("---------------> i = ", i);

                    var engine_list = ""; // engineId--> engineName으로 수정 필요!
                    for (var j = 0; j < result[start_arr_num + i].nodeList.length; j++) {
                        engine_list = engine_list + result[start_arr_num + i].nodeList[j].engineName + " - ";
                    }
                    engine_list = engine_list.substring(0, engine_list.length - 2);

                    var listContents = "<li>" +
                        "                   <div class='checkbox'>" +
                        "                       <input type='checkbox' id='flow_id_" + result[start_arr_num + i].flowId + "' class='sub_checkBox' onclick='checkOne(" + result[start_arr_num + i].flowId + "," + last_arr_num + ");'>" +
                        "                       <label for='flow_id_" + result[start_arr_num + i].flowId + "'></label>" +
                        "                   </div>" +
                        "                   <div class='list_desc'>" +
                        "                       <a href='javascript:callFlow(" + result[start_arr_num + i].flowId + ")' style='display: block;'>" +
                        "                           <h5>" + result[start_arr_num + i].name + "</h5>" +
                        "                           <p>" + engine_list + "</p> " +
                        "                       </a>" +
                        "                   </div>" +
                        "               </li>";

                    $('#api_list_table').append(listContents);
                }
            });

            // paging prev btn
            $('.prev').on('click', function () {
                //console.log('prev click!! ');
                var presentPageNum = parseInt($('#clickPage').text());

                if (presentPageNum != 1) {
                    var loadingPageId = "#api_load_page_" + (presentPageNum - 1);
                    $(loadingPageId).trigger('click');
                }
            });

            // paging next btn
            $('.next').on('click', function () {
                //console.log('next click!! ');
                var presentPageNum = parseInt($('#clickPage').text());

                if (presentPageNum != page_num) {
                    var loadingPageId = "#api_load_page_" + (presentPageNum + 1);
                    $(loadingPageId).trigger('click');
                }
            });

            $('#api_load_page_1').trigger('click'); //첫 페이지 목록 자동 loading
        },
        error: function (error) {
            // console.log("불러오기 ========> Fail");
            console.log(error.toString());

            alert(messages.mvp_flow.server_connectionError);
        }
    });
}

// 선택한 flow 목록 보여주기
function callFlow(flow_Id) {
    if (flow_request == null) {
        $('#api_loading').css('display', 'none');
        $('.pageing').empty();
        $('#resetFlow').trigger('click');

        // 일단은 engineId로 call
        flow_request = $.ajax({
            type: "get",
            contentType: 'application/json',
            dataType: 'json',
            url: '/mvp/api/Flows/' + parseInt(flow_Id) + "?lang=" + lang_code,
            timeout: TIMEOUT___MVP_API,
            async: false,
            success: function (result) {
                // console.log("선택 flow 조회 ========> OK");
                console.dir(result);
                console.log(engineList);
                for (var i = 0; i < result.nodeList.length; i++) {

                    var engine = engineList[result.nodeList[i].engineId];
                    bot_setting(engine);
                    if (engineIsLogicBox(engine)) { // 엔진이 로직박스인 경우에만 로직박스 요청 팝업들을 설정해준다
                        logicBoxPopupSetting();
                    }

                    for (var j = 0; j < result.nodeList[i].paramList.length; j++) {

                        //일치시켜야 하는 값 : result.nodeList[i].paramList[j].value
                        // groupParamList
                        for (var k = 0; k < engine.groupParamList.length; k++) {
                            for (var l = 0; l < engine.groupParamList[k].valueList.length; l++) {

                                if (engine.groupParamList[k].valueList[l].name == result.nodeList[i].paramList[j].value)
                                    $('option[value="' + result.nodeList[i].paramList[j].value + '"]').prop('selected', true);
                                else
                                    $('option[value="' + result.nodeList[i].paramList[j].value + '"]').prop('selected', false);
                            }
                        }

                        // paramList
                        for (var k = 0; k < engine.paramList.length; k++) {
                            if (engine.paramList[k].type == 'text') { // paramList array 중 type이 text인 경우,
                                $('.inputParam').attr('value', result.nodeList[i].paramList[j].value); // 신규 node면 default값을, 아니면 저장된 값을 불러온다
                            } else if (engine.paramList[k].type == 'enum') { // paramList array 중 type이 enum인 경우,
                                $('option[value="' + result.nodeList[i].paramList[j].value + '"]').prop('selected', true);
                            }
                        }
                        if (engine.engineId === 1076) {
                            result.nodeList.forEach((v, idx) => {
                                if (v.engineId === 1076) {
                                    let avatar = engine.paramList[0].data[0];
                                    engine.paramList[0].data.forEach((e) => {
                                        if (e.id.toString() == v.paramList[0].value) avatar = e;
                                    });
                                    $('avatar_list').children().removeClass("active");
                                    $('avatar_list').find("[data-avatar-id='" + v.paramList[0].value + "']").addClass('active');
                                    $('#avatar_selected').empty();
                                    $('#avatar_selected').append(`<img src="${avatar.thumbnailUrl}" width="20" height="20" alt="${avatar.displayName}" data-id="${avatar.id}" data-value="${avatar.model}" data-previewUrl="${avatar.previewUrl}" data-purchased="${avatar.purchased}">${lang_code == 'ko' ? avatar.displayName : avatar.displayNameEn}`);

                                    let bg = engine.paramList[1].data[0][0];
                                    engine.paramList[1].data[0].forEach((e) => {
                                        if (e.id.toString() == v.paramList[1].value) bg = e;
                                    });
                                    $('#bg_list').children().removeClass("active");
                                    $('#bg_list').find("[data-bg-id='" + v.paramList[1].value + "']").addClass('active')
                                    $('#bg_selected').empty();
                                    let bgUrl = bg.url;
                                    let bgWaterMark = bg.waterMarkUrl;
                                    if (bgUrl === "" || bgUrl === undefined) bgUrl = "/image/image.svg"
                                    $('#bg_selected').append(`<img src="${bgUrl}" width="20" height="20" alt="${lang_code == 'ko' ? bg.displayName : bg.displayNameEn}" data-id="${bg.id}" data-value="${bgUrl}" data-watermark="${bgWaterMark}">${lang_code == 'ko' ? bg.displayName : bg.displayNameEn}`);
                                }
                            })
                        }
                        // console.log(    $('.select option:selected').val()   );
                    }

                    output_set(engine); // 불러 온 엔진의 responseType에 따른 이미지 세팅
                    $('.start_btn').trigger('click');
                    resetEngineStatus(engine);
                }

                if (result.nodeList.length > 0 && null != result.nodeList[0].engineId) {
                    input_set(engineList[result.nodeList[0].engineId]); // 첫번째 엔진 input 세팅
                }

                workingFlow = result;
                workingFlowStatus.flowUpdated = false;

                if (workingFlow.userNo == 0) { // application flow를 불러왔을 때, 사용자가 flow를 신규 등록인 것 처럼 저장 및 수정하도록 설정
                    workingFlow.flowId = 0;
                }

                $('.pjt_infoBox').addClass('active');

                if (template == 'ai_humanSpeech' && workingFlow.flowId == 0) {
                    workingFlow.flowId = flowId;
                    workingFlow.templateId = 2;
                    workingFlowStatus.flowUpdated = true;
                    workingFlow.name = projectName;
                }

                if (workingFlow.name != null && workingFlow.name != "") {
                    $('.mvp_box .tblBox .tool_box .pjt_infoBox .tit').text(workingFlow.name);
                    title_cache = workingFlow.name;
                } else if (title_cache != null) {
                    $('.mvp_box .tblBox .tool_box .pjt_infoBox .tit').text(title_cache);
                } else {
                    $('.mvp_box .tblBox .tool_box .pjt_infoBox .tit').text('');
                }

                if (workingFlow.description != null && workingFlow.description != "") {
                    $('.mvp_box .tblBox .tool_box .pjt_infoBox .txt').text(workingFlow.description);
                    description_cache = workingFlow.description;
                } else if (description_cache != null) {
                    $('.mvp_box .tblBox .tool_box .pjt_infoBox .txt').text(description_cache);
                } else {
                    $('.mvp_box .tblBox .tool_box .pjt_infoBox .txt').text('');
                }

                // showPopup_Message('[' + workingFlow.name + '] ' + messages.mvp_flow.popup_loaded);

                if (workingFlow.flowId > 0) {
                    $('#saveFlow_btn').css('display', 'block');
                } else {
                    $('#saveFlow_btn').css('display', 'none');
                }
            },
            error: function (error) {
                // console.log("불러오기 ========> Fail");
                console.log(error.toString());

                alert(messages.mvp_flow.server_connectionError);
            },
            complete: function () {
                flow_request = null;
            }
        });
    }
    showExportBtn();
}

function showExportPopup() {
    $('#scenario_output_box').show();
}

function showExportBtn() {
    // 내보내기 버튼 노출
    if (workingFlow.nodeList.length === 3 &&
        workingFlow.nodeList[0].engineId === 2 &&
        workingFlow.nodeList[1].engineId === 1075 &&
        workingFlow.nodeList[2].engineId === 1076) {
        // 특정 엔진 조합일 경우만 노출
        if ($("#linkBtn_ai_humanSpeech").length < 1) {
            $('.stn_board').append('<button id="linkBtn_ai_humanSpeech" class="blue_link_btn blue_link_big_btn btn_export btn_modal_open" data-popup="scenario_output_box">'
                + messages.mvp_flow.appLink_button_aihuman + '</button>');
        }
        $("#linkBtn_ai_humanSpeech").show();
    } else {
        $("#linkBtn_ai_humanSpeech").hide();
    }
}

// 정해져 있는 application flow 불러오기
function callAppFlow(appName) {
    //console.log(appName);

    $.ajax({
        type: "get",
        url: '/mvp/api/Flows/appFlow/' + appName,
        contentType: "application/json",
        timeout: TIMEOUT___MVP_API,
        dataType: 'json',
        async: false,
        success: function (result) {
            console.log("flowId = " + result);
            callFlow(result);

            currentProjectIndex = -1;
            create_appLinkBtn(appName);
        },
        error: function (error) {
            console.log(error.toString());

            alert(messages.mvp_flow.server_connectionError);
        }
    });
    showExportBtn();
}

// 사용자의 flow 수정
function modifyUserFlow(showPopup) {
    console.log(workingFlow);
    workingFlow.name = $('#api_name').val();

    $.ajax({
        type: 'put',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(workingFlow),
        url: '/mvp/api/Flows/' + workingFlow.flowId,
        timeout: TIMEOUT___MVP_API,
        success: function (result) {
            console.log("기존 flow 변경 ========> OK");
            console.dir(result);

            $('#api_save .ico_close').trigger('click');

            /* 업데이트 정보 반영된 객체로 변경 */
            workingFlow = result;
            workingFlowStatus.flowUpdated = false;
            if (showPopup) {
                showPopup_Message(messages.mvp_flow.popup_saved);
            }
        },
        error: function (error) {
            console.log("========> Fail");
            console.log(error.toString());
            showPopup_Message(messages.mvp_flow.popup_save_failed);
        }
    });
}

// 사용자의 새로운 flow 저장
function saveNewFlow() {
    workingFlow.name = $('#api_name').val();

    $.ajax({
        type: 'post',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(workingFlow),
        url: '/mvp/api/Flows',
        timeout: TIMEOUT___MVP_API,
        success: function (result) {
            console.log("새로운 flow 저장 ========> OK");
            console.dir(result);

            $('#close_save_pop').trigger('click');

            /* 업데이트 정보 반영된 객체로 변경 */
            workingFlow = result;
            showFlowName();
        },
        error: function (error) {
            console.log("========> Fail");
            console.log(error.toString());
        }
    });
}

// 선택된 flow 삭제
function deleteFlow(flow_id) {
    //console.log(JSON.stringify(flow_id));

    $('.btn_delete').css('display', 'none');

    $.ajax({
        type: 'delete',
        url: '/mvp/api/Flows/delete/',
        contentType: "application/json",
        data: JSON.stringify(flow_id),
        timeout: TIMEOUT___MVP_API,
        success: function (result) {
            // console.log(result);

            $('#api_delete_confirm').css('display', 'none');
            callFlowList(user_no);
        },
        error: function (error) {
            console.log("========> Fail");
            console.log(error.toString());
        },
        complete: function () {
            $('.btn_delete').css('display', 'block');
        }
    });
}

function addFlowNode(engine) {
    bot_setting(engine);
    if (engineIsLogicBox(engine)) { // 추가된 node가 로직박스인 경우 그에대한 팝업도 설정
        logicBoxPopupSetting();
    }
    if (noParamRequired(engine)) {
        $('.start_btn').trigger('click');
    }
}

var flowParamBackground;

// BuilderBackground 목록 가져오기
function callBuilderBackground() {
    $.ajax({
        method: 'post',
        url: '/builder/background',
        async: true,
        success: function (result) {
            flowParamBackground = result;
        },
        error: function (error) {
            // console.log("불러오기 ========> Fail");
            console.log(error.toString());

            // alert(messages.mvp_flow.server_connectionError);
        }
    });
}

/* ===================================================================================================================== */
// FLOW UI 관리
/* ===================================================================================================================== */

// 봇 세팅
function bot_setting(engine) {
    console.group("bot_setting() : " + engine.name);
    console.dir(engine);
    console.groupEnd();

    var engineGrpId = engine.groupId;
    var inputType = engine.inputType;
    var outputType = engine.outputType;

    if (currentWorkingIndex === workingFlow.nodeList.length) { // 실행 완료 후 새 엔진 추가시, 다운로드 태그 제거
        $('#FlowNode_Output_' + (currentWorkingIndex - 1) + ' a.btn_dwn').remove();
    }

    var out_leng = $(".output").length;
    var inout_set = $(".inout_set").length;
    if (inout_set > 0 && out_leng != inout_set) {

        showPopup_Message(messages.mvp_flow.popup_selectModel);

    } else {
        // 엔진
        var ui_fmt___engine =
            "<div class='api_set clearfix inout_set'>\n" +
            "   <div class='api_show' id='FlowNode_Engine_{0}'>\n" +
            "       <div class='api_bot' id='{4}'>\n" +
            "           <span id='apibot_name'> {1} </span>\n" +
            "                <a href='{5}' class='btn_go_api' title='{2}: Cloud API'>" +
            "                     <img src='{2}' title='{3}'>\n" +
            "                </a>" +
            "           <div class='spinner'><strong class=\"fas fa-spinner\"></strong></div>\n" +
            "       </div>\n" +
            "   </div>\n" +
            "</div>\n";
        $('#bot_area').append(formatString(ui_fmt___engine, workingFlow.nodeList.length, engine.name, engine.iconPath, engine.desc, engine.engineId, null));
        // $('#bot_area').append( formatString(ui_fmt___engine, workingFlow.nodeList.length, engine.name, engine.iconPath, engine.desc, engine.engineId, engine.docUrl));


        let paramIndex = workingFlow.nodeList.length + 1;
        // 파라메타 LAYER UI
        var ui_fmt___param_layer =
            "<div class='api_select' id='Layer_Engine_Param{0}'>\n" +
            "   <div class='api_tit'>\n" +
            "       <span> {1} <em class='fas fa-angle-double-right'></em></span>\n" +
            "       <h5> {2} </h5> \n" +
            "       <span><em class='fas fa-angle-double-right'></em>{3}</span>\n" +
            "   </div>\n" +
            "   <div class='Layer_innerParam'>\n" + addEngineOpt(engine) +
            "   </div>\n" +
            // DANIEL: showEngOptButtons --> engine.paramList.length가 0이거나 engine.groupId가 5인 경우 'start_btn'에 style='display: none'을 추가해서 숨김
            "   <a class='start_btn' " + hideStartBtn(engine) + ">" + messages.mvp_flow.selection_complete + "</a>" +
            "   <a class='edit_btn' style='display: none'>" + messages.mvp_flow.update + "</a> \n" +
            "   <a href='#pop_medel_add' onclick='openModelRequest(this, \"{2}\");' class='medel_add btn_lyr_open'>" + messages.mvp_flow.model_request + "</a> \n" +
            "</div>\n";

        var ui_fmt___param_layer_scenario =
            "<div class='api_select' id='Layer_Engine_Param{0}'>\n" +
            "   <div class='api_tit'>\n" +
            "       <span> {1} <em class='fas fa-angle-double-right'></em></span>\n" +
            "       <h5> {2} </h5> \n" +
            "       <span><em class='fas fa-angle-double-right'></em>{3}</span>\n" +
            "   </div>\n" +
            "   <div class='Layer_innerParam'>\n" + addEngineOpt(engine) +
            "   </div>\n" +
            // DANIEL: showEngOptButtons --> engine.paramList.length가 0이거나 engine.groupId가 5인 경우 'start_btn'에 style='display: none'을 추가해서 숨김
            "   <a class='start_btn' " + hideStartBtn(engine) + ">" + messages.mvp_flow.selection_complete + "</a><a class='edit_btn' style='display: none'>" + messages.mvp_flow.update + "</a> \n" +
            "   <a href='https://fast-aicc.maum.ai/userChatbotBuilder' target='_blank' rel='noopener noreferrer' class='medel_add btn_lyr_open'>" + messages.mvp_flow.request_scenario + "</a> \n" +
            "</div>\n";

        // 파라메타 LAYER UI for Logic Box Only
        var ui_fmt___param_layer_logic_box =
            "<div class='api_select logic_select' id='Layer_Engine_Param{0}'>\n" +
            "   <div class='api_tit'>\n" +
            "       <span> {1} <em class='fas fa-angle-double-right'></em></span>\n" +
            "       <h5> {2} </h5> \n" +
            "       <span><em class='fas fa-angle-double-right'></em>{3}</span>\n" +
            "   </div>\n" +
            "   <div class='Layer_innerParam'>\n" + addEngineOpt(engine) +
            "   </div>\n" +
            "   <a href='#pop_request_add{0}' onclick='openLogicBoxRequestForm(this," + "{0}" + ");' class='request_add btn_lyr_open logic_btn'>" + messages.mvp_flow.request_info + "</a> \n" +
            "   <a href='#pop_response_add{0}' onclick='openLogicBoxResponseForm(this," + "{0}" + ");' class='response_add btn_lyr_open logic_btn'>" + messages.mvp_flow.response_info + "</a> \n" +
            "   <a class='start_btn'>" + messages.mvp_flow.selection_complete + "</a><a class='edit_btn' style='display: none'>" + messages.mvp_flow.update + "</a> \n" +
            "   <div class='arrow_btn' onclick='arrow(" + "{0}" + ")'><img src='/image/img_down.svg' alt='arrow'></div>\n" +
            "</div>\n";

        if (engineIsLogicBox(engine)) // 엔진이 로직박스인 경우에만 parameter UI를 다르게 설정
            $('.cont_api_select').append(formatString(ui_fmt___param_layer_logic_box, paramIndex, inputType, engine.name, outputType));
        else if (engineIsScenario(engine))
            $('.cont_api_select').append(formatString(ui_fmt___param_layer_scenario, paramIndex, inputType, engine.name, outputType));
        else
            $('.cont_api_select').append(formatString(ui_fmt___param_layer, paramIndex, inputType, engine.name, outputType));

        // Scale UI of model param box
        if ($('.mvp_contents .stn_select').find('.api_tit h5').height() >= 40) {
            $('.mvp_contents .stn_select').find('.api_select .Layer_innerParam p').css("margin-top", "15px")
        }

        // 속성 설정
        $('#Layer_Engine_Param' + paramIndex + ' div h5').css('color', '#' + engineGrpList[engineGrpId - 1].color);
        $('#Layer_Engine_Param' + paramIndex + ' .start_btn').css('background', '#' + engineGrpList[engineGrpId - 1].color);
        $('#Layer_Engine_Param' + paramIndex + ' .medel_add').css('background', '#' + engineGrpList[engineGrpId - 1].color);

        // 선택 완료 click event 등록
        $('#Layer_Engine_Param' + paramIndex + ' .start_btn').on('click', function () {
            var out_leng = $(".output").length;
            var inout_set = $(".inout_set").length;
            if (out_leng != inout_set) {
                if (inout_set <= 1) inoutBox_set(engine);
                else if (inout_set > 1) output_set(engine);
            }

            var select_engine_index = parseInt($(this).parent().attr('id').split('Layer_Engine_Param')[1]);
            // console.log("select_engine_index : " + select_engine_index + " / workingFlow.nodeList.length = " + workingFlow.nodeList.length);

            if (select_engine_index <= workingFlow.nodeList.length)   // 기존 엔진 수정
                modifyEngineOpt(select_engine_index, engine);
            else                                                     // 새로운 엔진 추가
                addEngineObj(engine);

            $(this).hide();
            // DANIEL: Parameter가 없는경우 edit_btn 을 reveal하지 못하게 condition
            if (!noParamRequired(engine)) {
                $(this).next().fadeIn();
            }


            $('.Layer_innerParam select').prop('disabled', true);

            let divId = $(this).parent().attr('id');
            $('#' + divId + ' select').prop('disabled', true);
            $('#' + divId + ' input').prop('disabled', true);

            // 모든 조건이 "선택완료된" 경우 실행버튼 활성화
            if (!$('.start_btn').is(':visible')) {
                workingFlowStatus.flowUpdated = true;
                if (workingFlow.flowId > 0) {
                    $('#saveFlow_btn').css('display', 'block');
                } else {
                    $('#saveFlow_btn').css('display', 'none');
                }
            }
        });

        //수정 버튼 click event 등록
        $('#Layer_Engine_Param' + paramIndex + ' .edit_btn').on('click', function () {
            console.log(paramIndex);
            $(this).hide();
            $(this).parent().children('.start_btn').fadeIn();
            $('.Layer_innerParam select').prop('disabled', false);

            let divId = $(this).parent().attr('id');
            $('#' + divId + ' select').prop('disabled', false);
            $('#' + divId + ' input').prop('disabled', false);
            $('#Button_MVPExec').addClass('disable').prop('disabled', true);
            $('#saveFlow_btn').addClass('disable').prop('disabled', true);
            if (workingFlow.flowId > 0) {
                $('#saveFlow_btn').css('display', 'block');
            } else {
                $('#saveFlow_btn').css('display', 'none');
            }
        });
        if(paramIndex > 1){
            $('#avatar_list > li').on('click', function (e) {
                let avatarId = $(this).attr("data-avatar-id");
                workingFlow.nodeList[paramIndex - 1].paramList[0].value = avatarId;
            })
            $('#bg_list > li').on('click', function (e) {
                let bgId = $(this).attr("data-bg-id")
                workingFlow.nodeList[paramIndex - 1].paramList[1].value = bgId;
            })
        }

        resetEngineStatus(engine);
    }

    close_btn();

}

function modifyEngineOpt(engine_index, engine) {
    var paramList_length = workingFlow.nodeList[engine_index - 1].paramList.length;

    for (var i = 0; i < paramList_length; i++) {

        //groupParamList
        for (var j = 0; j < engine.groupParamList.length; j++) {
            for (var k = 0; k < engine.groupParamList[j].valueList.length; k++) {

                if (engine.groupParamList[j].name == workingFlow.nodeList[engine_index - 1].paramList[i].name) {
                    // console.log(engine.groupParamList[j].name);
                    // console.log(workingFlow.nodeList[engine_index - 1].paramList[i].name);

                    workingFlow.nodeList[engine_index - 1].paramList[i].value = $("#Layer_Engine_Param" + engine_index + " .select option:selected").val();
                }
            }
        }

        //paramList - enum, text
        for (var j = 0; j < engine.paramList.length; j++) {

            if (engine.paramList[j].type == "enum") {
                if (engine.paramList[j].name == workingFlow.nodeList[engine_index - 1].paramList[i].name)
                    workingFlow.nodeList[engine_index - 1].paramList[i].value = $("#Layer_Engine_Param" + engine_index + " .select option:selected").val();
            } else if (engine.paramList[j].type == "text") {
                if (engine.paramList[j].name == workingFlow.nodeList[engine_index - 1].paramList[i].name)
                    workingFlow.nodeList[engine_index - 1].paramList[i].value = $("#Layer_Engine_Param" + engine_index + " .Layer_innerParam .inputParam").val();
            }
        }
    }

    workingFlowStatus.flowUpdated = true;
}

// 각 engine별 param option 추가
function addEngineOpt(engine) {
    var engine_opt = "";

    // groupParamList 추가
    for (var i = 0; i < engine.groupParamList.length; i++) {

        engine_opt = engine_opt + "<select class='select'>";
        //console.log(engine.groupParamList.length);

        var param = engine.groupParamList[i];

        for (var j = 0; j < param.valueList.length; j++) {

            var value = param.valueList[j];
            //console.log(value);

            if (j == 0) engine_opt = engine_opt + "<option selected name='" + param.name + "' value='" + value.name + "'>" + value.name + "</option>";
            else engine_opt = engine_opt + "<option name='" + param.name + "' value='" + value.name + "'>" + value.name + "</option>";
        }
        engine_opt = engine_opt + "</select>";
    }

    // paramList 추가
    for (var i = 0; i < engine.paramList.length; i++) {
        var param = engine.paramList[i];

        // display 값이 0인 경우에는 별도의 입력을 받지 않는다
        if (param.display == 0 || engine.groupId == 5) continue;

        if (param.type == 'text') { // paramList array 중 type이 text인 경우,
            engine_opt = engine_opt + "<input type='text' class='inputParam' name='" + param.name + "' value='" + param.defaultValue + "' placeholder='" + param.displayName + "'/>";
        } else if (param.type == 'enum') { // paramList array 중 type이 enum인 경우,

            engine_opt = engine_opt + "<select class='select'>";

            var cnt = param.enumValue.toString().split('|').length;
            // console.log(cnt);

            for (var j = 0; j < cnt; j++) {
                if (j == 0) {
                    engine_opt = engine_opt + '<option selected name="' + param.name + '" value="' + param.enumValue.toString().split("|")[j] + '">' + param.displayEnumValue.toString().split("|")[j] + '</option>';////////////////////////////////selected
                } else {
                    engine_opt = engine_opt + '<option name="' + param.name + '" value="' + param.enumValue.toString().split("|")[j] + '">' + param.displayEnumValue.toString().split("|")[j] + '</option>';
                }
            }
            engine_opt = engine_opt + "</select>";

        } else if (param.type == 'enum/image') { // paramList array 중 type이 enum/image인 경우,
            if (i == 0) {
                engine_opt = engine_opt + '<div class="dual_lipsync_box">\n';
            }
            // BACKGROUND
            if (param.engineId === 1076 && param.name === "image") {
                let bg = param.data[0];
                let initBgSrc = bg[0].url;
                if (initBgSrc === "" || initBgSrc === undefined) initBgSrc = "/image/image.svg"
                engine_opt += `   
                    <div class="select_list" onclick="displayDropDown(this)">
                    <p id="bg_selected" class="select">
                        <img src="${initBgSrc}" width="20" height="20" alt="${bg[0].displayName}" data-id="${bg[0].id}" data-value="${bg[0].url}" data-watermark="${bg[0].waterMarkUrl}">
                        ${lang_code == 'ko' ? bg[0].displayName : bg[0].displayNameEn}
                    </p>
                    <ul id="bg_list" class="list_box">
                `;
                for (var j = 0; j < bg.length; j++) {
                    let bgSrc = bg[j].url;
                    if (bgSrc === "" || bgSrc === undefined) bgSrc = "/image/image.svg"
                    engine_opt += `<li class="active" data-bg-id="${bg[j].id}" data-value="${bg[j].url}" ><img src="${bgSrc}" width="20" height="20" alt="${lang_code == 'ko' ? bg[j].displayName : bg[j].displayNameEn}" data-id="${bg[j].id}" data-value="${bg[j].url}" data-watermark="${bg[j].waterMarkUrl}">${lang_code == 'ko' ? bg[j].displayName : bg[j].displayNameEn}</li>`;

                }
                engine_opt = engine_opt + '</ul>\n' + '</div>\n';
            } else if (param.engineId === 1076 && param.name === "model") {
                let avatar = param.data;
                engine_opt += `   
                    <div class="select_list" onclick="displayDropDown(this)">
                    <p id="avatar_selected" class="select">
                        <img src="${avatar[0].thumbnailUrl}" width="20" height="20" alt="${avatar[0].displayName}" data-id="${avatar[0].id}" data-value="${avatar[0].model}" data-previewUrl="${avatar[0].previewUrl}"  data-purchased="${avatar[0].purchased}">
                        ${lang_code == 'ko' ? avatar[0].displayName : avatar[0].displayNameEn}
                    </p>
                    <ul id="avatar_list" class="list_box">
                `;
                for (var j = 0; j < avatar.length; j++) {
                    let avatarSrc = avatar[j].thumbnailUrl;
                    if (avatarSrc === "" || avatarSrc === undefined) avatarSrc = "/image/image.svg"
                    engine_opt += `<li class="active" data-avatar-id="${avatar[j].id}" data-value="${avatar[j].model}"><img src="${avatarSrc}" width="20" height="20" alt="${lang_code == 'ko' ? avatar[j].displayName : avatar[j].displayNameEn}"  data-id="${avatar[j].id}" data-value="${avatar[j].model}" data-previewUrl="${avatar[j].previewUrl}" data-purchased="${avatar[j].purchased}">${lang_code == 'ko' ? avatar[j].displayName : avatar[j].displayNameEn}</li>`;
                }
                engine_opt = engine_opt + '</ul>\n' + '</div>\n';
            } else {
                var cnt = param.enumValue.toString().split('|').length;

                engine_opt = engine_opt + '   <div class="select_list" onclick="displayDropDown(this)">\n' +
                    '       <p class="select"><img src="/image/lipsyncAvatar/' + param.enumValue.toString().split('|')[0] + '" width="20" height="20" alt=' + param.enumValue.toString().split('|')[0] + '>' + param.displayEnumValue.toString().split('|')[0] + '</p>\n' +
                    '       <ul class="list_box">\n'
                for (var j = 0; j < cnt; j++) {
                    if (j == 0) {
                        engine_opt = engine_opt +
                            '<li class="active"><img src="/image/lipsyncAvatar/' + param.enumValue.toString().split('|')[j] + '" width="20" height="20" alt=' + param.enumValue.toString().split('|')[j] + '>' + param.displayEnumValue.toString().split('|')[j] + '</li>\n';
                    } else {
                        engine_opt = engine_opt + '<li><img src="/image/lipsyncAvatar/' + param.enumValue.toString().split('|')[j] + '" width="20" height="20" alt=' + param.enumValue.toString().split('|')[j] + '>' + param.displayEnumValue.toString().split('|')[j] + '</li>\n';
                    }
                }
                engine_opt = engine_opt + '</ul>\n' + '</div>\n';
            }
            if (i == 0) {
                engine_opt = engine_opt = engine_opt + "<a onclick='openAvatarPreview(this)' class='btn_showing_avatar' height='31px'>" + messages.mvp_flow.preview + "</a> </div>";
            }

        }
    }

    if (engine_opt == "") {
        engine_opt = engine_opt + messages.mvp_flow.engine_noParamError;
    }

    return engine_opt;
}

/* YEJI: Add Dropdown function to Avatar param options */
function displayDropDown(obj) {
    if ($($(obj).parent().parent().parent().children('a.start_btn')).css('display') !== 'none' && $($(obj).parent().parent().children('a.start_btn')).css('display') !== 'none') {

        $($(obj).children('.list_box')).slideToggle("fast");

        // if outside of dropdown clicked, close dropdown
        $(document).on("click", function (event) {
            if ($(obj) !== event.target && !$(obj).has(event.target).length) {
                $($(obj).children('.list_box')).slideUp("fast");
                $(this).off(event)
            }
        });

        $(".list_box li").click(function () {
            $(this).addClass('active').siblings().removeClass('active');
            $($(this).parent().parent().children('.select')).html($(this).html());
        });


    }
}

function noParamRequired(engine) {
    return (engine.paramList.length === 0 || engine.groupId === 5);
}

function hideStartBtn(engine) {
    if (noParamRequired(engine)) {
        return " style='visibility: hidden; display: none;'";
    } else {
        return "";
    }
}

//처음 api 옵션 선택 했을 때
function inoutBox_set(engine) {
    let inputImg = engine.inputType;
    let inputTxt = engine.inputType;
    let outputType = engine.outputType;
    let engineGroup = engine.groupId;
    console.log(engineGroup);
    if (engineGroup == "1") {
        engineGroup = "purple"
    } else if (engineGroup == "2") {
        engineGroup = "pink"
    } else if (engineGroup == "3") {
        engineGroup = "green"
    } else if (engineGroup == "4") {
        engineGroup = "blue"
    } else if (engineGroup == "5") {
        engineGroup = "orange"
    } else if (engineGroup == "6") {
        engineGroup = "navy"
    }

    // TODO : 임시로 넣은 것! 제거하려면 변수 inputImg 와 inputTxt를 inputType 하나로만 쓰기
    if (inputImg === "none") {
        $('.inout_set').append(' \
                <div class="output_show" id="FlowNode_Output_' + workingFlow.nodeList.length + '">\
                    <div class="output ' + outputType + 'box ' + engineGroup + '"><span>' + messages.mvp_flow.result_value + '</span>\
                        <p>\
                            ' + outputType + '\
                        </p>\
                    </div>\
                </div>');
        return;
    }

    $('.inout_set').prepend(' \
                <div class="input_show" id="FlowNode_Input_' + workingFlow.nodeList.length + '">\
                    <div class="input ' + inputImg + 'box ' + engineGroup + ' "><span>' + messages.mvp_flow.input_value + '</span>\
                        <p>\
                            ' + inputTxt + '\
                        </p>\
                    </div>\
                </div>');
    $('.inout_set').append(' \
                <div class="output_show" id="FlowNode_Output_' + workingFlow.nodeList.length + '">\
                    <div class="output ' + outputType + 'box ' + engineGroup + '"><span>' + messages.mvp_flow.result_value + '</span>\
                        <p>\
                            ' + outputType + '\
                        </p>\
                    </div>\
                </div>');
}

//두번째 이후 api 옵션 선택 했을 때
function output_set(engine) {
    var outputType = engine.outputType;
    var engineGroup = engine.groupId;

    if (engineGroup == "1") {
        engineGroup = "purple"
    } else if (engineGroup == "2") {
        engineGroup = "pink"
    } else if (engineGroup == "3") {
        engineGroup = "green"
    } else if (engineGroup == "4") {
        engineGroup = "blue"
    } else if (engineGroup == "5") {
        engineGroup = "orange"
    } else if (engineGroup == "6") {
        engineGroup = "navy"
    }

    $('.api_set:last').append(' \
            <div class="output_show" id="FlowNode_Output_' + workingFlow.nodeList.length + '">\
                <div class="output ' + outputType + 'box ' + engineGroup + '"><span>' + messages.mvp_flow.result_value + '</span>\
                    <p>\
                    ' + outputType + '\
                </p>\
                </div>\
            </div>');
}

// 저장 목록 불러온 경우 engine input 세팅
function input_set(engine) {
    let inputType = engine.inputType;
    let engineGroup = engine.groupId;

    if (engineGroup == "1") {
        engineGroup = "purple"
    } else if (engineGroup == "2") {
        engineGroup = "blue"
    } else if (engineGroup == "3") {
        engineGroup = "green"
    } else if (engineGroup == "4") {
        engineGroup = "pink"
    } else if (engineGroup == "5") {
        engineGroup = "orange"
    } else if (engineGroup == "6") {
        engineGroup = "navy"
    }

    $('.inout_set:first').prepend(' \
            <div class="input_show" id="FlowNode_Input_0">\
                <div class="input ' + inputType + 'box ' + engineGroup + ' "><span>' + messages.mvp_flow.input_value + '</span>\
                    <p>\
                        ' + inputType + '\
                    </p>\
                </div>\
            </div>');
}

/*
** 정해져 있는 application flow call 이후, URL 버튼 생성 및 삭제
*/
function create_appLinkBtn(appName) {
    delete_appLinkBtn();

    if (appName == 'maum_minutes') {
        $('.stn_board').append('<a id="linkBtn_maumMinutes" class="blue_link_btn" href="https://minutes.maum.ai/" target="_blank">' + messages.mvp_flow.appLink_button_minutes + '</a>');
    } else if (appName == 'fast_ai') {
        $('.stn_board').append('<a id="linkBtn_fastAI" class="blue_link_btn" href="https://fast-aicc.maum.ai/" target="_blank">' + messages.mvp_flow.appLink_button_fast + '</a>');
    } else if (appName == 'ai_humanSpeech') {
        $('.stn_board .btn_export').on('click', function () {
            if ($($('.start_btn')[0]).css('display') !== 'none' || $($('.start_btn')[1]).css('display') !== 'none' || $($('.start_btn')[2]).css('display') !== 'none') {
                showPopup_Message(messages.mvp_flow.popup_selectModel);
            } else {
                generateExportCode();
                $('#scenario_output_box').show();
                $("#scenario_output_box .blue_btn").on('click', copyExportCode)
            }
        })
    } else if (appName == 'ai_humanText') {
        $('.stn_board .btn_export').on('click', function () {
            if ($($('.start_btn')[0]).css('display') !== 'none' || $($('.start_btn')[1]).css('display') !== 'none') {
                showPopup_Message(messages.mvp_flow.popup_selectModel);
            } else {
                generateExportCode();
                $('#scenario_output_box').show();
                $("#scenario_output_box .blue_btn").on('click', copyExportCode);
            }
        })

    }
}

function delete_appLinkBtn() {
    if ($('.stn_board').children().hasClass('blue_link_btn') == true) {
        $('.blue_link_btn').remove();
    }
}

function copyExportCode() {
    let $temp = $("<input>");
    let exportCode = $('#scenario_output_box .output_content p').text().trim();
    $("body").append($temp);
    $temp.val(exportCode).select();
    document.execCommand("copy");
    $temp.remove();
    $('#scenario_output_box').hide();
}


/* YEJI: 내보내기 스크립트 생성
 */
function generateExportCode() {

    let avatarEnumVal = $($('.select_list .list_box li.active')[0]).find('img').attr('alt').split(".")[0];
    let bgEnumVal = $($('.select_list .list_box li.active')[1]).find('img').attr('alt').split(".")[0];

    let avatar, bg, scenario;
    switch (avatarEnumVal) {
        case "avatar_baseline":
            avatar = 1;
            break;
        case "avatar_lipsync_teacher_man":
            avatar = 2;
            break;
        case "avatar_lipsync_curator_woman":
            avatar = 3;
            break;
        default:
            break;
    }

    if (bgEnumVal == "bg_gradation") {
        bg = 1;
    } else if (bgEnumVal == "bg_checkered") {
        bg = 2;
    } else if (bgEnumVal == "bg_office") {
        bg = 3;
    } else if (bgEnumVal == "bg_universe") {
        bg = 4;
    } else if (bgEnumVal == "bg_beach") {
        bg = 5;
    }
    scenario = 1;

    $('#scenario_output_box .popWrap .pop_bd .output_content').empty();
    $('#scenario_output_box .popWrap .pop_bd .output_content').append('<p>' +
        '&#60;script type="text/javascript"&#62;window.onload = function () {var iframe = document.createElement("iframe");iframe.id = "myframe";iframe.src = "https://human.maum.ai/?background=' +
        bg + '&scenario=' + scenario + '&avatar=' + avatar +
        '";iframe.frameBorder = "0";iframe.allow = "camera;microphone";document.body.appendChild(iframe);document.getElementsByTagName("html")[0].style.height = "100%";document.getElementsByTagName("body")[0].style.height = "100%";window.parent.document.getElementById("myframe").width = "100%";window.parent.document.getElementById("myframe").height = "100%";};&#60;/script&#62;'
        + '</p>'
    )
}


/*
** Flow name 영역 show or hide
*/
function showFlowName() {
    $('.flow_tit').show().text(workingFlow.name);
    $('.flow_tit').next().css('padding', '15px 20px');
    $('.sub_menu').css('margin', '0 0 45px 0');
}

function hideFlowName() {
    $('.flow_tit').hide();
    $('.flow_tit').next().css('padding', '20px 20px');
    $('.sub_menu').css('margin', '0 0 20px 0');
}

/*
** FLOW의 현재 진행 노드의 상태 출력
*/
function progressFlowNode(node_index, status) {
    let id = '#FlowNode_Engine_' + node_index;
    let outputId = '#FlowNode_Output_' + node_index;

    if (status == true) {
        $(id).addClass('api_progress');
        $(outputId).css('opacity', '1');
        $(id).css('opacity', '1');
    } else $(id).removeClass('api_progress');
}


/*
** Flow 노드의 INPUT 텍스트 출력 (첫 노드)
*/
function displayTextInput(node_index, text) {
    let id = '#FlowNode_Input_' + node_index;

    //해당 output 박스를 비워 준뒤
    $(id + ' .input').empty().css('background', 'none');

    //div를 추가해 주고, 그 안에 값을 넣어준다.
    $(id + ' .input').append('' +
        '<div class="textarea">' +
        text +
        '</div>' +
        '');
}


/*
** Flow 노드의 OUTPUT 텍스트 출력
*/
function displayTextOutput(node_index, text) {
    let id = '#FlowNode_Output_' + node_index;

    //해당 output 박스를 비워 준뒤
    $(id + ' .output').empty().css('background', 'none');
    //div를 추가해 주고, 그 안에 값을 넣어준다.
    $(id + ' .output').append('' +
        '<div class="textarea">' +
        text +
        '</div>' +
        '');
}

/*
** Flow 노드의 INPUT 오디오 출력
*/
function displayAudioInput(node_index, audio_data) {
    let id = '#FlowNode_Input_' + node_index;

    //해당 output 박스를 비워 준뒤
    $(id + ' .input').empty().css("cursor", "pointer");
    //div를 추가해 주고, 그 안에 값을 넣어준다.
    $(id + ' .input').append('\
        <div class="input">\
            <p>' + messages.mvp_flow.audio_playing + ' </p> \
            <audio> <source />  </audio> \
        </div>\
        ');

    $(id).on('click', function () {
        var url = URL.createObjectURL(audio_data);

        $(id + ' source').attr("src", url);
        $(id + ' audio')[0].pause();
        $(id + ' audio')[0].load();
        $(id + ' audio')[0].oncanplaythrough = $(id + ' audio')[0].play();
    });
}

/*
** Flow 노드의 INPUT 오디오 출력
*/
function displayImageInput(node_index, image_data) {
    let id = '#FlowNode_Input_' + node_index;
    let url = URL.createObjectURL(image_data);

    //해당 output 박스를 비워 준뒤
    $(id + ' .input').empty().css("cursor", "pointer");
    //div를 추가해 주고, 그 안에 값을 넣어준다.
    $(id + ' .input').append('<img id="input_img" src="' + url + '" class="input_image" src="" alt="입력 이미지" width="70px" height="70px">');

    $(id).on('click', function () {
        $('#ttiImgView').attr('src', $('#input_img').attr('src'));
        $('#result_img_view').show();
        $('#result_img_view .blue_close').on('click', function () {
            $('#result_img_view').hide();
        });
    });
}


/*
** Flow 노드의 INPUT none 출력
*/
function displayNoneInput(node_index) {
    let id = '#FlowNode_Input_' + node_index;

    //해당 output 박스를 비워 준뒤
    $(id + ' .input').empty().css("cursor", "pointer");
    //div를 추가해 주고, 그 안에 값을 넣어준다.
    $(id + ' .input').append('<p id="input_img" width="70px" height="70px">none</p>');

}


/*
** Flow 노드의 INPUT multi 출력
*/
function displayMultiInput(node_index, input_data) {
    let id = '#FlowNode_Input_' + node_index;

    //해당 output 박스를 비워 준뒤
    $(id + ' .input').empty().css("cursor", "pointer");
    //div를 추가해 주고, 그 안에 값을 넣어준다.
    $(id + ' .input').append('\
        <div class="input">\
            <p>' + 'multi' + '</p> \
            <audio> <source />  </audio> \
        </div>\
        ');

    $(id).on('click', function () {
        var url = URL.createObjectURL(input_data.audio);

        $(id + ' source').attr("src", url);
        $(id + ' audio')[0].pause();
        $(id + ' audio')[0].load();
        $(id + ' audio')[0].oncanplaythrough = $(id + ' audio')[0].play();
    });
}


/*
** Flow 노드의 OUTPUT 오디오 출력
*/
function displayAudioOutput(node_index, audio_data) {
    let id = '#FlowNode_Output_' + node_index;
    let url;

    console.log(node_index);
    //해당 output 박스를 비워 준뒤
    $(id + ' .output').empty().css('width', '120px').addClass('pointer');

    if (audio_data) {
        url = URL.createObjectURL(audio_data);
        //div를 추가해 주고, 그 안에 값을 넣어준다.
        $(id + ' .output').append('\
            <p>' + messages.mvp_flow.audio_playing + ' </p> \
            <audio> <source />  </audio>\
        ');

        if (node_index === workingFlow.nodeList.length - 1) { // 마지막 output만 다운로드 링크 생기도록
            $(id).append('<a href="' + url + '" class="btn_dwn" download="' + engineList[workingFlow.nodeList[node_index].engineId].name + '.wav"><em class="fas fa-download"></em>' + messages.mvp_flow.download_file + '</a>');
            $(id + ' .btn_dwn').fadeIn();
        }

        $(id + ' .output').on('click', function () {
            // var url = URL.createObjectURL(audio_data);

            $(id + ' source').attr("src", url);
            $(id + ' audio')[0].pause();
            $(id + ' audio')[0].load();
            $(id + ' audio')[0].oncanplaythrough = $(id + ' audio')[0].play();
        });
    } else {
        url = "";
        $(id + ' .output').append('\
            <p>null </p> \
            <audio> <source />  </audio>\
        ');

    }
}

/*
** Flow 노드의 OUTPUT 이미지 출력
*/
function displayImageOutput(node_index, image_data) {
    console.log("------------ Display ---------");
    console.log(image_data);

    let id = '#FlowNode_Output_' + node_index;
    $(id + ' .output').empty().addClass('pointer');//해당 output 박스를 비워 준뒤

    if (image_data) {

        //div를 추가해 주고, 그 안에 값을 넣어준다.
        $(id + ' .output').append(
            '<img id="output_img_' + node_index + '" class="output_image" src="" alt="결과 이미지" height="70" width="70">');

        if (node_index === workingFlow.nodeList.length - 1) { // 마지막 output만 다운로드 링크 생기도록
            $(id).append('<a href="" download="' + image_data.name + '" class="btn_dwn" id="img_download_btn_' + node_index + '" ><em class="fas fa-download"></em>' + messages.mvp_flow.download_file + '</a>');
            $(id + ' .btn_dwn').fadeIn();
        }

        let reader = new FileReader();
        reader.onload = function (e) {
            $("#output_img_" + node_index).attr('src', e.target.result);

            let imgUrl = URL.createObjectURL(image_data);
            $('#img_download_btn_' + node_index).attr('href', imgUrl);
        };
        reader.readAsDataURL(image_data);
    } else {
        $(id + ' .output').append('<img id="output_img_' + node_index + '" class="output_image" src="" alt="결과 이미지" height="70" width="70">');
    }


    $(id + ' .output').on('click', function () {
        $('#ttiImgView').attr('src', $('#output_img_' + node_index).attr('src'));
        $('#result_img_view').show();
    });

    $('#close_result_img_view').bind('click', function () {
        $('#result_img_view').css('display', 'none');
    });

}


/*
** Flow 노드의 OUTPUT multi 출력
*/
function displayMultiOutput(node_index, multi_data) {

    let id = '#FlowNode_Output_' + node_index;
    $(id + ' .output').empty().addClass('pointer').css('background', 'none');//해당 output 박스를 비워 준뒤
    let engineName = engineList[workingFlow.nodeList[node_index].engineId].name;

    if (engineName === messages.engine.face_track) {

        //div를 추가해 주고, 그 안에 값을 넣어준다.
        $(id + ' .output').append(
            '<div class="faceTrackingOutput_box" style="color: #f77a8d; height: 100%;">' +
            '<em class="fas fa-search-plus" style="display: block; font-size: 20px; padding: 16px 0 4px 0 "></em>' +
            messages.mvp_flow.view_result +
            '</div>'
        );

        $('#ft_output_box .scenebox').empty();
        for (let i = 1; i <= Object.keys(multi_data).length / 2; i++) {
            $('#ft_output_box .scenebox').append(
                '<li class="scene">' +
                '<div class="imgbox">' +
                '<img id="resultImg' + i + '" src="' + "data:image/jpeg;base64," + multi_data["frame_" + i] + '" alt="결과' + i + '">' +
                '</div>' +
                '<div class="textbox">' +
                '<p id="resultTime' + i + '">SceneTime : ' + JSON.parse(multi_data["result_json_" + i])[0]["sceneTime"] + 's</p>' +
                '<div class="scene_text">' +
                '<textarea id="resultTxt' + i + '">' + multi_data["result_json_" + i] + '</textarea>' +
                '</div>' +
                '</div>' +
                '</li>'
            );
        }

        $(id).on('click', function () {
            $('#ft_output_box').show();
        });

        $('#ft_output_box .ico_close, #ft_return').on('click', function () {
            $('#ft_output_box').css('display', 'none');
        });

    } else if (engineName == messages.engine.anom_det) {
        //div를 추가해 주고, 그 안에 값을 넣어준다.
        $(id + ' .output').append(
            '<div class="faceTrackingOutput_box" style="color: #f77a8d; height: 100%;">' +
            '<em class="fas fa-search-plus" style="display: block; font-size: 20px; padding: 16px 0 4px 0 "></em>' +
            messages.mvp_flow.view_result +
            '</div>'
        );

        var second = multi_data["result_text"];

        let substring = second.substr(2, 3);
        let number = substring.substring(2);
        let minute = substring.substr(0, 2);
        let numberto = parseInt(number);
        let startSecond;


        if (numberto < 10) {
            numberto = "0" + numberto;
            startSecond = minute + numberto;
        } else {
            startSecond = substring;
        }

        $('#AnomalyDetect_output_box .result_ul').empty();
        for (let i = 1; i <= Object.keys(multi_data).length - 1; i++) {
            if (i == 1) {
                $('#AnomalyDetect_output_box .result_ul').append(
                    '                                <li>' +
                    '                                    <span>' + messages.mvp_flow.anom_det_start + '</span>' +
                    '                                    <img id="resultImg' + i + '" alt="이상행동 시작 시점" src="' + "data:image/jpeg;base64," + multi_data["frame_" + i] + '" alt="결과' + i + '">' +
                    '                                    <p id="snap_time">' + startSecond + '</p>' +
                    '                                </li>'
                );
            } else {

                let timeCheck = 0.5 * (i - 1);

                $('#AnomalyDetect_output_box .result_ul').append(
                    '                                <li>' +
                    '                                    <span> </span>' +
                    '                                    <img id="resultImg' + i + '" src="' + "data:image/jpeg;base64," + multi_data["frame_" + i] + '" alt="결과' + i + '">' +
                    '                                    <p> \+ ' + timeCheck + ' ' + messages.mvp_flow.second + '</p>' +
                    '                                </li>'
                );
            }
        }

        $(id).on('click', function () {
            $('#AnomalyDetect_output_box').show();
        });

        $('#AnomalyDetect_output_box .ico_close, #AnomalyDetect_return').on('click', function () {
            $('#AnomalyDetect_output_box').css('display', 'none');
        });

    } else {
        $(id + ' .output').append('');
    }

    $(id).click();

}

/*
** Flow 노드의 OUTPUT video 출력
*/
function displayVideoOutput(node_index, video_data) {
    let id = '#FlowNode_Output_' + node_index;
    let url;

    console.log(node_index);
    //해당 output 박스를 비워 준뒤
    $(id + ' .output').empty().css('width', '120px').addClass('pointer');

    if (video_data) {
        // url = URL.createObjectURL(video_data);
        //div를 추가해 주고, 그 안에 값을 넣어준다.
        $(id + ' .output').append(
            '<img id="output_img_' + node_index + '" class="output_image" src="/image/video_output.png" alt="결과 이미지" height="30" width="30">');

        if (node_index === workingFlow.nodeList.length - 1) { // 마지막 output만 다운로드 링크 생기도록
            // $(id).append('<a href="' + video_data + '" class="btn_dwn" download="' + engineList[workingFlow.nodeList[node_index].engineId].name + '.mp4"><em class="fas fa-download"></em>' + messages.mvp_flow.download_file + '</a>');
            // $(id + ' .btn_dwn').fadeIn();
        }

        $(id + ' .output').on('click', function () {
            // var url = URL.createObjectURL(audio_data);
            $('#avatarVidView').attr('src', video_data);
            $('#result_vid_view').show();
            // $(id + ' source').attr("src", video_data);
            // $(id + ' video')[0].pause();
            // $(id + ' video')[0].load();
            // $(id + ' video')[0].oncanplaythrough = $(id + ' video')[0].play();
        });
        $('#close_result_vid_view').bind('click', function () {
            $('#avatarVidView')[0].pause();
            $('#result_vid_view').css('display', 'none');
        })
        $('.ico_close').bind('click', function () {
            $('#avatarVidView')[0].pause();
            $('#result_vid_view').css('display', 'none');
        })
    } else {
        url = "";
        $(id + ' .output').append('\
            <p>null </p> \
            <video> <source />  </video>\
        ');

    }
}

function initInputUI(node_index, input_type) {
    let id = '#FlowNode_Input_' + node_index;

    $(id + ' .input').empty();
    $(id + ' .input').append('\
        <span>' + messages.mvp_flow.input_value + '</span>\
        <p>\
        ' + input_type + '\
        </p>\
    ');
}

function initOutputUI(node_index, output_type) {
    let id = '#FlowNode_Output_' + node_index;

    $(id + ' .output').empty();
    $(id + ' .output').append(' \
            <span>' + messages.mvp_flow.result_value + '</span>\
            <p>\
                ' + output_type + '\
            </p>\
    ');
}


//api 삭제버튼 추가
function close_btn() {
    $('.api_show em').remove();
    $('.api_show:last').append(' \
                    <em class="fas fa-times api_close"></em>'
    );

}

//봇 지우기
$(document).on('click', '.api_close', function () {
    workingFlowStatus.flowUpdated = true
    console.log("api 지우기");
    $(this).parent().parent().remove();
    $('.api_select:last-child').remove();
    close_btn();

    var select_last_engine = parseInt($(this).parent().attr('id').split("FlowNode_Engine_")[1]);

    if (select_last_engine + 1 <= workingFlow.nodeList.length) {
        workingFlow.nodeList.pop();
    }

    if (workingFlow.nodeList.length > 0)
        resetEngineStatus(engineList[workingFlow.nodeList[workingFlow.nodeList.length - 1].engineId]);
    else resetEngineStatus(null);

    //TODO: 내보내기 버튼 활성화
    showExportBtn();
});

//Cloud API show
$(document).on('click', '.btn_go_api', function (e) {
    var hrefLink = $(this).attr('href');

    e.preventDefault();

    if (hrefLink == 'null') {
        return;
    }

    $('body').addClass('transform');

    $('#apiFrame').attr('src', hrefLink);

    //var apiFrame = $('#apiFrame')[0];
    //apiFrame.contentWindow.postMessage("message", "*")

    // //자식 frame접근
    // $('#apiFrame').load(function(){
    //     $('#apiFrame').contents().find('body').addClass('aiBuilder');
    // });

    //Cloud API hide
    $('.btn_aside_close').on('click', function () {
        $('body').removeClass('transform');
        $('.aside').removeClass('fullSize');
    });

    $('.btn_aside_size').on('click', function () {
        $('.aside').toggleClass('fullSize');
    });


});

//로직박스 셀렉트 박스 화살표 클릭 시 UI 변경
function arrow(index) {
    $('#Layer_Engine_Param' + index).toggleClass('folding');
}
