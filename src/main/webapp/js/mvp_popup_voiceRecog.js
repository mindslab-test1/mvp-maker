var callbackVoiceRecog = null; // 콜백
var vr_inputType = "";
var vr_audio = null;


$(document).ready(function () {

    // 파일 업로드
    $('#vrInputFile').on('change', function () {
        onClick_VR_fileUpload($(this));
    });

    // 녹음 시작
    $('#voice_input_box .record_btn').on('click', onClick_VR_record);

    // 재생 or 중지
    $('#voice_input_box .voice_play_btn').on('click', function () {
        if ($(this).text() === messages.common.play) {
            playAudio();
            $(this).text(messages.common.pause);
        } else {  // 중지
            pauseAudio();
            $(this).text(messages.common.play);
        }
    });

    // 녹음 중지
    $('#vrRecordStop').on('click', onClick_VR_stopRecording);

    // 삭제
    $('#voice_input_box .voice_delete').on('click', onClick_VR_deleteAudio);

    // voiceId 입력
    $('#vrInputText').on('input paste', VR_inputCompleteBtn_disable);

    // 입력 완료
    $('#vr_input_complete').on('click', onClick_VR_inputComplete);

    // close
    $('#voice_input_box .ico_close').on('click', function () {
        $('#voice_input_box').fadeOut(300);
        $('body').css({'overflow': ''});

        if (is_audio_recording) is_audio_recording = false;
        stopAndClearRecording();

        clear_VR_inputData();

        if (callbackVoiceRecog != null) callbackVoiceRecog(null, null);

    });

});


/* ===================================================================================================================== */
// 사용자 호출
/* ===================================================================================================================== */

function showPopup_VoiceRecog(vr_engine, callback) {

    callbackVoiceRecog = callback;
    vr_inputType = vr_engine.inputType;

    switch (vr_inputType) {
        case 'none':
            callbackVoiceRecog('none', 'none');
            break;
        case 'text':
            show_VR_textPopup();
            break;
        case 'audio':
            show_VR_audioPopup();
            setReadyAudioAPI(VR_audioErr_callback);
            break;
        case 'multi':
            show_VR_multiPopup();
            setReadyAudioAPI(VR_audioErr_callback);
            break;
        default:
            console.log("======= Voice Recog : inputType 오류 ==========");
            callbackVoiceRecog(null, null);
    }

}


/* ===================================================================================================================== */
// 버튼 클릭
/* ===================================================================================================================== */

function onClick_VR_inputComplete() {

    let voiceId = $('#vrInputText').val();
    let $Btn = $('#vr_input_complete');

    if (vr_inputType === 'audio') {
        if (!presentBlobData(vr_audio)) {
            console.log("오디오 없음");
            disableBtn($Btn, true);
            return;
        } else {
            stopAndClearRecording();
            callbackVoiceRecog('audio', vr_audio);
        }

    } else if (vr_inputType === 'text') {
        if (voiceId.trim() === "") {
            console.log("텍스트 없음");
            disableBtn($Btn, true);
            return;
        } else {
            callbackVoiceRecog('text', voiceId);
        }

    } else if (vr_inputType === 'multi') {
        if (!presentBlobData(vr_audio) || voiceId.trim() === "") {
            console.log("input 없음");
            disableBtn($Btn, true);
            return;
        } else {
            stopAndClearRecording();
            let multiObj = {
                "audio": vr_audio,
                "voiceId": voiceId
            };
            callbackVoiceRecog('multi', multiObj);
        }

    }

    $('#voice_input_box').fadeOut(300);
    clear_VR_inputData();

}


function onClick_VR_fileUpload(inputObj) {
    vr_audio = inputObj.get(0).files[0];

    if (!presentBlobData(vr_audio)) {
        return false;
    } else {
        let url = URL.createObjectURL(vr_audio);
        let audio = document.getElementById("AudioPlayer");
        audio.src = url;
        audio.addEventListener('ended', VR_audio_pause_evt);
        set_VR_playUI();
        VR_inputCompleteBtn_disable(); // 입력 완료 버튼 UI변경
    }
}


function onClick_VR_record() {
    is_audio_recording = true;
    startRecording();

    $('#voice_input_box .input_box').hide();
    $('#voice_input_box .btn').hide();
    $('#voice_input_box .recording').show();
    $('#voice_input_box .recording .record_Box').show();
    $('#voice_input_box .recording .record_desc').show();
}


function onClick_VR_stopRecording() {
    is_audio_recording = false;
    stopRecording();

    audio_recorder && audio_recorder.exportMonoWAV(function (blob) {
        vr_audio = blob;
        let url = URL.createObjectURL(blob);
        let audio = document.getElementById("AudioPlayer");
        audio.src = url;
        audio.addEventListener('ended', VR_audio_pause_evt);

        set_VR_playUI();
        VR_inputCompleteBtn_disable(); // 입력 완료 버튼 UI변경

    }, "audio/wav", 16000);


    $('#voice_input_box .input_box').show();
    $('#voice_input_box .btn').show();
    $('#voice_input_box .recording').hide();
    $('#voice_input_box .recording .record_Box').hide();
    $('#voice_input_box .recording .record_desc').hide();
}


function onClick_VR_deleteAudio() {
    stopAndClearRecording();

    vr_audio = null;
    $('#vrInputFile').val(null);
    setReadyAudioAPI(VR_audioErr_callback);

    set_VR_recordUI();
    disableBtn($('#vr_input_complete'), true);
}


/* ===================================================================================================================== */
// UI 설정
/* ===================================================================================================================== */

function init_VR_Popup_UI() {
    set_VR_recordUI();

    disableBtn($('#vr_input_complete'), true);
    $('#voice_input_box .record_btn').attr('disabled', false);
    $('#voice_input_box .record_btn').removeClass('disabled');

    $('#vr_audio').show();
    $('#vr_text').show();

    $('#voice_input_box #vrRecordStop strong').text(messages.common.complete);
}

function show_VR_textPopup() {
    init_VR_Popup_UI();
    $('#vr_audio').hide();
    $('#voice_input_box').show();
}

function show_VR_audioPopup() {
    init_VR_Popup_UI();
    $('#vr_text').hide();
    $('#voice_input_box').show();
}

function show_VR_multiPopup() {
    init_VR_Popup_UI();
    $('#voice_input_box').show();
}


function set_VR_recordUI() {
    $('#voice_input_box .record_btn').show();
    $('#voice_input_box .pop_bd .upload_box').show();
    $('#voice_input_box .pop_bd .voice_play_btn').hide();
    $('#voice_input_box .pop_bd .voice_delete').hide();
}

function set_VR_playUI() {
    $('#voice_input_box .record_btn').hide();
    $('#voice_input_box .pop_bd .upload_box').hide();
    $('#voice_input_box .pop_bd .voice_play_btn').show();
    $('#voice_input_box .pop_bd .voice_delete').show();
}


function VR_inputCompleteBtn_disable() {
    let vrIntputText = $('#vrInputText').val().trim();
    let $Btn = $('#vr_input_complete');

    if (vr_inputType === 'audio') {
        presentBlobData(vr_audio) ? disableBtn($Btn, false) : disableBtn($Btn, true);
    } else if (vr_inputType === 'text') {
        vrIntputText !== "" ? disableBtn($Btn, false) : disableBtn($Btn, true);
    } else if (vr_inputType === 'multi') {
        vrIntputText !== "" && presentBlobData(vr_audio) ? disableBtn($Btn, false) : disableBtn($Btn, true);
    }
}


/* ===================================================================================================================== */
// 공통 function
/* ===================================================================================================================== */
function clear_VR_inputData() {
    vr_audio = null;
    $('#vrInputFile').val(null);
    $('#vrInputText').val("");
}

function VR_audioErr_callback() {
    $('#voice_input_box .record_btn').attr('disabled', true);
    $('#voice_input_box .record_btn').addClass('disabled');
}

function VR_audio_pause_evt() {
    $('#voice_input_box .voice_play_btn').text(messages.common.play);
    // document.getElementById("AudioPlayer").removeEventListener('ended', VR_audio_pause_evt);
}

// blob data가 정상적으로 있으면 true
function presentBlobData(blob_data) {
    return !(blob_data === '' || blob_data === null || blob_data === undefined);
}