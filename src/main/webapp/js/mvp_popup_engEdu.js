var callbackEngEdu = null; // 콜백
var engEdu_audio = null;


$(document).ready(function () {

    // 파일 업로드
    $('#engEduInputFile').on('change', function () {
        onClick_EngEdu_fileUpload($(this));
    });

    // 녹음 시작
    $('#engEdu_input_box .record_btn').on('click', onClick_EngEdu_record);

    // 재생 or 중지
    // Check for english
    $('#engEdu_input_box .voice_play_btn').on('click', function () {
        if ($(this).text() === messages.common.play) {
            playAudio();
            $(this).text(messages.common.pause);
        } else {  // 중지 or Pause
            pauseAudio();
            $(this).text(messages.common.play);
        }
    });

    // 녹음 중지
    $('#engEduRecordStop').on('click', onClick_EngEdu_stopRecording);

    // 삭제
    $('#engEdu_input_box .voice_delete').on('click', onClick_EngEdu_deleteAudio);

    // 정답문장 입력
    $('#engEduInputText').on('input paste', EngEdu_inputCompleteBtn_disable);

    // 입력 완료
    $('#engEdu_input_complete').on('click', onClick_EngEdu_inputComplete);

    // close
    $('#engEdu_input_box .ico_close').on('click', function () {
        $('#engEdu_input_box').fadeOut(300);
        $('body').css({'overflow': ''});

        if (is_audio_recording) is_audio_recording = false;
        stopAndClearRecording();

        clear_EngEdu_inputData();

        if (callbackEngEdu != null) callbackEngEdu(null, null);

    });

});


/* ===================================================================================================================== */
// 사용자 호출
/* ===================================================================================================================== */

function showPopup_EngEdu(engEduEngine, callback) {
    callbackEngEdu = callback;

    $('#engEdu_input_box h2 span').text(engEduEngine.name + ' ' + messages.popup_engEdu.inputValue);
    init_EngEdu_Popup_UI();
    $('#engEdu_input_box').show();

    setReadyAudioAPI(EngEdu_audioErr_callback);
}


/* ===================================================================================================================== */
// 버튼 클릭
/* ===================================================================================================================== */

function onClick_EngEdu_fileUpload(inputObj) {
    engEdu_audio = inputObj.get(0).files[0];

    if (engEdu_audio === null || engEdu_audio === "" || engEdu_audio === undefined) {
        return false;
    } else {
        let url = URL.createObjectURL(engEdu_audio);
        let audio = document.getElementById("AudioPlayer");
        audio.src = url;
        audio.addEventListener('ended', EngEdu_audio_pause_evt);
        set_EngEdu_playUI();
        EngEdu_inputCompleteBtn_disable(); // 입력 완료 버튼 UI변경
    }
}


function onClick_EngEdu_record() {
    is_audio_recording = true;
    startRecording();
    set_EngEdu_recordUI();
}


function onClick_EngEdu_stopRecording() {
    is_audio_recording = false;
    stopRecording();

    audio_recorder && audio_recorder.exportMonoWAV(function (blob) {
        engEdu_audio = blob;
        let url = URL.createObjectURL(blob);
        let audio = document.getElementById("AudioPlayer");
        audio.src = url;
        audio.addEventListener('ended', EngEdu_audio_pause_evt);

        set_EngEdu_playUI();
        set_EngEdu_recordToInputUI();
        EngEdu_inputCompleteBtn_disable(); // 입력 완료 버튼 UI변경
    }, "audio/wav", 16000);

}


function onClick_EngEdu_deleteAudio() {
    stopAndClearRecording();

    engEdu_audio = null;
    $('#engEduInputFile').val(null);
    setReadyAudioAPI(EngEdu_audioErr_callback);

    set_EngEdu_ReadyUI();
    disableBtn($('#engEdu_input_complete'), true);
}


function onClick_EngEdu_inputComplete() {
    let answerText = $('#engEduInputText').val();

    if (!presentBlobData(engEdu_audio) || answerText.trim() === "") {
        console.log("No input");
        disableBtn($('#engEdu_input_complete'), true);
        return;
    } else {
        stopAndClearRecording();
        let multiObj = {
            "audio": engEdu_audio,
            "answerText": answerText
        };
        callbackEngEdu('multi', multiObj);
    }

    $('#engEdu_input_box').fadeOut(300);
    clear_EngEdu_inputData();
}


/* ===================================================================================================================== */
// UI 설정
/* ===================================================================================================================== */

function init_EngEdu_Popup_UI() {
    set_EngEdu_ReadyUI();
    disableBtn($('#engEdu_input_complete'), true);
    disableBtn($('#engEdu_input_box .record_btn'), false);

    $('#engEdu_audio').show();
    $('#engEdu_text').show();
}

// audio 입력 선택(녹음/업로드) 화면
function set_EngEdu_ReadyUI() {
    $('#engEdu_input_box .record_btn').show();
    $('#engEdu_input_box .pop_bd .upload_box').show();
    $('#engEdu_input_box .pop_bd .voice_play_btn').hide();
    $('#engEdu_input_box .pop_bd .voice_delete').hide();
}

// 녹음 화면
function set_EngEdu_recordUI() {
    $('#engEdu_input_box .input_box').hide();
    $('#engEdu_input_box .btn').hide();
    $('#engEdu_input_box .recording').show();
    $('#engEdu_input_box .recording .record_Box').show();
    $('#engEdu_input_box .recording .record_desc').show();
}

// audio 듣기 화면
function set_EngEdu_playUI() {
    $('#engEdu_input_box .record_btn').hide();
    $('#engEdu_input_box .pop_bd .upload_box').hide();
    $('#engEdu_input_box .pop_bd .voice_play_btn').show();
    $('#engEdu_input_box .pop_bd .voice_delete').show();
}

// 녹음 화면 숨기고, 입력 화면 보여주기
function set_EngEdu_recordToInputUI() {
    $('#engEdu_input_box .input_box').show();
    $('#engEdu_input_box .btn').show();
    $('#engEdu_input_box .recording').hide();
    $('#engEdu_input_box .recording .record_Box').hide();
    $('#engEdu_input_box .recording .record_desc').hide();
}


function EngEdu_inputCompleteBtn_disable() {
    if ($('#engEduInputText').val().trim() !== "" && presentBlobData(engEdu_audio)) {
        disableBtn($('#engEdu_input_complete'), false);
    } else {
        disableBtn($('#engEdu_input_complete'), true);
    }
}

/* ===================================================================================================================== */
// 공통 function
/* ===================================================================================================================== */
function EngEdu_audioErr_callback() {
    disableBtn($('#engEdu_input_box .record_btn'), true);
}

function EngEdu_audio_pause_evt() {
    $('#engEdu_input_box .voice_play_btn').text(messages.common.play);
    // document.getElementById("AudioPlayer").removeEventListener('ended', EngEdu_audio_pause_evt);
}

function clear_EngEdu_inputData() {
    engEdu_audio = null;
    $('#engEduInputFile').val(null);
    $('#engEduInputText').val("");
}