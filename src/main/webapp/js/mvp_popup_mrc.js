var callbackMrc = null; // 콜백

$(document).ready(function () {

    $('#mrc_input_context, #mrc_input_question').on('input paste', MRC_inputCompleteBtn_disable);

    // 입력 완료
    $('#mrc_input_box .blue_btn').on('click', onClick_MRC_inputComplete);

    // close
    $('#mrc_input_box .ico_close').on('click', function () {
        $('#mrc_input_box').fadeOut(300);
        $('body').css({'overflow': ''});

        if (callbackMrc != null) callbackMrc(null, null);
    });
});


/* ===================================================================================================================== */
// 버튼 클릭
/* ===================================================================================================================== */
function onClick_MRC_inputComplete() {

    let context = $('#mrc_input_context').val();
    let question = $('#mrc_input_question').val();

    if (context !== "" && question !== "") {
        let multiObj = {
            "context": context,
            "question": question
        };
        callbackMrc('multi', multiObj);

        $('#mrc_input_box').fadeOut(300);
        init_MRC_Popup_UI();
    } else {
        disableBtn($('#mrc_input_complete'), true);
    }

}

/* ===================================================================================================================== */
// UI 설정
/* ===================================================================================================================== */

function show_MRC_multiPopup() {
    init_MRC_Popup_UI();
    $('#mrc_input_box').show();
}

function init_MRC_Popup_UI() {
    $('#mrc_input_context').val("");
    $('#mrc_input_question').val("");
    disableBtn($('#mrc_input_complete'), true);
}

function MRC_inputCompleteBtn_disable() {
    let context = $('#mrc_input_context').val().trim();
    let question = $('#mrc_input_question').val().trim();
    let $Btn = $('#mrc_input_complete');

    (context !== "" && question !== "") ? disableBtn($Btn, false) : disableBtn($Btn, true);
}

/* ===================================================================================================================== */
// 사용자 호출
/* ===================================================================================================================== */

function showPopup_MRC(engine, callback) {

    callbackMrc = callback;

    if (engine.inputType == 'multi')
        show_MRC_multiPopup();
    else {
        console.log("======= MRC : inputType 오류 ==========");
        callbackMrc(null, null);
    }
}