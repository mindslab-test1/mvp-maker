/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.stt] = callApi_STT;

/*
** API 실행
*/
function callApi_STT(input_data, flow_node, engine, handler) {
    console.log('--> [ STT ] 실행');

    var param_list = new Array();
    getParamListFromFlowNode(flow_node, param_list);
    console.dir(param_list);

    var formData = new FormData();
    var audio_file = new File([input_data], 'stt_file.wav');

    for(var xx = 0; xx < param_list.length; xx++) {
        formData.append(param_list[xx].name, param_list[xx].value);
        console.log("    * ", param_list[xx].name, " : ", param_list[xx].value)
    }
    formData.append('cmd', 'runFileStt');
    formData.append('file', audio_file);

    mvp_request = new XMLHttpRequest();
    mvp_request.responseType ="json";
    mvp_request.onreadystatechange = function(){
        if (mvp_request.readyState === 4 ) {
            console.dir(mvp_request.response);

            if(mvp_request.status == 200) {
                console.log("    # result >> ", mvp_request.response.data);
                handler.controlFlow('text', mvp_request.response.data);
            }
            else {
                handler.controlFlow('text', null);
            }
        }

        if(mvp_request.readyState === 0){
            console.log(engine.name + " Request abort()");
            return false;
        }
    };
    mvp_request.open('POST', '/mvp/runner/api/stt');
    mvp_request.send(formData);
}