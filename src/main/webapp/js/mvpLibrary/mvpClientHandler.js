/* *********************************************************************************************************************
**
** MVP FLOW 실행 모듈
**
** *********************************************************************************************************************
*/

/* ['name'] = function */
var api_func_link = new Array();

loadJavascript(url___mvp_maker + "/js/mvpLibrary/mvpExecChat.js?1", () => {}, "UTF-8");
loadJavascript(url___mvp_maker + "/js/mvpLibrary/mvpExecStt.js?1", () => {}, "UTF-8");
loadJavascript(url___mvp_maker + "/js/mvpLibrary/mvpExecTts.js?1", () => {}, "UTF-8");



function mvpClientHandler(api_id, api_key, flow) {

    this.api_id = api_id;                   // API ID/KEY
    this.api_key = api_key;

    this.flow = flow;                       // 실행할 Flow 데이타
    this.is_running = false;                // 실행 여부
    this.working_node_index = -1;           // 현재 실행 중인 노드의 인덱스

    this.callback_onStart = null;                    // 콜백 - 플로우 실행 시작
    this.callback_onCancel = null;                   // 콜백 - 플로우 실행 취소
    this.callback_onFinish = null;                   // 콜백 - 플로우 실행 완료
    this.callback_onBeforeEngine = null;             // 콜백 - 엔진 실행 시작
    this.callback_onAfterEngine = null;              // 콜백 - 엔진 실행 완료
    this.callback_onError = null;                    // 콜백 - 에러

}

/* ===================================================================================================================== */
// METHOD : 기동 종료
/*===================================================================================================================== */

mvpClientHandler.prototype.start = function (input_data) {
    console.log('==================================================');
    console.log("시나리오를 실행합니다. (", this.flow.name, ")");
    console.log('--------------------------------------------------');
    console.dir(input_data);

    if(this.onStart != null) this.onStart(this);

    this.working_node_index = 0;

    if(engines[this.flow.nodeList[0].engineId].inputType === 'text') {
        console.log('input = text');
    }
    else if(engines[this.flow.nodeList[0].engineId].inputType === 'audio') {
        console.log('input = audio');
    }
    else if(engines[this.flow.nodeList[0].engineId].inputType === 'image') {
        console.log('input = image');
    }
    else {
        console.log('input = unknown');
    }

    this.controlFlow(engines[this.flow.nodeList[0].engineId].inputType, input_data);
}

mvpClientHandler.prototype.cancel = function () {
    if(this.onCancel != null) this.onCancel(this);
}


/* ===================================================================================================================== */
// METHOD : 등록
/*===================================================================================================================== */

mvpClientHandler.prototype.onStart = function(handler, node_index) {
    if(this.callback_onStart != null) this.callback_onStart(handler, node_index);
}
mvpClientHandler.prototype.setOnStart = function (callback) {
    this.callback_onStart = callback;
    return this;
}


mvpClientHandler.prototype.onCancel = function(handler) {
    if(this.callback_onCancel != null) this.callback_onCancel(handler);
}
mvpClientHandler.prototype.setOnCancel = function (callback) {
    this.callback_onCancel = callback;
    return this;
}


mvpClientHandler.prototype.onFinish = function(handler, node_index) {
    if(this.callback_onFinish != null) this.callback_onFinish(handler, node_index);
}
mvpClientHandler.prototype.setOnFinish = function (callback) {
    this.callback_onFinish = callback;
    return this;
}


mvpClientHandler.prototype.onBeforeEngine = function(handler, node_index, data_type, data) {
    if(this.callback_onBeforeEngine != null) this.callback_onBeforeEngine(handler, node_index, data_type, data);
}
mvpClientHandler.prototype.setOnBeforeEngine = function (callback) {
    this.callback_onBeforeEngine = callback;
    return this;
}



mvpClientHandler.prototype.onAfterEngine = function(handler, node_index, data_type, data) {
    if(this.callback_onAfterEngine != null) this.callback_onAfterEngine(handler, node_index, data_type, data);
}
mvpClientHandler.prototype.setOnAfterEngine = function (callback) {
    this.callback_onAfterEngine = callback;
    return this;
}


mvpClientHandler.prototype.onError = function(handler) {
    if(this.callback_onError != null) this.callback_onError(handler);
}
mvpClientHandler.prototype.setOnError = function (callback) {
    this.callback_onError = callback;
    return this;
}


/* ===================================================================================================================== */
// METHOD : 상태 조회
/*====================================================================================================================== */

mvpClientHandler.prototype.isRunning = function() {
    return this.is_running;
}




/* ===================================================================================================================== */
// 시나리오 실행
/*===================================================================================================================== */


/*
** FLOW 를 실행한다.
*/
mvpClientHandler.prototype.controlFlow = function (input_type, input_data) {

    if(this.working_node_index != 0) this.onAfterEngine(this, this.working_node_index-1, input_type, input_data);

    console.log("[ ControlFlow ] -----------------------------------------------------------------------")
    console.log("working_index = ", this.working_node_index, ", node length = ", this.flow.nodeList.length);

    if(this.working_node_index >= this.flow.nodeList.length) {
        console.log('--------------------------------------------------');
        console.log("시나리오가 종료 되었습니다. (", this.flow.name, ")");
        console.log('==================================================');

        return;
    }

    this.onBeforeEngine(this, this.working_node_index, input_type, input_data);
    this.runNode(input_data, this.flow.nodeList[this.working_node_index]);
    this.working_node_index++;
}

/*
** FLOW 를 실행한다.
*/
mvpClientHandler.prototype.runNode = function (input_data, flow_node) {
    let engine = engines[flow_node.engineId];

    api_func_link[engine.name](input_data, flow_node, engine, this);

}







