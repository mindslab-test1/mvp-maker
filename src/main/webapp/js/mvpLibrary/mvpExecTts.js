/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.tts] = callApi_TTS;

/*
** API 실행
*/
function callApi_TTS(input_data, flow_node, engine, handler) {
    var param_list = new Array();
    getParamListFromFlowNode(flow_node, param_list);
    console.dir(param_list)

    console.log('--> [ TTS ] 실행');
    console.log('    * input : ', input_data);
    console.log('    * voiceName : ', param_list[0].value);

    var formData = new FormData();

    formData.append('apiId','maum-ai-web-demo');
    formData.append('apiKey','eee2ed4e7e084c32adc2ad93d14bb22a');
    formData.append('text',input_data);
    formData.append('voiceName', param_list[0].value);

    mvp_request = new XMLHttpRequest();
    mvp_request.responseType ="blob";
    mvp_request.onreadystatechange = function(){
        if (mvp_request.readyState === 4 ) {
            console.dir(mvp_request.response);

            if(mvp_request.status == 200) {
                var sample1SrcUrl = URL.createObjectURL(mvp_request.response);

                // $("#AudioSource_TTS").attr("src", sample1SrcUrl);
                // $("#AudioPlayer_TTS")[0].pause();
                // $("#AudioPlayer_TTS")[0].load();
                // $("#AudioPlayer_TTS")[0].oncanplaythrough = $("#AudioPlayer_TTS")[0].play();
                // $("#AudioPlayer_TTS").off('ended').on('ended', function () {
                //     handler.runNode('audio', mvp_request.response);
                // });

                console.log("----> 1");
                handler.controlFlow('audio', mvp_request.response);
            }
            else {
                console.log("----> 2");
                handler.controlFlow('audio', null);
            }
        }

        if(mvp_request.readyState === 0){
            console.log(engine.name + " Request abort()");
            return false;
        }
    };
    mvp_request.open('POST', 'https://api.maum.ai/tts/stream/');
    mvp_request.send(formData);
}



