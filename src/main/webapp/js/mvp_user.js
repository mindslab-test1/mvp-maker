$(document).ready(function () {
    /*
    ** logout 버튼 클릭
    */
    $('.btn_logout').on('click', function () {
        $(".lyrWrap").css("display", "block");
        $('#api_logout').fadeIn();
    });


    /*
    ** logout 취소
    */
    $('#Button_LogoutCancel').on('click', function () {
        $('#api_logout').fadeOut();
        $('.pop_simple').css("display", "none");
        $(".lyrWrap").css("display", "none");
        $('.pop_simple').fadeOut(300);
        $('.lyr_service').fadeOut(200);
        $('#mail_success').fadeOut(200);
        $('body').css({
            'overflow': '',
        });
    });


    /*
    ** 로그아웃 실행
    */
    $('#Button_LogoutOk').on('click', function () {
        $('#api_logout').fadeOut();
        $('.etcmenu').hide();
        window.location.href = "/security/logout";
    });

    // 공통 팝업창 닫기
    $('.ico_close, .pop_bg, .btn_close, .btn_lyrWrap_close').on('click', function () {
        $('.pop_simple').fadeOut(300);
        $('.lyr_service').fadeOut(200);
        $('#mail_success').fadeOut(200);
        $('body').css({
            'overflow': '',
        });
    });

});


