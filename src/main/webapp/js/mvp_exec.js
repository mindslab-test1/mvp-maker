var mvp_request = null;
var mvp_request_statusCheck = null;
var reqAbort = false;
var activeMedia = {};

$(document).ready(function () {
    $('#Button_MVPExec').on("click", function () {
        reqAbort = false;
        workingFlowStatus.execInProg = true;

        $('.api_close').css('display', 'none'); // Hide engine close button

        let out_leng = $(".output").length;
        let inout_set = $(".inout_set").length;
        if (inout_set > 0 && out_leng != inout_set) {
            showPopup_Message(messages.mvp_exec.popup_selectModel);
            return;
        }

        // Pause button clicked
        if ($(this).html() === messages.common.pause) {
            reqAbort = true;
            controlFlow();
        } else {
            if (workingFlow.nodeList.length == 0) {
                showPopup_Message(messages.mvp_exec.popup_selectFlowNode);
                return;
            }

            $('.api_show').css('opacity', '0.7');
            $('.output_show').css('opacity', '0.7');
            $('#FlowNode_Input_0').css('opacity', '1');
            $('.api_set:last-child .output_show a').remove();

            console.log('==================================================');
            console.log("시나리오를 실행합니다. (", workingFlow.name, ")");
            console.log('--------------------------------------------------');
            runFlow();
        }

        $(this).blur(); // focus out 시키기
    });

    console.dir(workingFlow);
});


/* ===================================================================================================================== */
// Flow 실행 제어
/* ===================================================================================================================== */

var currentWorkingIndex;

function runFlow() {
    beforeProc_MVPExec();

    currentWorkingIndex = 0;
    console.dir(workingFlow);

    let engineName = engineList[workingFlow.nodeList[0].engineId].name;
    let engineInputType = engineList[workingFlow.nodeList[0].engineId].inputType;

    if (engineName.indexOf(messages.engine.voice_rec) !== -1) {
        showPopup_VoiceRecog(engineList[workingFlow.nodeList[0].engineId], popupCallback);
        return;
    } else if (engineName.indexOf(messages.engine.face_rec) !== -1) {
        showPopup_FaceRecog(engineList[workingFlow.nodeList[0].engineId], popupCallback);
        return;
    } else if (engineName.indexOf(messages.engine.stt_eng_edu) !== -1 || engineName.indexOf(messages.engine.pron_scoring) !== -1 || engineName.indexOf(messages.engine.phonics_assess) !== -1) {
        showPopup_EngEdu(engineList[workingFlow.nodeList[0].engineId], popupCallback);
        return;
    } else if (engineName.indexOf(messages.engine.voice_filter) !== -1) {
        showPopup_VoiceFilter(engineList[workingFlow.nodeList[0].engineId], popupCallback);
        return;
    } else if (engineName.indexOf(messages.engine.mrc) !== -1) {
        showPopup_MRC(engineList[workingFlow.nodeList[0].engineId], popupCallback);
        return;
    } else if (engineName.indexOf(messages.engine.esr) !== -1 || engineName.indexOf(messages.engine.subt_extr) !== -1 || engineName.indexOf(messages.engine.lic_plate_rec) !== -1) {
        // ESR, AVR, Subtit_recog
        // 이미지 샘플이 들어가 있는 팝업
        showPopup_ImageSample(engineList[workingFlow.nodeList[0].engineId], popupCallback);
        return;
    }

    if (engineInputType === 'text') {
        let input_hint = null;
        if (engineName === messages.engine.ai_styling) {
            input_hint = messages.mvp_exec.ai_styling_inputHint
        }
        showPopup_InputText(null, input_hint, popupCallback);
    } else if (engineInputType === 'audio') {
        showPopup_AudioRecord(getSamplingRate(workingFlow.nodeList[0]), popupCallback);
    } else if (engineInputType === 'image') {
        showPopup_CameraCapture(popupCallback);
    } else if (engineInputType === 'video') {
        showPopup_VideoUpload(popupCallback);
    } else {
        console.log("### 실패 >> 알 수 없는 입력 유형 >> ", engineInputType);
    }

}

function popupCallback(input_type, data) {
    if (data == null) {
        console.log('--------------------------------------------------');
        console.log("시나리오가 취소 되었습니다. (", workingFlow.name, ")");
        console.log('==================================================');

        afterProc_MVPExec();
        return;
    }

    controlFlow(input_type, data);
}


async function controlFlow(input_type, input_data) {
    console.log('--------------------------------------------------');
    console.log(input_type);
    console.log(input_data);
    console.log('--------------------------------------------------');


    if (reqAbort === true) {
        console.log('--------------------------------------------------');
        console.log("시나리오를 중지합니다. (", workingFlow.name, ")");
        console.log('==================================================');

        if (mvp_request)
            mvp_request.abort();

        if (mvp_request_statusCheck) {
            abort_statusCheck = true;
        }


        progressFlowNode(currentWorkingIndex - 1, false);

        ///////////////////////////// 미디어 멈추기
        if (activeMedia.prom) {
            activeMedia.prom.then(function () {
                activeMedia.elem.pause();
            })
        }

        currentWorkingIndex = workingFlow.nodeList.length;
        afterProc_MVPExec();
        return;
    }

    console.log("currentWorkingIndex = ", currentWorkingIndex);

    if (currentWorkingIndex > 0 && input_type == 'text') displayTextOutput(currentWorkingIndex - 1, input_data);
    if (currentWorkingIndex > 0 && input_type == 'audio') displayAudioOutput(currentWorkingIndex - 1, input_data);
    if (currentWorkingIndex > 0 && input_type == 'json') displayTextOutput(currentWorkingIndex - 1, JSON.stringify(input_data, null, 4));
    if (currentWorkingIndex > 0 && input_type == 'image') displayImageOutput(currentWorkingIndex - 1, input_data);
    if (currentWorkingIndex > 0 && input_type == 'multi') displayMultiOutput(currentWorkingIndex - 1, input_data);
    // TODO: added for video output box
    if (currentWorkingIndex > 0 && input_type == 'video') displayVideoOutput(currentWorkingIndex - 1, input_data);

    if (currentWorkingIndex > 0 && currentWorkingIndex < workingFlow.nodeList.length) await sleep(800);
    progressFlowNode(currentWorkingIndex - 1, false);

    if (currentWorkingIndex >= workingFlow.nodeList.length) {
        console.log('--------------------------------------------------');
        console.log("시나리오가 종료 되었습니다. (", workingFlow.name, ")");
        console.log('==================================================');

        afterProc_MVPExec();
        return;
    }

    if (currentWorkingIndex == 0 && input_type == 'text') displayTextInput(currentWorkingIndex, input_data);
    if (currentWorkingIndex == 0 && input_type == 'audio') displayAudioInput(currentWorkingIndex, input_data);
    if (currentWorkingIndex == 0 && input_type == 'image') displayImageInput(currentWorkingIndex, input_data);
    // if(currentWorkingIndex == 0 && input_type == 'none') displayNoneInput(currentWorkingIndex);
    if (currentWorkingIndex == 0 && input_type == 'multi') displayMultiInput(currentWorkingIndex, input_data); // TODO : multi 엔진 추가되면 수정 필요

    progressFlowNode(currentWorkingIndex, true);
    runFlowNode(input_data, workingFlow.nodeList[currentWorkingIndex], currentWorkingIndex + 1);
    currentWorkingIndex++;
}


function runFlowNode(input_data, flow_node, nodeIndex) {
    var engine = engineList[flow_node.engineId];
    callApi(input_data, flow_node, engine, nodeIndex);
}

function callApi(input_data, flow_node, engine, nodeIndex) {
    if (input_data) { // Make API engine calls
        api_func_link[engine.name](input_data, flow_node, engine, nodeIndex)
            .then(data => {
                console.log("--> moving to next flow")
                controlFlow(data.input_type, data.resp_data)
            })
            .catch(err => {
                console.log("--> error handling")
                if (!reqAbort) {
                    if (err.errResponse) {
                        logicBoxErrMsgPopup(err.engineName, err.errMsg, err.errResponse, err.jsonPath)
                    } else {
                        engineErrMsgPopup(err.engineName, err.errMsg)
                    }
                }
                controlFlow(err.input_type, null);
            })
    } else { // node 인풋이 null이거나 비어있는 경우 flow 중단
        progressFlowNode(currentWorkingIndex, false);
        currentWorkingIndex = workingFlow.nodeList.length;
        controlFlow(null, null);
    }
}

// Logic Box 외의 엔진들의 에러 팝업창 내용 설정
function engineErrMsgPopup(engineName, errMsg) {
    let title = "<span style='color:#2575f9;'>" + engineName + "</span>";
    let msg = errMsg

    $('#Engine_InputErr_Popup').fadeIn();
    $('#Engine_InputErr_Popup h2').html(title);
    $('#Engine_InputErr_Popup_Contents').html(msg);
    $('#Button_engErr').on('click', function () {
        $('#Engine_InputErr_Popup').fadeOut();
    })
}

// Logic Box 에러 팝업창 내용 설정
function logicBoxErrMsgPopup(engineName, errMsg, errResponse, jsonPath) {
    let title = "<span style='color:#2575f9;'>" + engineName + "</span>";
    let messageBuilder = errMsg;
    if (jsonPath) {
        messageBuilder += '<br /><br />Specified path: ' + jsonPath;
    }
    $('#Engine_LogicBoxErr_Popup').fadeIn();
    $('#Engine_LogicBoxErr_Popup h2').html(title);
    $('#Engine_LogicBoxErr_Popup_ErrMsg').html(messageBuilder);
    $('#Engine_LogicBoxErr_Popup_ErrResponse_InnerBox').html(errResponse)
    $('#Button_LogicBoxErr').on('click', function () {
        $('#Engine_LogicBoxErr_Popup').fadeOut();
    })
}

/* ===================================================================================================================== */
// UI 작업
/* ===================================================================================================================== */

/*
** MVP 실행 전, 사전 작업
*/
function beforeProc_MVPExec() {
    // MVP 실행 비활성화
    $('#Button_MVPExec').html(messages.common.pause);

    for (let xx = 0; xx < workingFlow.nodeList.length; xx++) {
        if (xx == 0) initInputUI(xx, engineList[workingFlow.nodeList[xx].engineId].inputType);
        initOutputUI(xx, engineList[workingFlow.nodeList[xx].engineId].outputType);
    }
}

/*
** MVP 실행 후, 사후 작업
*/
function afterProc_MVPExec() {
    $('.api_close').css('display', 'block');

    var last_engine_id = $('.inout_set').eq($('.inout_set').length - 1).children('.api_show').children('.api_bot').attr('id');

    // 마지막 엔진의 output이 audio나 image일 경우 다운로드 버튼 활성화
    if (last_engine_id) {
        if (engineList[last_engine_id].responseType == 'audio' || engineList[last_engine_id].responseType == 'image')
            $('.inout_set').eq($('.inout_set').length - 1).children('.output_show').children('.output').children('.btn_dwn').css('display', 'block');
    }

    // MVP 실행 버튼 활성화
    $('#Button_MVPExec').html("<em class=\"fas fa-project-diagram\"></em>" + messages.common.exec);

}

/* ===================================================================================================================== */
// AUDIO의 샘플링 값 조회
/* ===================================================================================================================== */

function getSamplingRate(flow_node) {
    // 음성 인식인 경우엔, sampling 파라메타를 확인한다.
    if (engineList[flow_node.engineId].name == messages.engine.stt) {
        return getSamplingRate_STT(flow_node);
    }

    return 16000;
}

function getSamplingRate_STT(flow_node) {
    var param_list = new Array();
    getParamListFromFlowNode(flow_node, param_list);

    for (var xx = 0; xx < param_list.length; xx++) {
        if (param_list[xx].name == 'sampling') return param_list[xx].value;
    }

    return 16000;
}

/* ===================================================================================================================== */
// API Call 주입 값 Validation
/* ===================================================================================================================== */
function inputSizeValidation(engineName, dataSize, requiredSize) {
    if (dataSize > requiredSize) {
        console.log(engineName + "===> Fail, 파일 용량 제한: " + dataSize);
        return true;
    }
    return false;
}


