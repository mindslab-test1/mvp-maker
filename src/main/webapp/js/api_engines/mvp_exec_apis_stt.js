/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.stt] = callApi_STT;

/*
** API 실행
*/
function callApi_STT(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'text';
        data.engineName = engine.name;

        if (inputSizeValidation(engine.name,  Math.round(input_data.size / 1024),5120)) { // 5MB = 5120KB
            data.errMsg = messages.mvp_exec_api.error_fileSize_exceed + "5MB";
            reject(data);
        }
        else {
            console.log('--> [ STT ] 실행');

            var param_list = new Array();
            getParamListFromFlowNode(flow_node, param_list);
            console.dir(param_list);

            var formData = new FormData();
            var audio_file = new File([input_data], 'stt_file.wav');

            for (var xx = 0; xx < param_list.length; xx++) {
                if(param_list[xx].name === "level") formData.append("model", param_list[xx].value);
            }
            formData.append('file', audio_file);

            mvp_request = new XMLHttpRequest();
            mvp_request.open('POST', '/mvp/runner/api/cnnStt');
            mvp_request.send(formData);

            mvp_request.onerror = function () {
                data.errMsg = messages.engineError.output;
                reject(data);
            }

            mvp_request.onreadystatechange = function () {
                if (mvp_request.readyState === 4) {
                    console.dir(mvp_request.response);

                    if (mvp_request.status === 200) {
                        console.log("    # result >> ", mvp_request.response.data);

                        if (mvp_request.response.data === "" || mvp_request.response.data === null) {
                            console.log("    # error >> empty response data")
                            // No sound recognized please try recording again.
                            data.errMsg = messages.engineError.output;
                            reject(data);
                        }else {
                            data.resp_data = mvp_request.response;
                            resolve(data);
                        }
                    } else {
                        console.log("    # error >> response status " + mvp_request.status)
                        data.errMsg = messages.engineError.output;
                        reject(data);
                    }
                }

                if (mvp_request.readyState === 0) {
                    console.log(engine.name + " Request abort()");
                    return false;
                }
            };
        }
    });
}