/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.stt_eng_edu] = callApi_EngEdu_STT;
api_func_link[messages.engine.pron_scoring] = callApi_EngEdu_PronScore;
api_func_link[messages.engine.phonics_assess] = callApi_EngEdu_Phonics;

/*
** API 실행
*/
function callApi_EngEdu_STT(input_data, flow_node, engine) {
    return callApi_EngEdu_common(input_data, flow_node, engine, "/mvp/runner/api/engedu/stt");
}

function callApi_EngEdu_PronScore(input_data, flow_node, engine) {
    return callApi_EngEdu_common(input_data, flow_node, engine, "/mvp/runner/api/engedu/pron");
}

function callApi_EngEdu_Phonics(input_data, flow_node, engine) {
    return callApi_EngEdu_common(input_data, flow_node, engine, "/mvp/runner/api/engedu/phonics");
}

// function callApi_EngEdu_Phonics(input_data, flow_node, engine) {
//     callApi_EngEdu_common(input_data, flow_node, engine, "/mvp/runner/api/engEdu/phonics");
// }


function callApi_EngEdu_common(input_data, flow_node, engine, url) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'json';
        data.engineName = engine.name;

        console.log('--> [ ' + engine.name + ' ] 실행');

        let param_list = [
            {name: "model", value: "customer1"},
            {name: "userId", value: user_email}
        ];
        // getParamListFromFlowNode(flow_node, param_list);
        console.dir(param_list);

        let formData = new FormData();
        let audio_file = new File([input_data.audio], engine.name + '_file.wav');

        for (let xx = 0; xx < param_list.length; xx++) {
            formData.append(param_list[xx].name, param_list[xx].value);
            console.log("    * ", param_list[xx].name, " : ", param_list[xx].value)
        }

        formData.append('answerText', input_data.answerText);
        formData.append('file', audio_file);

        mvp_request = new XMLHttpRequest();
        mvp_request.responseType = "json";

        mvp_request.open('POST', url);
        mvp_request.send(formData);

        mvp_request.onerror = function () {
            data.errMsg = messages.engineError.output;
            reject(data);
        }
        mvp_request.onreadystatechange = function () {
            if (mvp_request.readyState === 4) {
                console.dir(mvp_request.response);

                if (mvp_request.status === 200) {
                    console.log("    # result >> ", mvp_request.response);

                    if ($.isEmptyObject(mvp_request.response)) {
                        console.log("    # error >> empty response data")
                        data.errMsg = messages.engineError.output;
                        reject(data);
                        return;
                    }
                    data.resp_data = mvp_request.response;
                    resolve(data);
                } else {
                    console.error("Response code : " + mvp_request.status + "\nResponse message : " + mvp_request.statusText);
                    data.errMsg = messages.engineError.output;
                    reject(data);
                    return;
                }
            }

            if (mvp_request.readyState === 0) {
                console.log(engine.name + " Request abort()");
                return false;
            }
        };
    })
}