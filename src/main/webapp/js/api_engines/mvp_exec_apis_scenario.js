/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.scenario] = callApi_Scenario;

/*
** API 실행
*/
function callApi_Scenario(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'text';
        data.engineName = engine.name;

        let scenarioHost = $("option[name=scenarioModel]").parent().find(":selected").val();

        console.log('--> [ Scenario ] 실행');
        console.log('    * scenario type : ', flow_node.paramList[0].value);
        console.log('    * content : ', input_data);
        console.log('    * scenario host : ', scenarioHost);

        mvp_request = $.ajax({
            url : "/mvp/runner/api/scenario",
            method: "POST",
            headers: {
                "contentType" : "application/json;utf-8"
            },
            responseType : "json",
            data: {
                utter: input_data,
                scenarioHost: scenarioHost
            },
            dataType: "text",
            success : function(response) {
                console.log(response);

                if (response === "") {
                    console.log("    # error >> empty response data")
                    data.errMsg = messages.engineError.output;
                    reject(data);
                }
                else {
                    data.resp_data = response;
                    resolve(data);
                }
            },
            error : function(error, exception) {
                if (exception === "abort") {
                    console.log(engine.name + " Request abort()");
                    return false;
                }

                console.log("========> Fail");
                console.log(error.toString());

                data.errMsg = messages.engineError.output;
                reject(data);
            }
        });
    })
}