/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.itf] = callApi_ITF;

/*
** API 실행
*/
function callApi_ITF(input_data, flow_node, engine) {
    return new Promise ((resolve,reject) => {
        let data = {};
        data.input_type = 'json';
        data.engineName = engine.name;

        console.log('--> [ ITF ] 실행');

        mvp_request = $.ajax({
            type: 'post',
            data: {
                "utter": input_data,
                "lang": "ko_KR" //한국어만 지원
            },
            url: '/mvp/runner/api/itf',
            timeout: 20000,
            success: function(result) {
                var output_data = null;
                try {
                    var obj = JSON.parse(result);

                    if ($.isEmptyObject(obj)) {
                        console.log("    # error >> empty response data")
                        data.errMsg = messages.engineError.output;
                        reject(data);
                        return;
                    }

                    console.log("========> OK");
                    console.dir(result);
                    output_data = result.toString();
                } catch (e) {
                    console.log('ITF: 결과 수신 에러');
                    data.errMsg = messages.engineError.output;
                    reject(data);
                }
                data.resp_data = output_data;
                resolve(data);
            },
            error: function(error, exception) {
                if(exception === "abort") {
                    console.log(engine.name + " Request abort()");
                    return false;
                }
                console.log("========> Fail");
                console.log(error.toString());

                data.errMsg = messages.engineError.output;
                reject(data);
            }
        });
    });

}