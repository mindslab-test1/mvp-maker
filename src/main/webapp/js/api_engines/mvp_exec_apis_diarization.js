/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link['화자분리'] = callApi_Diarization;

/*
** API 실행
*/
function callApi_Diarization(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'json`';
        data.engineName = engine.name;
        console.log('--> [ Diarization ] 실행');

        if (Math.round(input_data.size / 1024) > 2048) //업로드 된 파일 사이즈 제한
        {
            console.log(engine.name + "===> Fail , 파일 용량 제한");
            alert("파일의 크기가 2MB를 초과합니다. 다시 시도해 주세요.");

            progressFlowNode(currentWorkingIndex - 1, false);
            ///////////////////////////// 미디어 멈추기
            currentWorkingIndex = workingFlow.nodeList.length;
            reqAbort = false;
            $('.spinner').hide();   // 진행 중을 나타내는 애니메이션 이미지 제거
            $('.api_show').css('border', 'solid 1px #cfd5eb');
            $('.api_bot img').css('opacity', 1);
            $('.api_bot span').css('opacity', 1);

            controlFlow('audio', null);
            afterProc_MVPExec();

        } else {

            var formData = new FormData();
            formData.append('file', input_data);
            // formData.append('${_csrf.parameterName}', '${_csrf.token}');

            mvp_request = new XMLHttpRequest();

            //mvp_request.responseType ="blob";
            mvp_request.onerror = function () {
                data.errMsg = messages.engineError.output;
                reject(data);
            }

            mvp_request.onreadystatechange = function () {
                if (mvp_request.readyState === 4) {

                    if (mvp_request.status === 200) {
                        console.log(mvp_request.response);

                        if($.isEmptyObject(mvp_request.response)){
                            console.log("    # error >> empty response data")
                            data.errMsg = messages.engineError.output;
                            reject(data);
                            return
                        }

                        data.resp_data= mvp_request.response;
                        resolve(data)
                    } else {
                        console.log("request status : " + mvp_request.status);
                        data.errMsg = messages.engineError.output;
                        reject(data)
                        return;
                    }
                }

                if (mvp_request.readyState === 0) {
                    console.log(engine.name + " Request abort()");
                    return false;
                }
            };

            mvp_request.open('POST', '/mvp/runner/api/diarization');
            mvp_request.send(formData);
        }
    })

}