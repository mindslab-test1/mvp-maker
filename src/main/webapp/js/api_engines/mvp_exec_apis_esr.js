/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.esr] = callApi_ESR;

/*
** API 실행
*/
function callApi_ESR(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'image';
        data.engineName = engine.name;

        console.log('--> [ ESR ] 실행');

        var imgUrl = URL.createObjectURL(input_data);

        $('#esrImgView').attr('src', imgUrl);

        var img = document.getElementById('esrImgView');
        var imgWidth = 0;
        var imgHeight = 0;

        img.onload = function () {
            imgWidth = img.width;
            imgHeight = img.height;

            console.log("Image width : " + imgWidth + " , Image height" + imgHeight);

            if (inputSizeValidation(engine.name, Math.round(input_data.size / 1024), 30)) { //30KB
                data.errMsg = messages.mvp_exec_api.error_fileSize_exceed + "30KB";
                reject(data);
            } else if (imgWidth > 200 || imgHeight > 200) {
                console.log(engine.name + "===> Fail , 이미지 크기 제한");
                data.errMsg = "File size is too big."
                reject(data);
            }

            else {
                mvp_request = new XMLHttpRequest();

                var formData = new FormData;
                formData.append('file', input_data);
                formData.append('${_csrf.parameterName}', '${_csrf.token}');

                mvp_request.timeout = 20000;
                mvp_request.responseType = "blob";

                mvp_request.open('POST', '/mvp/runner/api/esr');
                mvp_request.send(formData);

                mvp_request.onerror = function () {
                    data.errMsg = messages.engineError.output;
                    reject(data);
                }
                mvp_request.onreadystatechange = function () {
                    if (mvp_request.readyState === 4) {
                        console.dir(mvp_request.response);

                        if (mvp_request.status === 200) {
                            if($.isEmptyObject(mvp_request.response)){
                                console.log("    # error >> empty response data")
                                data.errMsg = messages.engineError.output;
                                reject(data);
                                return
                            }
                            data.resp_data = new File([mvp_request.response], "ESR.jpg", {type: 'image/jpeg'});
                            resolve(data)
                        } else {
                            console.error("Response code : " + mvp_request.status + "\nResponse message : " + mvp_request.statusText);
                            console.log(engine.name + "========> Fail");
                            data.errMsg = messages.engineError.output;
                            reject(data)
                        }
                    }
                };
            }
        };
    })

}