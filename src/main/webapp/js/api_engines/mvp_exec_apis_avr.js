/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.lic_plate_rec] = callApi_AVR;

/*
** API 실행
*/
function callApi_AVR(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'text';
        data.engineName = engine.name;

        if (inputSizeValidation(engine.name, Math.round(input_data.size / 1024), 2048)) { // 2MB = 2048KB
            data.errMsg = messages.mvp_exec_api.error_fileSize_exceed + "2MB";
            reject(data)
        }
        else {
            console.log('--> [차량번호인식] 실행');

            var formData = new FormData;
            formData.append('file', input_data);
            formData.append('${_csrf.parameterName}', '${_csrf.token}');

            mvp_request = $.ajax({
                type: 'post',
                data: formData,
                url: '/mvp/runner/api/avr',
                timeout: 20000,
                processData: false,
                contentType: false,
                success: function (result) {
                    console.log("AVR result : ");
                    console.dir(result);

                    if ($.isEmptyObject(result)) {
                        console.log("    # error >> empty response data")
                        data.errMsg = messages.engineError.output;
                        reject(data);
                        return;
                    }

                    // TODO: if an image request is not license plate related it will respond with an empty data

                    data.resp_data = result['plt_num'];
                    resolve(data);
                },
                error: function (error, exception) {
                    if (exception === "abort") {
                        console.log(engine.name + " Request abort()");
                        return false;
                    }
                    console.log("========> Fail");
                    console.dir(error);

                    data.errMsg = messages.engineError.output;
                    reject(data);
                }
            });
        }
    });
}