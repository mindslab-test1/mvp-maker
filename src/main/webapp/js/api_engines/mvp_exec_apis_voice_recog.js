/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.voice_rec_view] = callApi_getVoice;
api_func_link[messages.engine.voice_rec_set] = callApi_setVoice;
api_func_link[messages.engine.voice_rec_del] = callApi_delVoice;
api_func_link[messages.engine.voice_rec_rec] = callApi_recogVoice;
/*
** API 실행
*/
function callApi_getVoice(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'json';
        data.engineName = engine.name;
        console.log('--> [ Voice Recog : getVoice ] 실행');
        console.log("===> input_data : " + input_data);
        var param_list = new Array();
        getParamListFromFlowNode(flow_node, param_list);
        console.dir(param_list);
        var formData = new FormData();
        // for(var xx = 0; xx < param_list.length; xx++) {
        //     formData.append(param_list[xx].name, param_list[xx].value);
        //     console.log("    * ", param_list[xx].name, " : ", param_list[xx].value)
        // }
        formData.append('dbId', 'default');
        mvp_request = new XMLHttpRequest();
        mvp_request.responseType ="json";
        mvp_request.open('POST', '/mvp/runner/api/voiceRecog/get');
        mvp_request.send(formData);
        mvp_request.onerror = function() {
            data.errMsg = messages.engineError.output;
            reject(data);
        }
        mvp_request.onreadystatechange = function(){
            if (mvp_request.readyState === 4 ) {
                console.dir(mvp_request.response);
                if(mvp_request.status === 200) {
                    if (mvp_request.response.message.status !== 0) {
                        console.log("    # result >> ", mvp_request.response);
                        data.errMsg = engine.name + " " + messages.engineError.fail;
                        reject(data);
                    }
                    data.resp_data = mvp_request.response;
                    resolve(data);
                }
                else {
                    console.error("Response code : " + mvp_request.status + "\nResponse message : " + mvp_request.statusText);
                    data.errMsg = messages.engineError.output;
                    reject(data);
                }
            }
            if(mvp_request.readyState === 0){
                console.log(engine.name + " Request abort()");
                return false;
            }
        };
    });
}
function callApi_setVoice(input_data, flow_node, engine) {
    return new Promise((resolve,reject) => {
        let data = {};
        data.input_type = 'json';
        data.engineName = engine.name;
        if (inputSizeValidation(engine.name, Math.round(input_data.audio.size / 1024), 2048)) { // 2MB = 2048KB
            data.errMsg = messages.mvp_exec_api.error_fileSize_exceed + "2MB";
            reject(data);
        }
        else {
            console.log('--> [ Voice Recog : setVoice ] 실행');
            console.log("===> input_data : " + input_data);
            var param_list = new Array();
            getParamListFromFlowNode(flow_node, param_list);
            console.dir(param_list);
            let formData = new FormData();
            let audio_file = new File([input_data.audio], 'voiceRecog_file.wav');
            // for(let xx = 0; xx < param_list.length; xx++) {
            //     formData.append(param_list[xx].name, param_list[xx].value);
            //     console.log("    * ", param_list[xx].name, " : ", param_list[xx].value)
            // }
            formData.append('dbId', 'default');
            formData.append('file', audio_file);
            formData.append('voiceId', input_data.voiceId);
            mvp_request = new XMLHttpRequest();
            mvp_request.responseType ="json";
            mvp_request.open('POST', '/mvp/runner/api/voiceRecog/set');
            mvp_request.send(formData);
            mvp_request.onerror = function() {
                console.log("========> Fail");
                data.errMsg = messages.engineError.output;
                reject(data);
            }
            mvp_request.onreadystatechange = function() {
                if (mvp_request.readyState === 4 ) {
                    console.dir(mvp_request.response);
                    if(mvp_request.status === 200) {
                        console.log("    # result >> ", mvp_request.response);
                        data.resp_data = mvp_request.response;
                        resolve(data);
                    }
                    else {
                        data.errMsg = messages.engineError.output;
                        reject(data);
                    }
                }
                if(mvp_request.readyState === 0){
                    console.log(engine.name + " Request abort()");
                    return false;
                }
            };
        }
    });
}
function callApi_delVoice(input_data, flow_node, engine) {
    return new Promise((resolve,reject) => {
        let data = {};
        data.input_type = 'json';
        data.engineName = engine.name;
        console.log('--> [ Voice Recog : delVoice ] 실행');
        console.log("===> input_data : " + input_data);
        var param_list = new Array();
        getParamListFromFlowNode(flow_node, param_list);
        console.dir(param_list);
        let formData = new FormData();
        // for(let xx = 0; xx < param_list.length; xx++) {
        //     formData.append(param_list[xx].name, param_list[xx].value);
        //     console.log("    * ", param_list[xx].name, " : ", param_list[xx].value)
        // }
        formData.append('dbId', 'default');
        formData.append('voiceId', input_data);
        mvp_request = new XMLHttpRequest();
        mvp_request.responseType ="json";
        mvp_request.open('POST', '/mvp/runner/api/voiceRecog/delete');
        mvp_request.send(formData);
        mvp_request.onerror = function() {
            console.log("========> Fail");
            data.errMsg = messages.engineError.output;
            reject(data);
        }
        mvp_request.onreadystatechange = function(){
            if (mvp_request.readyState === 4 ) {
                console.dir(mvp_request.response);
                if(mvp_request.status === 200) {
                    console.log("    # result >> ", mvp_request.response);
                    if (mvp_request.response.message.status !== 0) {
                        data.errMsg = engine.name + " " + messages.engineError.fail;
                        reject(data);
                    }
                    data.resp_data = mvp_request.response;
                    resolve(data);
                }
                else {
                    data.errMsg = messages.engineError.output;
                    reject(data);
                }
            }
            if(mvp_request.readyState === 0){
                console.log(engine.name + " Request abort()");
                return false;
            }
        };
    });
}
function callApi_recogVoice(input_data, flow_node, engine) {
    return new Promise((resolve,reject) => {
        let data = {};
        data.input_type = 'text';
        data.engineName = engine.name;
        if (inputSizeValidation(engine.name, Math.round(input_data.size / 1024), 2048)) { // 2MB = 2048KB
            data.errMsg = messages.mvp_exec_api.error_fileSize_exceed + "2MB";
            reject(data);
        }
        else {
            console.log('--> [ Voice Recog : recogVoice ] 실행');
            console.log("===> input_data : " + input_data);
            var param_list = new Array();
            getParamListFromFlowNode(flow_node, param_list);
            console.dir(param_list);
            let formData = new FormData();
            let audio_file = new File([input_data], 'voiceRecog_file.wav');
            formData.append('file', audio_file);
            // for(let xx = 0; xx < param_list.length; xx++) {
            //     formData.append(param_list[xx].name, param_list[xx].value);
            //     console.log("    * ", param_list[xx].name, " : ", param_list[xx].value)
            // }
            formData.append('dbId', 'default');
            mvp_request = new XMLHttpRequest();
            mvp_request.responseType ="json";
            mvp_request.open('POST', '/mvp/runner/api/voiceRecog/recog');
            mvp_request.send(formData);
            mvp_request.onerror = function() {
                console.log("========> Fail");
                data.errMsg = messages.engineError.output;
                reject(data);
            }
            mvp_request.onreadystatechange = function(){
                if (mvp_request.readyState === 4 ) {
                    console.dir(mvp_request.response);
                    if(mvp_request.status === 200) {
                        console.log("    # result >> ", mvp_request.response);
                        data.resp_data = mvp_request.response.result.id;
                        resolve(data);
                    }
                    else {
                        data.errMsg = messages.engineError.output;
                        reject(data);
                    }
                }
                if(mvp_request.readyState === 0){
                    console.log(engine.name + " Request abort()");
                    return false;
                }
            };
        }
    });
}