/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.text_rem] = callApi_TXTR;

/*
** API 실행
*/
function callApi_TXTR(input_data, flow_node, engine) {
    return new Promise ((resolve,reject) => {
        let data = {};
        data.input_type = 'image';
        data.engineName = engine.name;

        console.log('--> [ TextRemoval ] 실행');

        if (inputSizeValidation(engine.name,  Math.round(input_data.size / 1024),2048)) { // 2MB = 2048KB
            data.errMsg = messages.mvp_exec_api.error_fileSize_exceed + "2MB";
            reject(data);
        }
        else {
            mvp_request = new XMLHttpRequest();

            var formData = new FormData;
            formData.append('file', input_data);
            formData.append('${_csrf.parameterName}', '${_csrf.token}');

            mvp_request.timeout = 20000;
            mvp_request.responseType = "blob";

            mvp_request.open('POST', '/mvp/runner/api/textRemoval');
            mvp_request.send(formData);

            mvp_request.onerror = function() {
                console.log("========> Fail");
                data.errMsg = messages.engineError.output;
                reject(data);
            }
            mvp_request.onreadystatechange = function () {
                if (mvp_request.readyState === 4) {
                    console.dir(mvp_request.response);
                    if (mvp_request.status === 200) {
                        data.resp_data = new File([mvp_request.response], "txt_removal.jpg",{type:'image/jpeg'});
                        resolve(data);
                    }
                    else {
                        console.log(engine.name + "========> Fail");
                        data.errMsg = messages.engineError.output;
                        reject(data);
                    }
                }
            };
        }
    });
}