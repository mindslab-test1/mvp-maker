/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.chatbot] = callApi_Chat;

/*
** API 실행
*/
function callApi_Chat(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'text';
        data.engineName = engine.name;

        let chat_model = flow_node.paramList[0].value;
        console.log('--> [ 챗봇 ] 실행');
        console.log('    * model : ', chat_model);
        console.log('    * question : ', input_data);

        mvp_request = $.ajax({
            url: '/mvp/runner/api/chatbot',
            type: 'post',
            headers: {
                "contentType": "application/json;utf-8"
            },
            data: {
                "question" : input_data,
                "ntop": 1,
                "domain" : chat_model
            },
            timeout: 20000,
            success: function (response) {
                if ($.isEmptyObject(response)) {
                    console.log("    # error >> empty response data")
                    data.errMsg = messages.engineError.output;
                    reject(data);
                    return;
                }

                data.resp_data = response.payload.answer;
                resolve(data);
            },
            error: function (error, exception) {
                if (exception === "abort") {
                    console.log(engine.name + " Request abort()");
                    return false;
                }
                console.log("========> Fail");
                console.log(error.toString());

                data.errMsg = messages.engineError.output;
                reject(data);
            }
        });
    })

}
