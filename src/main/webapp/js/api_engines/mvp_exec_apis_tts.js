/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.tts] = callApi_TTS;

/*
** API 실행
*/
function callApi_TTS(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'audio';
        data.engineName = engine.name;

        if (inputSizeValidation(engine.name, input_data.length, 1000)) {
            data.errMsg = messages.mvp_exec_api.error_charLimit_exceed + "1000";
            reject(data);
        }
        else {
            var param_list = new Array();
            getParamListFromFlowNode(flow_node, param_list);
            console.dir(param_list);

            console.log('--> [ TTS ] 실행');
            console.log('    * input : ', input_data);
            console.log('    * voiceName : ', param_list[0].value);

            var formData = new FormData();

            formData.append('text', input_data);
            formData.append('voiceName', param_list[0].value);

            mvp_request = new XMLHttpRequest();
            mvp_request.responseType = "blob";

            mvp_request.open('POST', '/mvp/runner/api/tts');
            mvp_request.send(formData);

            mvp_request.onerror = function () {
                console.log("========> Fail");
                data.errMsg = messages.engineError.output;
                reject(data);
            }

            mvp_request.onreadystatechange = function () {
                try {
                    console.log(mvp_request)
                    if (mvp_request.readyState === 4) {
                        console.dir(mvp_request.response);
                        if (mvp_request.status === 200) {
                            if (mvp_request.response === null) {
                                data.errMsg = messages.engineError.output;
                                reject(data);
                                return false;
                            }
                            if (mvp_request.response.size > 0) {
                                var sample1SrcUrl = URL.createObjectURL(mvp_request.response);
                            } else {
                                console.log("    # error >> response size 0 --> 416 Range Not Satisfiable ")
                                data.errMsg = messages.engineError.output;
                                reject(data)
                            }

                            resetAudio(); // empties any existing information on the audio player
                            newAudioSetup(sample1SrcUrl); // sets up audio player with new source and plays it

                            $("#AudioPlayer_TTS").off('ended').on('ended', function () {
                                data.resp_data = mvp_request.response;
                                resolve(data);
                            });
                        } else {
                            console.error("Response code : " + mvp_request.status + "\nResponse message : " + mvp_request.statusText);
                            data.errMsg = messages.engineError.output;
                            reject(data)
                        }
                    }

                    if (mvp_request.readyState === 0) {
                        console.log(engine.name + " Request abort()");
                        return false;
                    }

                } catch (e) {
                    console.log(e);
                }
            };
        }
    });
}

function resetAudio() {
    $("#AudioPlayer_TTS")[0].pause();
    $('#AudioSource_TTS')[0].removeAttribute('src');
    $("#AudioPlayer_TTS")[0].load();
}

function newAudioSetup(srcUrl) {
    activeMedia.elem = $("#AudioPlayer_TTS")[0];
    $("#AudioSource_TTS").attr("src", srcUrl);
    $("#AudioPlayer_TTS")[0].pause();
    $("#AudioPlayer_TTS")[0].load();
    $("#AudioPlayer_TTS")[0].oncanplaythrough = function () {
        if (!reqAbort) {
            activeMedia.prom = $("#AudioPlayer_TTS")[0].play();
        }
    };
}


