/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.pose_rec] = callApi_PoseExtract;

/*
** API 실행
*/
function callApi_PoseExtract(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'image';
        data.engineName = engine.name;

        if (inputSizeValidation(engine.name,Math.round(input_data.size / 1024),2048)) { // 2MB = 2048KB
            data.errMsg = messages.mvp_exec_api.error_fileSize_exceed + "2MB";
            reject(data);
        } else {
            console.log('--> [ PoseExtract ] 실행');
            var formData = new FormData;
            formData.append('pose_img', input_data);
            formData.append('${_csrf.parameterName}', '${_csrf.token}');

            mvp_request = $.ajax({
                type: "POST",
                async: true,
                url: '/mvp/runner/api/poseExtract',
                data: formData,
                processData: false,
                contentType: false,
                success: function (result) {
                    let resultData = JSON.parse(result);

                    if ($.isEmptyObject(resultData)) {
                        console.log("    # error >> empty response data")
                        data.errMsg = messages.engineError.output;
                        reject(data);
                        return
                    }

                    let bstr = atob(resultData.result_img),
                        n = bstr.length,
                        u8arr = new Uint8Array(n);
                    while (n--) {
                        u8arr[n] = bstr.charCodeAt(n);
                    }
                    data.resp_data = new File([u8arr], "pose_extract.jpg", {type: "image/jpeg"});
                    resolve(data);
                },
                error: function (jqXHR, error) {
                    if (jqXHR.status === 0) {
                        return false;
                    }
                    data.errMsg = messages.engineError.output;
                    reject(data);
                }
            });
        }
    });
}