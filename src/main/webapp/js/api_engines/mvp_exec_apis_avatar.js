/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.avatar] = callApi_Avatar;


/*
** API 실행
*/

let avatarKey;
let callAvatar;
let tryCnt;
let abort_statusCheck = false;

function callApi_Avatar(input, flow_node, engine, nodeIndex) {

    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'video';
        data.engineName = engine.name;

        // TODO: Check text length < 50 chars

        console.log('--> [ Lip Sync Avatar ] 실행');
        console.log('    * text : ', input);
        let divId = '#Layer_Engine_Param' + nodeIndex;
        let param_list = new Array();

        let watermark = null;
        $(divId + ' .select_list .select img').each(function () {
            let attr = $(this).attr('data-value');
            param_list.push(attr);

            if ($(this).attr('data-watermark')) {
                watermark = $(this).attr('data-watermark');
            }
        });

        let modelParam = param_list[0];
        // let imageParam = param_list[1];
        let imageParam = watermark;
        let imageType = imageParam.split('.')[1];
        console.log("Selected Model: " + modelParam);
        console.log("Selected Background: " + imageParam);

        fetch(imageParam)
            .then(function (response) {
                return response.blob()
            })
            .then(function (blob) {
                let imageData = new File([blob], imageParam, {type: "img/" + imageType});
                let formData = new FormData();

                formData.append('text', input);
                formData.append('model', modelParam);
                if (imageParam == 'bg_checkered.png') {
                    formData.append('image', null);
                    formData.append('transparent', 'true');
                } else {
                    formData.append('image', imageData);
                }

                mvp_request = $.ajax({
                    url: "/mvp/runner/api/avatar",
                    type: "POST",
                    data: formData,
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (response) {
                        console.dir(response)
                        avatarKey = response;
                        runAvatar().then(function () {

                            downloadAvatar().then(function (avatar_data) {
                                // data.resp_data = "https://api.maum.ai/lipsync/download?requestKey=" + avatarKey;
                                data.resp_data = avatar_data
                                resolve(data);
                            })
                        });
                    },
                    error: function (err) {
                        console.error(err);
                    }
                });
            })

    })
}

function runAvatar() {
    return new Promise(function (resolve, reject) {
        tryCnt = 0;
        callAvatar = setInterval(
            // callAvatarApi,
            function () {
                if (abort_statusCheck) {
                    clearInterval(callAvatar);
                    abort_statusCheck = false;
                } else {
                    tryCnt++;
                    let data = {
                        // TODO: apiId, apiKey hard coded
                        "apiId": "curogom-mindslab",
                        "apiKey": "96395eb75d3943cbadd28c8da50292ab",
                        "requestKey": avatarKey
                    }

                    mvp_request_statusCheck = $.ajax({
                        url: "https://api.maum.ai/lipsync/statusCheck",
                        type: "POST",
                        data: JSON.stringify(data),
                        contentType: "application/json"
                    }).done(function (resultData) {
                        console.dir(resultData);
                        let statusCode = resultData.payload.statusCode;
                        if (statusCode === 2) {
                            console.log("    # succ >> avatar video created");
                            clearInterval(callAvatar);
                            console.log('tried : ' + tryCnt);
                            console.log("resolving...");
                            resolve();
                            // resolve("https://api.maum.ai/lipsync/download?requestKey=" + avatarKey);
                        }
                    });
                }
            },
            1000
        );

    })

}

function downloadAvatar() {
    return new Promise(function (resolve, reject) {
        let formData = new FormData();
        formData.append('key', avatarKey);

        $.ajax({
            url: "/mvp/runner/api/avatar-url",
            type: "POST",
            data: formData,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                resolve(data);
            },
            error: function (err){
                console.error(err);
            }
        });
    })
}