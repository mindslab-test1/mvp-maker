/*
** 아래 배열이 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.nlu] = callApi_NLU;

/*
** API 실행
*/
function callApi_NLU(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'json';
        data.engineName = engine.name;

        console.log('--> [ NLU ] 실행');
        console.log('    * lang : ', flow_node.paramList[0].value);
        console.log('    * content : ', input_data);

        mvp_request = $.ajax({
            url : "/mvp/runner/api/nlu",
            method : "POST",
            headers : {
                "contentType" : "application/json;utf-8"
            },
            responseType : "json",
            data : {
                reqText: input_data,
                lang: flow_node.paramList[0].value
            },
            success : function(response){
                console.log(response);

                if ($.isEmptyObject(response)) {
                    console.log("    # error >> empty response data")
                    data.errMsg = messages.engineError.output;
                    reject(data);
                    return;
                }

                if(response['message']['status'] !== 0) {
                    data.errMsg = messages.engineError.fail;
                    reject(data);
                }
                else {
                    data.resp_data = response;
                    resolve(data);
                }
            },
            error : function(error, exception){
                if(exception === "abort") {
                    console.log(engine.name + " Request abort()");
                    return false;
                }

                console.log("========> Fail");
                console.log(error.toString());

                data.errMsg = messages.engineError.output;
                reject(data);
            }
        });
    })
}