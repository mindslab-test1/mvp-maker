/*
** 아래 배열을 꼭 등록해줘야 함.
*/
api_func_link[messages.engine.hotel_conc_chatbot] = callApi_hotelChatbot;

/*
** API 실행
*/
function callApi_hotelChatbot(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'text';
        data.engineName = engine.name;

        console.log('--> [Hotel chatbot] 실행');
        console.log('    * input_data : ', input_data);

        mvp_request = $.ajax({
            url: '/mvp/runner/api/hotelChatbot',
            type: 'post',
            headers: {
                "contentType": "application/json;utf-8"
            },
            data: {
                "utter": input_data,
            },
            timeout: 20000,
            success: function (response) {
                if ($.isEmptyObject(response)) {
                    console.log("    # error >> empty response data")
                    data.errMsg = messages.engineError.output;
                    reject(data)
                    return
                }

                data.resp_data = response.answer['answer']
                resolve(data)
            },
            error: function (error, exception) {
                if (exception === "abort") {
                    console.log(engine.name + " Request abort()");
                    return false;
                }
                console.log("========> Fail");
                console.log(error.toString());

                data.errMsg = messages.engineError.output;
                reject(data)
            }
        });
    })

}