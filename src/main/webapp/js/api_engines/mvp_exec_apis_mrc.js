/*
** 아래 배열이 꼭 등록되어야 함.
*/
api_func_link[messages.engine.mrc] = callApi_MRC;

/*
** API 실행
*/
function callApi_MRC(input_data, flow_node, engine) {
    return new Promise((resolve, reject) => {
        let data = {};
        data.input_type = 'text';
        data.engineName = engine.name;

        console.log('--> [ MRC ] 실행');
        console.log('    * lang : ', flow_node.paramList[0].value);
        console.log('    * input_data : ', input_data);

        mvp_request = $.ajax({
            url: "/mvp/runner/api/mrc",
            method: "POST",
            headers: {
                "contentType": "application/json;utf-8"
            },
            responseType: "json",
            data: {
                lang: flow_node.paramList[0].value,
                context: input_data.context,
                question: input_data.question
            },
            success: function (response) {
                let resp_data = JSON.parse(response);
                console.log('json', response);

                if ($.isEmptyObject(resp_data)) {
                    console.log("    # error >> empty response data")
                    data.errMsg = messages.engineError.output;
                    reject(data);
                    return;
                }

                if (resp_data['message']['status'] !== 0) {
                    data.errMsg = messages.engineError.fail;
                    reject(data);
                } else {
                    data.resp_data = JSON.parse(response)['answer'];
                    resolve(data);
                }
                init_MRC_Popup_UI();
            },
            error: function (error, exception) {
                if (exception === "abort") {
                    console.log(engine.name + " Request abort()");
                    return false;
                }
                console.log("========> Fail");
                console.log(error.toString());

                data.errMsg = messages.engineError.output;
                reject(data);
            }
        });
    });
}