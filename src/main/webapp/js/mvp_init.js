const TIMEOUT___MVP_API = 6000;

/* ===================================================================================================================== */
// 전역 변수 정의
/* ===================================================================================================================== */

var engineGrpList = new Array();    // API 엔진 그룹 목록
var engineList = new Array();       // API 엔진 목록


/* ===================================================================================================================== */
// 초기 작업
/* ===================================================================================================================== */

function initTemplate(template, flag) {
    initEngine();
    switch (template) {
        case 'ai_humanSpeech' :
            if( flag == 'read' ) {
                callFlow(flowId);
            } else {
                history.pushState(null, null, location.href + '&flag=read');
                callAppFlow(template);
                modifyUserFlow(false);
            }
            break;
        case 'none' :
            callFlow(flowId);
            break;
    }
}