/* ===================================================================================================================== */
// FLOW NODE 객체
/* ===================================================================================================================== */

function Flow(name) {
    this.flowId = 0;
    this.userNo = 0;
    this.name = name;
    this.nodeList = new Array();
}

function FlowNode(engine_id) {
    this.flowNodeId = 0;
    this.flowId = 0;
    this.engineId = engine_id;
    this.paramList = new Array();
    this.inputMethod = 'text';      // text, upload, mic, camera, flow
}

function Param(name, value) {
    this.name = name;
    this.value = value;
}


/* ===================================================================================================================== */
// FLOW 등록 관리
/* ===================================================================================================================== */

var workingFlow = new Flow('test1');

function addStt() {
    var flow_node = new FlowNode(2);
    flow_node.inputMethod = 'mic';
    var param = new Param("model", "한글 고객센터용 8K");
    flow_node.paramList.push(param);

    workingFlow.nodeList.push(flow_node);
}

function addGpt() {
    var flow_node = new FlowNode(14);
    flow_node.inputMethod = 'flow';
    var param = new Param("lang", "Korean");
    flow_node.paramList.push(param);

    workingFlow.nodeList.push(flow_node);
}

function addTts() {
    var flow_node = new FlowNode(1);
    flow_node.inputMethod = 'flow';
    var param = new Param("voiceName", "baseline_kor");
    flow_node.paramList.push(param);

    workingFlow.nodeList.push(flow_node);
}

function runTest() {
    addStt();
    addGpt();
    addTts();

    // var json_data = JSON.stringify(workingFlow);
    // console.log("========> ORG: ", json_data);

    $.ajax({
        type: 'post',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(workingFlow),
        url: '/mvp/api/Flows',
        timeout: TIMEOUT___MVP_API,
        success: function (result) {
            console.log("========> OK");
            console.dir(result);

            /* 업데이트 정보 반영된 객체로 변경 */
            workingFlow = result;
        },
        error: function (error) {
            console.log("========> Fail");
            console.log(error.toString());
        }
    });
}

runTest();


/* ===================================================================================================================== */
// FLOW UI 관리
/* ===================================================================================================================== */


















