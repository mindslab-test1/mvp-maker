$(document).ready(function () {

    /**
     * validLicense
     */
    $("#license_input").keyup(function () {
        let inputLength = $(this).val().length;
        if (inputLength == 19) {
            $.ajax({
                url: "/license/valid",
                type: "post",
                contentType: "application/json",
                data: JSON.stringify({
                    licenseKey: $(this).val()
                }),
                success: function (result) {
                    if (result.code == 200) {
                        $("#license_valid_display").removeClass("error").addClass("success");
                        $("#license_input").attr("disabled", "disabled");
                    } else {
                        $("#license_valid_display").removeClass("success").addClass("error");
                    }
                },
                error: function (jqXHR) {
                    $("#license_valid_display").removeClass("success").addClass("error");
                    // TODO => message.properties 분리
                    alert("일시적 서버 오류입니다. 잠시 후 다시 시도해주세요.");
                }
            });
        } else {
            $("#license_valid_display").removeClass("success").addClass("error");
        }
    });

    function resetInputLicense() {
        $("#license_input").val("");
        $("#license_input").removeAttr("disabled");
        $("#license_valid_display").removeClass("success").removeClass("error");
    }

    /**
     * insertLicense
     */
    $("#license_btn_ok").click(function () {
        const licenseVal = $("#license_input").val();
        if (licenseVal.length != 19) {
            alert("라이센스를 입력해주세요.")
            $("#license_input").focus();
            return false;
        }

        if ($("#license_valid_display").hasClass('error')) {
            return false;
        }

        $.ajax({
            url: "/license/insert",
            type: "post",
            contentType: "application/json",
            data: JSON.stringify({
                licenseKey: licenseVal
            }),
            success: function (result) {
                let alertMessage = lang_code == 'ko' ? 'License registration was successful.' : '라이선스 등록에 성공하였습니다.';
                if (result.code === 200) {
                    $("body").css("overflow", "");
                    $(".lyr_bg").fadeOut(800).remove();
                    $("body").find("#pop_license_add").unwrap('<div class="lyrWrap"></div>');
                    setTimeout(() => {
                        alert(alertMessage);
                        window.location.reload();
                    }, 300);
                } else if (result.code === 300) {
                    alert(lang_code == 'ko' ? 'This license key has already been registered.' : '이미 등록된 라이센스 키 입니다.');
                    $("body").css("overflow", "");
                    $(".lyr_bg").fadeOut(800).remove();
                    $("body").find("#pop_license_add").unwrap('<div class="lyrWrap"></div>');
                } else {
                    alert(lang_code == 'ko' ? 'License registration failed.' : '라이센스 등록에 실패하였습니다.');
                    $("body").css("overflow", "");
                    $(".lyr_bg").fadeOut(800).remove();
                    $("body").find("#pop_license_add").unwrap('<div class="lyrWrap"></div>');
                }
            },
            error: function (jqXHR) {
                alert("일시적 서버 오류입니다. 잠시 후 다시 시도해주세요.");
            },
            complete: function () {
                resetInputLicense();
            }
        })

    });


});