function engineIsLogicBox(engine) { // 현재 다루는 엔진이 로직박스인지 확인
    return engine.engineId === 1053;
}

function isNullOrWhitespace(input) { // 텍스트가 비어있거나 스페이스로만 구성된 경우를 그렇지 않은 텍스트와 구별
    return !input || !input.trim();
}

function isIncompleteKeyVal(elemPath) { // key-value 페어에 둘중 하나가 빠진경우를 구별
    let bool = false;
    $(elemPath).each(function () {
        let checkBox = $(this).find('input[type="checkbox"]').prop("checked"),
            key = isNullOrWhitespace($(this).find('input.key').val()),
            val = isNullOrWhitespace($(this).find('input.value').val());
        if (checkBox && ((key && !val) || (!key && val))) {
            bool = true;
        }
    });
    return bool;
}

function isIncompleteBody(bodyId) { // request의 JSON body가 비어있는지 구별
    return isNullOrWhitespace($(bodyId).val());
}

function openLogicBoxRequestForm(obj, nodeIdx) { // Request 팝업창 UI와 기능 설정
    // Request popup 창 UI와 버튼 설정
    let winHeight = $(window).height() * 0.7,
        hrefId = $(obj).attr('href');

    $('body').css('overflow', 'hidden');
    $('body').find(hrefId).wrap('<div class="lyrWrap"></div>');
    $('body').find(hrefId).before('<div class="lyr_bg"></div>');

    $('.lyrWrap .lyrBox .lyr_mid .error').removeClass('active').html('');

    //mid max-height
    $('.lyrWrap .lyrBox .lyr_mid').each(function () {
        $(obj).css('max-height', Math.floor(winHeight) + 'px');
    });

    let contType = $('.lyrWrap .lyrBox .request_contType_value').find('option:selected').attr('value');
    if (contType === 'application/x-www-form-urlencoded') { // 처음 팝업을 오픈시 content-type에 따라 필요한 인풋박스만 보여줌
        $('.lyrWrap .lyrBox .request_params_table').show();
        $('.lyrWrap .lyrBox .request_body_table').hide();
    } else {
        $('.lyrWrap .lyrBox .request_params_table').hide();
        $('.lyrWrap .lyrBox .request_body_table').show();
    }

    $('.lyrWrap .lyrBox .request_contType_value').on('change', function () {
        // Content Type이 변경될때마다 필요한 인풋박스만 보여준다
        $(this).find('option:selected').each(function () {
            let optionValue = $(this).attr('value');
            if (optionValue === 'application/x-www-form-urlencoded') {
                $('.lyrWrap .lyrBox .request_params_table').show();
                $('.lyrWrap .lyrBox .request_body_table').hide();
            } else {
                $('.lyrWrap .lyrBox .request_params_table').hide();
                $('.lyrWrap .lyrBox .request_body_table').show();
            }
        })
    });
    $('button.btn_request_headers_addRow').on('click', function () {
        let idx = $(this).parents('.request_headers_table').find('tr').length;
        // Request popup창의 Header에 열을 추가하는 버튼
        let newRow =
            '<tr class="request_headers">' +
            '<td class="logicBoxIndent"></td>' +
            '<td class="checkbox"><input type="checkbox" id="header_' + nodeIdx + '_' + idx + '" name="headerRow" value="1" >' +
            '<label class="check" for="header_' + nodeIdx + '_' + idx + '"></label></td>' +
            '<td><input class="key" type="text"/></td>' +
            '<td><input class="value" type="text"/></td>' +
            '<td><input class="desc" type="text"/></td>' +
            '<td class="delete"><em class="fas fa-times btn_request_deleteRow"></em></td>' +
            '</tr>';
        $('.lyrWrap .lyrBox .request_headers_table').append(newRow);

        $('.btn_request_deleteRow').on('click', function () {
            // Request popup창의 Header에 열을 삭제하는 버튼
            $(this).parent().parent('tr').remove();
        });
    });

    $('button.btn_request_params_addRow').on('click', function () {
        let idx = $(this).parents('.request_params_table').find('tr').length;
        // Request popup창의 Params에 열을 추가하는 버튼
        let newRow =
            '<tr class="request_params">' +
            '<td class="logicBoxIndent"></td>' +
            '<td class="checkbox"><input type="checkbox" id="param_' + nodeIdx + '_' + idx + ' " name="paramRow" value="1" >' +
            '<label class="check" for="param_' + nodeIdx + '_' + idx + '"></label></td>' +
            '<td><input class="key" type="text"/></td>' +
            '<td><input class="value" type="text"/></td>' +
            '<td><input class="desc" type="text"/></td>' +
            '<td class="delete"><em class="fas fa-times btn_request_deleteRow"></em></td>' +
            '</tr>';
        $('.lyrWrap .lyrBox .request_params_table').append(newRow);

        $('.btn_request_deleteRow').on('click', function () {
            // Request popup창의 열을 삭제하는 버튼
            $(this).parent().parent('tr').remove();
        });
    });

    //Layer popup close
    $('.req_form_done, .req_form_close, .lyr_bg').on('click', function () { //요청 정보가 미완성일 경우 에러메세지 출력
        let contType = $('.lyrWrap .lyrBox .request_contType_value').val()
        if (isIncompleteKeyVal('.lyrWrap .lyrBox .request_headers_table tr.request_headers')) {
            $('.lyrWrap .lyrBox .lyr_mid .error').addClass('active').html(messages.logicBox.reqHeaderErr);
        } else if (contType === 'application/x-www-form-urlencoded' && isIncompleteKeyVal('.lyrWrap .lyrBox .request_params_table tr.request_params')) {
            $('.lyrWrap .lyrBox .lyr_mid .error').addClass('active').html(messages.logicBox.reqParamErr);
        } else if (contType === 'application/json' && isIncompleteBody('.lyrWrap .lyrBox .request_body_content')) {
            $('.lyrWrap .lyrBox .lyr_mid .error').addClass('active').html(messages.logicBox.reqBodyErr);
        } else {
            $('.lyrWrap .lyrBox .request_contType_value').off('change');
            $('button.btn_request_headers_addRow').off('click');
            $('button.btn_request_params_addRow').off('click');
            $('.req_form_done, .req_form_close, .lyr_bg').off('click');
            $('body').css('overflow', '');
            $('body').find(hrefId).unwrap('<div class="lyrWrap"></div>');
            $('.lyr_bg').remove();
        }
    });

    // 닫기버튼이 팝업 내용을 모두 제거해주는 기능 (현재 미사용)
    // $('#req_form_close, .lyr_bg').on('click', function() {
    //     $('#LogicBox_Form_Exit_Popup').fadeIn();
    //     $('#LogicBox_Form_Exit_Content').text(messages.logicBox.formClose);
    //     $('#LogicBox_Form_Exit_Confirm').on('click', function () {
    //         $('#request_headers_table tr.request_headers').each(function() {
    //             $(this).find('input.key').val('');
    //             $(this).find('input.value').val('');
    //         })
    //         $('#request_params_table tr.request_params').each(function() {
    //             $(this).find('input.key').val('');
    //             $(this).find('input.value').val('');
    //         })
    //         $('#request_body_content').val('');
    //         $('#LogicBox_Form_Exit_Popup').fadeOut();
    //         $('body').css('overflow', '');
    //         $('body').find(hrefId).unwrap('<div class="lyrWrap"></div>');
    //         $('.lyr_bg').remove();
    //     })
    //     $('#LogicBox_Form_Exit_Cancel').on('click', function() {
    //         $('#LogicBox_Form_Exit_Popup').fadeOut();
    //     })
    // });
}

function openLogicBoxResponseForm(obj, nodeIdx) {
    // Response popup 창 UI와 버튼 설정
    let winHeight = $(window).height() * 0.7,
        hrefId = $(obj).attr('href');

    $('body').css('overflow', 'hidden');
    $('body').find(hrefId).wrap('<div class="lyrWrap"></div>');
    $('body').find(hrefId).before('<div class="lyr_bg"></div>');

    //mid max-height
    $('.lyrWrap .lyrBox .lyr_mid').each(function () {
        $(obj).css('max-height', Math.floor(winHeight) + 'px');
    });

    $('.lyrWrap .lyrBox .lyr_mid .error').removeClass('active').html('');

    $('button.btn_response_output_addRow').on('click', function () {
        let idx = $(this).parents('.response_output_table').find('tr').length;
        // Response popup창의 Output에 열을 추가하는 버튼
        let newRow =
            '<tr class="response_outputs">' +
            '<td class="logicBoxIndent"></td>' +
            '<td class="checkbox"><input type="checkbox" id="response_' + nodeIdx + '_' + idx + '" name="response" value="1" >' +
            '<label class="check" for="response_' + nodeIdx + '_' + idx + '"></label></td>' +
            '<td><input class="key" type="text"/></td>' +
            '<td><input class="value" type="text"/></td>' +
            '<td class="delete"><em class="fas fa-times btn_request_deleteRow"></em></td>' +
            '</tr>';
        $('.lyrWrap .lyrBox .response_output_table').append(newRow);

        $('.btn_request_deleteRow').on('click', function () {
            // response popup창의 열을 삭제하는 버튼
            $(this).parent().parent('tr').remove();
        });
    });

    //Layer popup close
    $('.resp_form_done, .resp_form_close, .lyr_bg').on('click', function () { // 응답정보가 미완성일 경우 에러메세지 출력
        if (isIncompleteKeyVal('.lyrWrap .lyrBox .response_output_table tr.response_outputs')) {
            $('.lyrWrap .lyrBox .lyr_mid .error').addClass('active').html(messages.logicBox.respOutputErr);
        } else {
            $('button.btn_response_output_addRow').off('click');
            $('.resp_form_done, .resp_form_close, .lyr_bg').off('click');
            $('body').css('overflow', '');
            $('body').find(hrefId).unwrap('<div class="lyrWrap"></div>');
            $('.lyr_bg').remove();
        }
    });

    // 닫기버튼이 팝업 내용을 모두 제거해주는 기능 (현재 미사용)
    // $('#req_form_close, .lyr_bg').on('click', function() {
    //     $('#LogicBox_Form_Exit_Popup').fadeIn();
    //     $('#LogicBox_Form_Exit_Content').text(messages.logicBox.formClose);
    //     $('#LogicBox_Form_Exit_Confirm').on('click', function () {
    //         $('#response_output_table tr.response_outputs').each(function() {
    //             $(this).find('input.key').val('');
    //             $(this).find('input.value').val('');
    //         })
    //         $('#LogicBox_Form_Exit_Popup').fadeOut();
    //         $('body').css('overflow', '');
    //         $('body').find(hrefId).unwrap('<div class="lyrWrap"></div>');
    //         $('.lyr_bg').remove();
    //     })
    //     $('#LogicBox_Form_Exit_Cancel').on('click', function() {
    //         $('#LogicBox_Form_Exit_Popup').fadeOut();
    //     })
    // })
}

function logicBoxPopupSetting() { // flow에 추가된 로직박스 node 하나당 팝업 element 추가
    let paramIndex = workingFlow.nodeList.length + 1;

    let requestHTML = // 요청 정보 팝업 설정
        "<div id='pop_request_add{0}' class='logicBox_req_form lyrBox logic_box pop_request_add'>\n" +
        "    <div class='lyr_top'>\n" +
        "        <h3>" + messages.logicBox.request_title + "</h3>\n" +
        "        <button class='req_form_close btn_lyr_close'>" + messages.logicBox.close + "</button>\n" +
        "    </div>\n" +
        "    <div class='lyr_mid'>\n" +
        "        <table class='request_headers_table'>\n" +
        "            <caption>request_headers_table</caption>\n" +
        "            <colgroup>\n" +
        "                <col class=\"col_1\">\n" +
        "                <col class=\"col_2\">\n" +
        "                <col class=\"col_3\">\n" +
        "                <col class=\"col_4\">\n" +
        "                <col class=\"col_5\">\n" +
        "                <col class=\"col_6\">\n" +
        "            </colgroup>\n" +
        "            <thead>" +
        "            <tr class='request_header_title'>\n" +
        "                <th>" + messages.logicBox.request_headers + "</th>\n" +
        "                <th></th>" +
        "                <th>" + messages.logicBox.request_key + "</th>\n" +
        "                <th>" + messages.logicBox.request_value + "</th>\n" +
        "                <th>" + messages.logicBox.request_desc + "</th>\n" +
        "                <th></th>" +
        "            </tr>\n" +
        "            </thead>" +
        "            <tbody>" +
        "            <tr class='request_content_type'>\n" +
        "                <td class='logicBoxIndent'><button type='button' class='btn_request_headers_addRow'><em class=\"fas fa-plus-circle\"></em>" + messages.logicBox.add_row + "</button></td>\n" +
        "                <td class=\"checkbox\">\n" +
        "                   <input type=\"checkbox\" id=\"header_{0}_1\" name=\"headerRow\" value=\"header\" checked>\n" +
        "                   <label class=\"check\" for=\"header_{0}_1\"></label>\n" +
        "                </td>" +
        "                <td><input type='text' placeholder='Content Type' disabled></td>\n" +
        "                <td>\n" +
        "                    <select class='request_contType_value'>\n" +
        "                        <option value='application/x-www-form-urlencoded'>application/x-www-form-urlencoded</option>\n" +
        "                        <option value='application/json'>application/json</option>\n" +
        "                    </select>\n" +
        "                </td>\n" +
        "                <td>\n" +
        "                    <input class='desc' type='text'>\n" +
        "                </td>\n" +
        "            <td class=\"delete\"></td>\n" +
        "            </tr>\n" +
        "            <tr class='request_headers'>\n" +
        "                <td class='logicBoxIndent'></td>\n" +
        "                <td class=\"checkbox\">\n" +
        "                   <input type=\"checkbox\" id=\"header_{0}_2\" name=\"headerRow\" value=\"header\">\n" +
        "                   <label class=\"check\" for=\"header_{0}_2\"></label>\n" +
        "                </td>" +
        "                <td><input class='key' type='text'></td>\n" +
        "                <td><input class='value' type='text'></td>\n" +
        "                <td><input class='desc' type='text'></td>\n" +
        "                <td class=\"delete\"></td>\n" +
        "            </tr>\n" +
        "            <tr class='request_headers'>\n" +
        "                <td class='logicBoxIndent'></td>\n" +
        "                <td class=\"checkbox\">\n" +
        "                   <input type=\"checkbox\" id=\"header_{0}_3\" name=\"headerRow\" value=\"header\">\n" +
        "                   <label class=\"check\" for=\"header_{0}_3\"></label>\n" +
        "                </td>" +
        "                <td><input class='key' type='text'></td>\n" +
        "                <td><input class='value' type='text'></td>\n" +
        "                <td><input class='desc' type='text'></td>\n" +
        "                <td class=\"delete\"></td>\n" +
        "            </tr>\n" +
        "        </tbody>" +
        "        </table>\n" +
        "        <table class='request_params_table'>\n" +
        "            <caption>request_params_table</caption>\n" +
        "            <colgroup>\n" +
        "                <col class=\"col_1\">\n" +
        "                <col class=\"col_2\">\n" +
        "                <col class=\"col_3\">\n" +
        "                <col class=\"col_4\">\n" +
        "                <col class=\"col_5\">\n" +
        "                <col class=\"col_6\">\n" +
        "            </colgroup>\n" +
        "            <thead>" +
        "            <tr class='request_params_title'>\n" +
        "                <th>" + messages.logicBox.request_params + "</th>\n" +
        "                <th></th>" +
        "                <th>" + messages.logicBox.request_key + "</th>\n" +
        "                <th>" + messages.logicBox.request_value + "</th>\n" +
        "                <th>" + messages.logicBox.request_desc + "</th>\n" +
        "                <th></th>" +
        "            </tr>\n" +
        "            </thead>" +
        "            <tbody>" +
        "            <tr class='request_params'>\n" +
        "                <td class='logicBoxIndent'><button type='button' class='btn_request_params_addRow'><em class=\"fas fa-plus-circle\"></em>" + messages.logicBox.add_row + "</button></td>\n" +
        "                <td class=\"checkbox\">\n" +
        "                   <input type=\"checkbox\" id=\"param_{0}_1\" name=\"paramRow\" value=\"paramRow\" checked>\n" +
        "                   <label class=\"check\" for=\"param_{0}_1\"></label>\n" +
        "                </td>" +
        "                <td><input class='key' type='text'></td>\n" +
        "                <td><input class='value' type='text'></td>\n" +
        "                <td><input class='desc' type='text'></td>\n" +
        "                <td class=\"delete\"></td>\n" +
        "            </tr>\n" +
        "            <tr class='request_params'>\n" +
        "                <td class='logicBoxIndent'></td>\n" +
        "                <td class=\"checkbox\">\n" +
        "                   <input type=\"checkbox\" id=\"param_{0}_2\" name=\"paramRow\" value=\"paramRow\">\n" +
        "                   <label class=\"check\" for=\"param_{0}_2\"></label>\n" +
        "                </td>" +
        "                <td><input class='key' type='text'></td>\n" +
        "                <td><input class='value' type='text'></td>\n" +
        "                <td><input class='desc' type='text'></td>\n" +
        "                <td class=\"delete\"></td>\n" +
        "            </tr>\n" +
        "            <tr class='request_params'>\n" +
        "                <td class='logicBoxIndent'></td>\n" +
        "                <td class=\"checkbox\">\n" +
        "                   <input type=\"checkbox\" id=\"param_{0}_3\" name=\"paramRow\" value=\"paramRow\">\n" +
        "                   <label class=\"check\" for=\"param_{0}_3\"></label>\n" +
        "                </td>" +
        "                <td><input class='key' type='text'></td>\n" +
        "                <td><input class='value' type='text'></td>\n" +
        "                <td><input class='desc' type='text'></td>\n" +
        "                <td class=\"delete\"></td>\n" +
        "            </tr>\n" +
        "            <tbody>" +
        "        </table>\n" +
        "        <table class='request_body_table'>\n" +
        "            <colgroup>\n" +
        "                <col style=\"width:168px;\">\n" +
        "                <col class=\"\">\n" +
        "            </colgroup>" +
        "            <tr class='request_body'>\n" +
        "                <th class='logicBoxTitleIndent'>" + messages.logicBox.request_body + "</th>\n" +
        "                <td class='contentslogicBox'><textarea class='textArea request_body_content' " +
        "                                                       rows='5'>" +
        messages.logicBox.request_placeholder + "</textarea>" +
        "                </td>\n" +
        "            </tr>\n" +
        "        </table>\n" +
        "            <p class='error'></p>\n" +
        "    </div>\n" +
        "    <div class='lyr_btm'>\n" +
        "        <div class='btnBox'>\n" +
        "            <button class='btn_lyr_close req_form_done'>" + messages.logicBox.done + "</button>\n" +
        "        </div>\n" +
        "    </div>\n" +
        "</div>\n";

    let responseHTML = // 응답 정보 팝업 설정
        "<div id='pop_response_add{0}' class='logicBox_resp_form lyrBox logic_box pop_response_add'>\n" +
        "    <div class='lyr_top'>\n" +
        "        <h3>" + messages.logicBox.response_title + "</h3>\n" +
        "        <button class='btn_lyr_close resp_form_close'>" + messages.logicBox.close + "</button>\n" +
        "    </div>\n" +
        "    <div class='lyr_mid'>\n" +
        "        <table class='response_output_table'>\n" +
        "            <caption>response_output_table</caption>\n" +
        "            <colgroup>\n" +
        "                <col class=\"col_1\">\n" +
        "                <col class=\"col_2\">\n" +
        "                <col class=\"col_3\">\n" +
        "                <col class=\"col_4\">\n" +
        "                <col class=\"col_5\">\n" +
        "            </colgroup>\n" +
        "            <thead>" +
        "            <tr class='response_outputs_title'>" +
        "                <th>" + messages.logicBox.response_output + "</th>\n" +
        "                <th></th>" +
        "                <th>" + messages.logicBox.response_path + "</th>\n" +
        "                <th>" + messages.logicBox.response_param + "</th>\n" +
        "                <th></th>" +
        "            </tr>\n" +
        "            <tr class='response_outputs'>\n" +
        "                <td class='logicBoxIndent'><button type='button' class='btn_response_output_addRow'><em class=\"fas fa-plus-circle\"></em>" + messages.logicBox.add_row + "</button></td>\n" +
        "                <td class=\"checkbox\">\n" +
        "                   <input type=\"checkbox\" id=\"response_{0}_1\" name=\"response\" value=\"response\" checked>\n" +
        "                   <label class=\"check\" for=\"response_{0}_1\"></label>\n" +
        "                </td>" +
        "                <td><input class='key' type='text'></td>\n" +
        "                <td><input class='value' type='text'></td>\n" +
        "                <td class=\"delete\"></td>\n" +
        "            </tr>\n" +
        "            <tr class='response_outputs'>\n" +
        "                <td class='logicBoxIndent'></td>\n" +
        "                <td class=\"checkbox\">\n" +
        "                   <input type=\"checkbox\" id=\"response_{0}_2\" name=\"response\" value=\"response\">\n" +
        "                   <label class=\"check\" for=\"response_{0}_2\"></label>\n" +
        "                </td>" +
        "                <td><input class='key' type='text'></td>\n" +
        "                <td><input class='value' type='text'></td>\n" +
        "                <td class=\"delete\"></td>\n" +
        "            </tr>\n" +
        "            <tr class='response_outputs'>\n" +
        "                <td class='logicBoxIndent'></td>\n" +
        "                <td class=\"checkbox\">\n" +
        "                   <input type=\"checkbox\" id=\"response_{0}_3\" name=\"response\" value=\"response\">\n" +
        "                   <label class=\"check\" for=\"response_{0}_3\"></label>\n" +
        "                </td>" +
        "                <td><input class='key' type='text'></td>\n" +
        "                <td><input class='value' type='text'></td>\n" +
        "                <td class=\"delete\"></td>\n" +
        "            </tr>\n" +
        "        </table>\n" +
        "        <p class='error'></p>\n" +
        "    </div>\n" +
        "    <div class='lyr_btm'>\n" +
        "        <div class='btnBox'>\n" +
        "            <button class='btn_lyr_close resp_form_done'>" + messages.logicBox.done + "</button>\n" +
        "        </div>\n" +
        "    </div>\n" +
        "</div>";

    // node 번호에 따라 index 숫자와 함께 HTML에 렌더링
    $('.cont_logicBox_forms')
        .append(formatString(requestHTML, paramIndex))
        .append(formatString(responseHTML, paramIndex));
}
