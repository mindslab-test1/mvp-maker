var callbackImageSample = null; // 콜백
var sampleImgBlob = null;


$(document).ready(function () {

    // 입력 완료
    $('#imgSample_input_complete').on('click', onClick_imageSample_inputComplete);

    // 팝업창 닫기
    $('#imgSample_input_box .ico_close').on('click', function () {

        sampleImgBlob = null;
        $('#imgSample_input_box').fadeOut(300);
        $('#imgSample_input_box em.close').click(); // upload된 파일 비워주기

        $('body').css({'overflow': '',});

        if (callbackImageSample != null) callbackImageSample(null, null);
    });

    //파일명 변경
    $('#imgSampleInputFile').on('change', function () {
        $('#imgSample_input_box .demolabel').text($(this).get(0).files[0].name);
        $('#imgSample_input_box #uploadFile').removeClass('upload').addClass('upload_change');
    });

    var thisURL = $(location).attr('href'),
        urlStr = thisURL.substring(thisURL.indexOf("/?"));
    var text;
    if (lang_code == 'en' || urlStr == '/?lang=en' || urlStr == '/?lang=en#none') {
        text = "Upload File"
    } else {
        text = "이미지 업로드"
    }

    //업로드 취소(x) 버튼
    $('em.close').on('click', function () {
        $(this).parent().removeClass("upload_change").addClass("upload");
        $(this).parent().children('.demolabel').text(text);
        $(this).parent().children('.demoFile').val(null);
    });
});


/* ===================================================================================================================== */
// 사용자 호출 
/* ===================================================================================================================== */

function showPopup_ImageSample(engine, callback) {

    callbackImageSample = callback;

    init_imgSample_popup_UI(engine.name);
    $('#imgSample_input_box').fadeIn(200);
}


/* ===================================================================================================================== */
// 버튼 클릭
/* ===================================================================================================================== */

function onClick_imageSample_inputComplete() {

    let $imageSampleInputFile = $('#imgSampleInputFile');

    if ($imageSampleInputFile.val() === null || $imageSampleInputFile.val() === "") {
        callbackImageSample('image', sampleImgBlob);
    } else {
        callbackImageSample('image', $imageSampleInputFile.get(0).files[0]);
    }

    sampleImgBlob = null;
    $('#imgSample_input_box').fadeOut(300);
    $('#imgSample_input_box em.close').click(); // upload된 파일 비워주기

}


/* ===================================================================================================================== */
// UI 설정
/* ===================================================================================================================== */

function init_imgSample_popup_UI(engine_name) {
    let labelClass, imgSrc = '';
    if (engine_name.indexOf(messages.engine.esr) !== -1) {
        labelClass = 'label esr_label';
        imgSrc = '/image/img_sr_sample.png';
    } else if (engine_name.indexOf(messages.engine.lic_plate_rec) !== -1) {
        labelClass = 'label avr_label';
        imgSrc = '/image/img_avr.png';
    } else if (engine_name.indexOf(messages.engine.subt_extr) !== -1) {
        labelClass = 'label subtitle_label';
        imgSrc = '/image/img_subtitle_sample.jpg';
    }

    $('#imgSample_input_box .pop_hd h2 span').text(engine_name);

    $('#imgSample_input_box .pop_bd .radio label').attr('class', labelClass);
    $('#imgSample_input_box .pop_bd .radio label img').attr('src', imgSrc);

    loadSample(imgSrc);
}


/* ===================================================================================================================== */
// 공통 function
/* ===================================================================================================================== */

function loadSample(url) {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.responseType = "blob";
    xhr.onload = function () {
        sampleImgBlob = xhr.response;
    };

    xhr.send();
}

