package ai.maum.mvp_maker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MvpMakerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MvpMakerApplication.class, args);
    }

}
