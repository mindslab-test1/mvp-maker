package ai.maum.mvp_maker.editor.mapper;

import ai.maum.mvp_maker.model.BuilderUserBackgroundVO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@Mapper
public interface User_Background_Mapper {

    /**
     * insert user background
     */
    @Insert("INSERT INTO BuilderUserBackground (url, waterMarkUrl, userNo) values (#{url}, #{waterMarkUrl}, #{userNo})")
    int insertBackground(BuilderUserBackgroundVO background);

    /**
     * get user background list
     */
    @Select("SELECT TOP 10 * FROM BuilderUserBackground WHERE userNo=#{userNo} ORDER BY id desc")
    List<BuilderUserBackgroundVO> getUserBackgroundList(int userNo);

    /**
     * delete user background
     */
    @Delete("DELETE FROM BuilderUserBackground WHERE id=#{id} AND userNo=#{userNo}")
    int deleteUserBackground(BuilderUserBackgroundVO builderUserBackgroundVO);

    @Select("SELECT watermarkUrl FROM BuilderUserBackground where url = #{url}")
    Optional<String> getUserWaterMarkUrl(String url);
}
