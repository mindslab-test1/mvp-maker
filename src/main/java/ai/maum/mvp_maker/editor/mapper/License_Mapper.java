package ai.maum.mvp_maker.editor.mapper;

import ai.maum.mvp_maker.model.ConsultUsageDTO;
import ai.maum.mvp_maker.model.ShopifyLicenseDTO;
import ai.maum.mvp_maker.model.UserLicenseDTO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Component
@Mapper
public interface License_Mapper {

    @Select("select top 1 id from SHOPIFY_LICENSE where EMAIL = #{email} and LICENSE_KEY=#{licenseKey}")
    Optional<Integer> checkUserTemplateLicenseKey(String email, String licenseKey);

    @Insert("insert into SHOPIFY_LICENSE_USAGE_LOG (EMAIL, TEMPLATE_ID, MODEL_ID, USAGE)" +
            "values (#{email}, #{templateId}, #{modelId}, #{usage})")
    void insertUsageLog(String email, int templateId, int modelId, int usage);

    @Select("select ISNULL((SUM(USAGE_LIMIT) - ISNULL((select SUM(USAGE) from SHOPIFY_LICENSE_USAGE_LOG where SHOPIFY_LICENSE_USAGE_LOG.EMAIL = #{email} AND MODEL_ID = #{modelId}), 0)), 0)" +
            "from SHOPIFY_LICENSE where EMAIL = #{email} AND LICENSE_KEY = #{licenseKey}")
    Optional<Integer> selectAvatarUsage(String email, String licenseKey, int modelId);

    @Update({
            "UPDATE SHOPIFY_LICENSE SET USAGE_LIMIT = #{usage} ",
            "WHERE ID = #{id}"
    })
    void insertUsage(int id, int usage);


//    @Select("select ID from SHOPIFY_LICENSE where REG_DATE = (select max(REG_DATE) FROM SHOPIFY_LICENSE where email = #{email} AND LICENSE_KEY = #{licenseKey} AND USAGE_LIMIT = 0 AND CONSULT_USAGE_LIMIT = 0 AND CONSULT_PERIOD = 0)")
    @Select({
            "SELECT TOP 1 ID FROM SHOPIFY_LICENSE ",
            "WHERE EMAIL = #{email}  AND LICENSE_KEY = #{licenseKey} AND USAGE_LIMIT = 0 AND CONSULT_USAGE_LIMIT = 0 AND CONSULT_PERIOD = 0 ",
            "ORDER BY ID DESC"
    })
    Optional<Integer> selectRecentInsertLicenseUserId(String email, String licenseKey);

    @Select({
            "SELECT * ",
            "FROM SHOPIFY_LICENSE ",
            "WHERE EMAIL = #{email} ",
            "AND LICENSE_KEY=#{licenseKey} ",
            "ORDER BY REG_DATE DESC"
    })
    List<ShopifyLicenseDTO> getLicenseUsageList(String email, String licenseKey);

    /**
     * Consult
     */
    @Select({
            "SELECT * ",
            "FROM SHOPIFY_LICENSE ",
            "WHERE EMAIL = #{email} ",
            "AND LICENSE_KEY = #{licenseKey} ",
            "AND (GETDATE() BETWEEN CONSULT_START_DATE - 1 AND DATEADD(month, CONSULT_PERIOD, CONSULT_START_DATE)) ",
            "ORDER BY CONSULT_START_DATE"
    })
    List<ShopifyLicenseDTO> getActiveConsultList(String email, String licenseKey);

    @Update({
            "<script> ",
            "UPDATE SHOPIFY_LICENSE ",
            "<set> ",
            "<if test = 'startDate == null'>CONSULT_START_DATE = DATEADD(day, DATEDIFF(day, 0, GETDATE()), 0),</if> ",
            "<if test = 'startDate != null'>CONSULT_START_DATE = DATEADD(day, DATEDIFF(day, 0, #{startDate}), 0),</if>",
            "CONSULT_USAGE_LIMIT = #{usage}, CONSULT_PERIOD = #{period} ",
            "</set> ",
            "WHERE ID = #{id} ",
            "</script>"
    })
    void insertConsultUsage(Integer id, int usage, int period, LocalDateTime startDate);

    @Insert({
            "INSERT INTO SHOPIFY_CONSULT_USAGE_LOG(EMAIL, AVATAR_ID)",
            "VALUES (#{email}, #{avatarId})"
    })
    void insertConsultUsageLog(String email, int avatarId);

    @Select({
            "SELECT TOP 1 U.UserNo userNo, U.Email email, BA.id avatarId, BA.licenseKey licenseKey ",
            "FROM  User_T U, Flow F, FlowNode FN, FlowNodeParam FNP, BuilderAvatar BA ",
            "WHERE F.flowId = #{flowId} ",
            "AND F.userNo = U.UserNo ",
            "AND F.flowId = FN.flowId ",
            "AND FN.engineId = 1076 ",
            "AND FN.flowNodeId = FNP.flowNodeId ",
            "AND FNP.name = 'model' ",
            "AND FNP.value = BA.id"
    })
    UserLicenseDTO getUserByFlowId(int flowId);

    @Select({
            "SELECT COUNT(*) usage",
            "FROM SHOPIFY_CONSULT_USAGE_LOG ",
            "WHERE EMAIL = #{email} ",
            "AND AVATAR_ID = #{avatarId} ",
            "AND USAGE_DATE BETWEEN #{date} AND DATEADD(mm, 1, #{date})"
    })
    ConsultUsageDTO getConsultUsage(String email, int avatarId, LocalDateTime date);

}