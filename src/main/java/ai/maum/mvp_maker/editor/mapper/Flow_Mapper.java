package ai.maum.mvp_maker.editor.mapper;

import ai.maum.mvp_maker.model.*;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

@Component
@Mapper
public interface Flow_Mapper {

    /* ================================================================================================================= */
    // 조회
    /* ================================================================================================================= */

    /*
     ** 사용자의 플로우 목록 조회
     */
    @Select("select * from Flow where userNo=#{user_no}")
    List<FlowVO> getFlowList(int user_no);

    @Select("SELECT * FROM Flow FL INNER JOIN BuilderTemplate BT ON FL.TEMPLATEID = BT.ID WHERE USERNO = #{user_no}")
    List<FlowVO> getFlowListForBuilder(int user_no);
    /*
     ** 플로우 조회
     */
    @Select("select * from Flow where flowId=#{flow_id}")
    FlowVO getFlow(int flow_id);

    @Select("SELECT flowId, userNo, name_en AS [name], name FROM Flow where flowId=#{flow_id}")
    FlowVO getFlow_en(int flow_id);

    /*
     ** 플로우 노드 목록 조회
     */
//    @Select( "select * from FlowNode where flowId=#{flow_id}" )
    @Select("select FlowNode.*, Engine.name as engineName from FlowNode " +
            "inner join Engine on Engine.engineId=FlowNode.engineId  " +
            "where flowId=#{flow_id}")
    List<FlowNodeVO> getFlowNodeList(int flow_id);

    @Select("select FlowNode.*, Engine.name_en as engineName from FlowNode " +
            "inner join Engine on Engine.engineId=FlowNode.engineId  " +
            "where flowId=#{flow_id}")
    List<FlowNodeVO> getFlowNodeList_en(int flow_id);

    /*
     ** 플로우 노드 파라메타 목록 조회
     */
    @Select("select * from FlowNodeParam where flowNodeId=#{flow_node_id}")
    List<FlowNodeParamVO> getFlowNodeParamList(int flow_node_id);

    /*
     ** 정해져 있는 application flow 호출
     */
    @Select("select flowId from Flow where userNo=0 and name=#{appName}")
    int getAppFlowId(String appName);

    /* ================================================================================================================= */
    // 추가
    /* ================================================================================================================= */

    @Insert("insert into Flow (userNo, name, description, createDate, createUser, updateDate, updateUser, delYn, displayOrder, templateId) "
            + "values (#{userNo}, #{name}, #{description}, #{createDate}, #{createUser}, #{updateDate}, #{updateUser}, #{delYn}, #{displayOrder}, #{templateId}) "
            + "SELECT SCOPE_IDENTITY()")
    int insertFlow(FlowVO flow);

    @Select("SELECT flowId FROM Flow WHERE flowId = @@Identity")
    int getLastId_Flow();

    @Insert("insert into FlowNode (flowId, engineId, inputMethod) values (#{flowId}, #{engineId}, #{inputMethod})")
    int insertFlowNode(FlowNodeVO flow);

    @Select("SELECT flowNodeId FROM FlowNode WHERE flowNodeId = @@Identity")
    int getLastId_FlowNode();

    @Insert("insert into FlowNodeParam (flowNodeId, name, value) values (#{flowNodeId}, #{name}, #{value})")
    int insertFlowNodeParam(FlowNodeParamVO flow);

    @Select("SELECT id FROM FlowNodeParam WHERE id = @@Identity")
    int getLastId_FlowNodeParam();

    /* ================================================================================================================= */
    // 수정
    /* ================================================================================================================= */

    /*
     ** Flow의 이름 변경
     */
    @Update("update Flow set name=#{name} where flowId=#{flowId}")
    int updateFlow(String name, int flowId);

    @Update("update Flow set displayOrder=#{displayOrder}, updateDate=#{currentDate}, updateUser=#{user_no}  where flowId=#{flow_id}")
    int updateDisplayOrder(String flow_id, int displayOrder, Date currentDate, int user_no);

    @Update("update Flow set name=#{name}, updateDate=#{updateDate}, updateUser=#{userNo}  where flowId=#{flowId}")
    void updateName(FlowVO flow);


    /* ================================================================================================================= */
    // 삭제
    /* ================================================================================================================= */

    /*
     ** 지정 Flow 삭제
     */
    @Delete("delete from Flow where flowId=#{flow_id}")
    int deleteFlow(int flow_id);

    /*
     ** BuilderScript 삭제
     */
    @Delete("delete from BuilderScript where flowId=#{flow_id}")
    int deleteBuilderScript(int flow_id);
    /*
     ** Flow의 모든 Node 삭제
     */
    @Delete("delete from FlowNode where flowId=#{flow_id}")
    int deleteFlowNode(int flow_id);

    /*
     ** FlowNode의 모든 파라메타 삭제
     */
    @Delete("delete from FlowNodeParam where flowNodeId=#{flow_node_id}")
    int deleteFlowNodeParam(int flow_node_id);

    /*
     ** Builder Flow 추가
     */
    @Insert("insert into Flow(userNo, name, createDate, createUser, updateDate, delYn, templateId, updateUser)"
            + "values(#{userNo}, #{projectName}, #{createDate}, #{userNo}, #{updateDate}, 'N', #{templateId}, #{userNo})")
    @Options(useGeneratedKeys = true, keyProperty = "flowId")
    int insertBuilderFlow(BuilderFlowVO builderFlowVO);

    @Update("update Flow set updateDate = #{currentDate} where flowId = #{flowId}")
    int updateBuilderFlow(LocalDateTime currentDate, int flowId);

    /*
     ** Builder Flow Node Parameter 조
     */
//    @Select("SELECT Engine.engineId as engineId, Engine.name_en as engineName, FlowNodeParam.name as paramName, FlowNodeParam.value as paramValue "
//            + "FROM FlowNode "
//            + "LEFT JOIN FlowNodeParam ON FlowNodeParam.flowNodeId = FlowNode.flowNodeId "
//            + "LEFT JOIN Engine ON Engine.engineId = FlowNode.engineId "
//            + "WHERE FlowNode.flowId = #{flowId}"
//    )
    @Select("SELECT FN.engineId engineId, FNP.name name, FNP.value value " +
            "FROM FlowNode FN, FlowNodeParam FNP " +
            "WHERE FN.flowId = #{flowId} " +
            "AND FN.flowNodeId = FNP.flowNodeId"
    )
    List<FlowParamVO> getFlowParam(int flowId);

    @Select( "select u.Email from Flow f inner join User_T u on f.userNo = u.UserNo where flowId = #{flowId}" )
    String getEmailByFlowId(int flowId);
}
