package ai.maum.mvp_maker.editor.mapper;

import ai.maum.mvp_maker.model.EngineModelRequestVO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface EngineModelRequest_Mapper {

    @Insert("insert into EngineModelRequest " +
            "(userNo, name, phone, email, engine, flowId, title, content, state) " +
            "values " +
            "(#{userNo}, #{name}, #{phone}, #{email}, #{engine}, #{flowId}, #{title}, #{content}, #{state})")
    int insert(EngineModelRequestVO vo);

}
