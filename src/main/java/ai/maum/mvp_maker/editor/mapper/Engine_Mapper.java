package ai.maum.mvp_maker.editor.mapper;

import ai.maum.mvp_maker.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface Engine_Mapper {

    /*
    ** 그룹에 소속된 엔진 목록 조회
    */
    @Select( "select * from Engine where groupId=#{group_id}" )
    List<EngineVO> getList(int group_id);

    @Select( "select engineId" +
            "      , groupId" +
            "      , name_en as [name]" +
            "      , desc_en as [desc]" +
            "      , apiUrl" +
            "      , apiMethod" +
            "      , requestType" +
            "      , responseType" +
            "      , iconPath" +
            "      , inputType" +
            "      , outputType" +
            "      , active"+
            "      , docUrl_en as docUrl " +
            "      , groupId2" +
            "      , depth " +
            "from Engine where groupId=#{group_id}" )
    List<EngineVO> getList_en(int group_id);

    /*
    ** 엔진 INPUT 목록 조회
    */
    @Select( "select * from EngineInput where engineId=#{engine_id}" )
    List<EngineInputVO> getInputList(int engine_id);

    /*
    ** 엔진 파라메타 목록 조회
    */
    @Select( "select * from EngineParam where engineId=#{engine_id}" )
    List<EngineParamVO> getParamList(int engine_id);

    @Select( "select  id" +
            "      ,engineId" +
            "      ,name" +
            "      ,displayName_en as displayName" +
            "      ,type" +
            "      ,displayEnumValue_en as displayEnumValue" +
            "      ,enumValue" +
            "      ,defaultValue_en as defaultValue" +
            "      ,display" +
            "      from EngineParam where engineId=#{engine_id}" )
    List<EngineParamVO> getParamList_en(int engine_id);

    /*
    ** 엔진 그룹 파라메타 목록 조회
    */
    @Select( "select * from EngineGroupParam where engineId=#{engine_id}" )
    List<EngineGroupParamVO> getGroupParamList(int engine_id);

    /*
    ** 엔진 그룹 파라메타 값 목록 조회
    */
    @Select( "select * from EngineGroupParamValue where groupParamId=#{group_param_id}" )
    List<EngineGroupParamValueVO> getGroupParamValueList(int group_param_id);

    @Select( "select id" +
            "      ,groupParamId" +
            "      ,name_en as [name]" +
            "      ,name as [name_en]" +
            "      ,data" +
            "     from EngineGroupParamValue where groupParamId=#{group_param_id}" )
    List<EngineGroupParamValueVO> getGroupParamValueList_en(int group_param_id);
}
