package ai.maum.mvp_maker.editor.mapper;

import ai.maum.mvp_maker.model.BuilderScriptVO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@Mapper
public interface Script_Mapper {

    /*
     ** flowId를 기준으로 존재하는 script counting
     */
    @Select("select id, flowId, avatarId, backgroundUrl, text from BuilderScript where flowId = #{flowId}")
    List<BuilderScriptVO> getScriptAsFlowId(int flowId);

    /*
     ** script Delete
     */
    @Delete("delete from BuilderScript where flowId = #{flowId}")
    int deleteScriptAsFlowId(int flowId);

    /*
     ** script Insert
     */
    @Insert("insert into BuilderScript(flowId, avatarId, backgroundUrl, text, createDate, updateDate)"
            + "values(#{flowId}, #{avatarId}, #{backgroundUrl}, #{text}, #{currentDate}, #{currentDate})")
    int insertScript(int flowId, int avatarId, String backgroundUrl, LocalDateTime currentDate, String text);
}
