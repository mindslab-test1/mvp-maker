package ai.maum.mvp_maker.editor.mapper;

import ai.maum.mvp_maker.model.EngineGroupVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface EngineGroup_Mapper {

    /*
    ** 엔진 그룹 목록 조회
    */
    @Select( "select * from EngineGroup" )
    List<EngineGroupVO> getList();

    @Select( "select groupId ,name_en as name, color, maxCols from EngineGroup" )
    List<EngineGroupVO> getList_en();

}
