package ai.maum.mvp_maker.editor.mapper;

import ai.maum.mvp_maker.model.BuilderAvatarLicenseVO;
import ai.maum.mvp_maker.model.BuilderAvatarVO;
import ai.maum.mvp_maker.model.PurchasedVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface Avatar_Mapper {

    /**
     *  get avatar list with licensed info
     */
    @Select("select *, " +
            "case when email is null then 'N' else 'Y' end purchased " +
            "from BuilderAvatar ba " +
            "left outer join ( select distinct LICENSE_KEY, EMAIL from SHOPIFY_LICENSE where email = #{email}) sl on ba.licenseKey = sl.LICENSE_KEY")
    List<BuilderAvatarLicenseVO> getAvatarInfoList(String email);

    /**
     *  get avatar list
     */
    @Select("select * from BuilderAvatar where id = #{id}")
    BuilderAvatarVO getAvatar(int id);

    @Select("select ba.model, ba.licenseKey,\n" +
            "    case when email is null then 'N' else 'Y' end purchased\n" +
            "    from BuilderAvatar ba\n" +
            "    left outer join ( select LICENSE_KEY, EMAIL from SHOPIFY_LICENSE where email = #{email}) sl on ba.licenseKey = sl.LICENSE_KEY")
    List<PurchasedVO> getPurchasedList(String email);
}
