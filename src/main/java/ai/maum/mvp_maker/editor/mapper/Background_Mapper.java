package ai.maum.mvp_maker.editor.mapper;

import ai.maum.mvp_maker.model.BuilderBackgroundVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@Mapper
public interface Background_Mapper {

    /**
     * get default background list
     */
    @Select("SELECT * FROM BuilderBackground ORDER BY id ASC")
    List<BuilderBackgroundVO> getBackgroundList();

    @Select("SELECT watermarkUrl FROM BuilderBackground where url = #{url}")
    Optional<String> getWaterMarkUrl(String url);
}
