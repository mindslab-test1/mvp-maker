package ai.maum.mvp_maker.editor.mapper;

import ai.maum.mvp_maker.model.FlowNodeParamVO;
import ai.maum.mvp_maker.model.FlowNodeVO;
import ai.maum.mvp_maker.model.FlowVO;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface Common_Mapper {

    /*
    ** 사용자의 플로우 목록 조회
    */
    @Select( "SELECT SCOPE_IDENTITY()" )
//    @Select( "SELECT flowId AS LastID FROM Flow WHERE flowId = @@Identity" )
    int getLastId(String table_name);

}
