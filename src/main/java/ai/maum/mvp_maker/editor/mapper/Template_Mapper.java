package ai.maum.mvp_maker.editor.mapper;

import ai.maum.mvp_maker.model.BuilderAvatarVO;
import ai.maum.mvp_maker.model.BuilderTemplateVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface Template_Mapper {

    /**
     *  get avatar list
     */
    @Select("select *" +
            "from dbo.BuilderTemplate")
    List<BuilderTemplateVO> getTemplateList();
}
