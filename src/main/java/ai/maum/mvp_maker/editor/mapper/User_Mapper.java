package ai.maum.mvp_maker.editor.mapper;

import ai.maum.mvp_maker.model.EngineVO;
import ai.maum.mvp_maker.model.UserVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface User_Mapper {

    /*
    ** 사용자 정보 조회
    */
    @Select( "select * from User_T where email=#{email}" )
    UserVO getUser(String email);

}
