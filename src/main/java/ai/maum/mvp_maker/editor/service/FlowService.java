package ai.maum.mvp_maker.editor.service;

import ai.maum.mvp_maker.editor.mapper.Flow_Mapper;
import ai.maum.mvp_maker.model.*;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class FlowService {

    @Autowired
    Flow_Mapper flowMapper;

    /*
     ** Flow 신규 등록
     */
    @Transactional
    public FlowVO addFlow(int user_no, FlowVO flow) {
        Date currentDate = new Date();

        flow.setUserNo(user_no);
        flow.setCreateDate(currentDate);
        flow.setUpdateDate(currentDate);
        flow.setCreateUser(user_no);
        flow.setUpdateUser(user_no);
        flow.setDelYn('N');
        flowMapper.insertFlow(flow);
        flow.setFlowId(flowMapper.getLastId_Flow());
        for (FlowNodeVO node : flow.getNodeList()) {
            node.setFlowId(flow.getFlowId());
            flowMapper.insertFlowNode(node);
            node.setFlowNodeId(flowMapper.getLastId_FlowNode());

            for (FlowNodeParamVO param : node.getParamList()) {
                param.setFlowNodeId(node.getFlowNodeId());
                flowMapper.insertFlowNodeParam(param);
                param.setId(flowMapper.getLastId_FlowNodeParam());
            }
        }
        return flow;
    }

    /*
     ** Flow 변경
     */
    @Transactional
    public FlowVO updateFlow(FlowVO flow) {

        // Flow에 등록된 모든 노드 삭제
        deleteFlowNodes(flow.getFlowId());

        for (FlowNodeVO node : flow.getNodeList()) {
            node.setFlowId(flow.getFlowId());
            flowMapper.insertFlowNode(node);
            node.setFlowNodeId(flowMapper.getLastId_FlowNode());

            for (FlowNodeParamVO param : node.getParamList()) {
                param.setFlowNodeId(node.getFlowNodeId());
                flowMapper.insertFlowNodeParam(param);
                param.setId(flowMapper.getLastId_FlowNodeParam());
            }
        }
        return flow;
    }

    @Transactional
    public void updateFlowDisplayOrder(JsonNode[] jsonDataArr, int user_no) {
        java.sql.Date currentDate = new java.sql.Date(new Date().getTime());

        for (JsonNode jsonData : jsonDataArr) {
            flowMapper.updateDisplayOrder(jsonData.get("flow_id").asText(), jsonData.get("displayOrder").asInt(), currentDate, user_no);
        }

    }

    @Transactional
    public void updateFlowName(List<FlowVO> flows, int userNo) {
        java.sql.Date currentDate = new java.sql.Date(new Date().getTime());

        for (FlowVO flow : flows) {
            flow.setUpdateDate(currentDate);
            flow.setUpdateUser(userNo);
            flowMapper.updateName(flow);
        }
    }

    /*
     ** Flow 삭제 - Flow와 관련된 정보도 함께 삭제
     * */
    public void deleteFlow(String flow_id) {
        //System.out.println(flow_id);

        // String으로 받은 정보 parsing
        String parseTxt = flow_id.substring(flow_id.indexOf('[') + 1, flow_id.indexOf(']'));
        int cnt = 0;
        for (int i = 0; i < parseTxt.length(); i++) {
            if (parseTxt.charAt(i) == ',')
                cnt++;
        }

        for (int i = 0; i <= cnt; i++) {

            int id = Integer.parseInt(parseTxt.split(",")[i]);

            deleteFlowNodes(id);   //관련 정보 함께 삭제
            flowMapper.deleteFlow(id);
        }
    }

    /*
     ** Flow에 등록된 모든 노드와 파라메타를 삭제한다.
     */
    void deleteFlowNodes(int flow_id) {
        List<FlowNodeVO> node_list = flowMapper.getFlowNodeList(flow_id);
        for (FlowNodeVO node : node_list) {
            flowMapper.deleteFlowNodeParam(node.getFlowNodeId());
        }
        flowMapper.deleteFlowNode(flow_id);
    }


    /*
     ** 지정 사용자의 Flow 목록 조회
     */
    public List<FlowVO> getFlowList(int user_id, String lang) {
        List<FlowVO> flow_list = flowMapper.getFlowList(user_id);
        for (FlowVO flow : flow_list) {
            if (lang.equals("en")) {
                flow.setNodeList(flowMapper.getFlowNodeList_en(flow.getFlowId()));
            } else {
                flow.setNodeList(flowMapper.getFlowNodeList(flow.getFlowId()));
            }
        }
        return flow_list;
    }

    /*
     **
     */
    public FlowVO getFlow(int flow_id, String lang) {

        FlowVO flow;
        if (lang.equals("en")) {
            flow = flowMapper.getFlow(flow_id);
            if (flow.getUserNo() == 0) {
                if ("maum 회의록".equalsIgnoreCase(flow.getName())) {
                    flow.setName("maum Minutes");
                } else if ("FAST 대화형 AI".equalsIgnoreCase(flow.getName())) {
                    flow.setName("FAST AI");
                } else if ("키오스크".equalsIgnoreCase(flow.getName())) {
                    flow.setName("Kiosk");
                } else if ("AI Human(음성대화)".equalsIgnoreCase(flow.getName())) {
                    flow.setName("AI Human(Speech)");
                } else if ("AI Human(텍스트대화)".equalsIgnoreCase(flow.getName())) {
                    flow.setName("AI Human(Text)");
                }
            }
        } else {
            flow = flowMapper.getFlow(flow_id);
        }

        // predefined flow

        flow.setNodeList(flowMapper.getFlowNodeList(flow_id));
        for (FlowNodeVO node : flow.getNodeList()) {
            node.setParamList(flowMapper.getFlowNodeParamList(node.getFlowNodeId()));
        }

        return flow;
    }

    /*
     ** 정해져 있는 application flow 호출
     */
    public int getAppFlowId(String appName) {
        String flowName;

        if ("maum_minutes".equalsIgnoreCase(appName)) {
            flowName = "maum 회의록";
        } else if ("fast_ai".equalsIgnoreCase(appName)) {
            flowName = "FAST 대화형 AI";
        } else if ("kiosk".equalsIgnoreCase(appName)) {
            flowName = "키오스크";
        } else if ("ai_humanSpeech".equalsIgnoreCase(appName)) {
            flowName = "AI Human(음성대화)";
        } else if ("ai_humanText".equalsIgnoreCase(appName)) {
            flowName = "AI Human(텍스트대화)";
        } else {
            flowName = "";
        }

        int appFlowId = flowMapper.getAppFlowId(flowName);

        return appFlowId;
    }
    public void deleteFlow(int flowId) {
        flowMapper.deleteFlow(flowId);
    }
    public void deleteBuilderScript(int flowId) {
        flowMapper.deleteBuilderScript(flowId);
    }

    public void updateFlow(String name, int flowId) {
        flowMapper.updateFlow(name, flowId);
    }
}
