package ai.maum.mvp_maker.editor.service;

import ai.maum.mvp_maker.editor.mapper.EngineModelRequest_Mapper;
import ai.maum.mvp_maker.model.EngineModelRequestVO;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Created by kirk@mindslab.ai on 2020-07-23
 */
@Slf4j
@AllArgsConstructor
@Service
public class EngineModelRequestService {
    EngineModelRequest_Mapper engineModelRequestMapper;

    /**
     * 모델 학습 요청 INSERT
     * @param vo
     * @return
     */
    public int insert(EngineModelRequestVO vo) {
        return engineModelRequestMapper.insert(vo);
    }
}
