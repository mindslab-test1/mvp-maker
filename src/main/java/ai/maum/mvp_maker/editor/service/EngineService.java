package ai.maum.mvp_maker.editor.service;

import ai.maum.mvp_maker.editor.mapper.EngineGroup_Mapper;
import ai.maum.mvp_maker.editor.mapper.Engine_Mapper;
import ai.maum.mvp_maker.model.*;
import ai.maum.mvp_maker.runner.service.BuilderService;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EngineService {
    private final Logger logger = LoggerFactory.getLogger(EngineService.class);

    @Autowired
    BuilderService builderService;

    @Autowired
    EngineGroup_Mapper groupMapper;

    @Autowired
    Engine_Mapper engineMapper;

    @Value("${human.chatbot.location}")
    String chatbotServerLocation;

    @Autowired
    RestTemplate restTemplate;

    /*
     ** 엔진 그룹 목록 조회
     */
    public List<EngineGroupVO> getEngineGroupList(String lang) {
        List<EngineGroupVO> listGroup;
        if (lang.equals("en")) {
            listGroup = groupMapper.getList_en();
        } else {

            listGroup = groupMapper.getList();
        }

        return listGroup;
    }

    /*
     ** 지정 엔진 그룹의 엔진 목록 조회
     */
    public List<EngineVO> getEngineList(int group_id, String lang, UserVO userVO) {
        List<EngineVO> listEngine;

        if (lang.equals("en")) {
            listEngine = engineMapper.getList_en(group_id);
        } else {
            listEngine = engineMapper.getList(group_id);
        }

        for (EngineVO engine : listEngine) {
            getEngine_InputList(engine);
            getEngine_ParamList(engine, lang, userVO);
            getEngine_GroupParamList(engine, lang);
        }

        return listEngine;
    }

    private void getEngine_InputList(EngineVO engine) {
        engine.setInputList(engineMapper.getInputList(engine.getEngineId()));
    }

    private void getEngine_ParamList(EngineVO engine, String lang, UserVO userVO) {
        if (lang.equals("en")) {
            engine.setParamList(engineMapper.getParamList_en(engine.getEngineId()));
        } else {
            engine.setParamList(engineMapper.getParamList(engine.getEngineId()));
        }
        if(engine.getEngineId() == 1075 && // 시나리오 엔진인 경우
                (userVO != null && userVO.getEmail() != null) // 로그인 유저 정보가 있는 경우
        ) {
            // 시나리오 엔진(id:1075)인 경우 fast팀 사용자 챗봇 조회 API 조회하여 추가
            JsonArray list = getUserChatbotList(userVO.getEmail());
            if(list != null && list.size() > 0) {
                String enumValue = engine.getParamList().get(0).getEnumValue();
                String displayEnumValue = engine.getParamList().get(0).getDisplayEnumValue();
                String displayEnumValueEn = engine.getParamList().get(0).getDisplayEnumValueEn();

                for(int i=0 ; i < list.size() ; i++) {
                    JsonObject obj = (JsonObject) list.get(i);
                    EngineParamVO vo = new EngineParamVO();
                    enumValue += "|" + obj.get("No").getAsString();
                    displayEnumValue += "|" + obj.get("Name").getAsString();
                    displayEnumValueEn += "|" + obj.get("Name").getAsString();
                }
                engine.getParamList().get(0).setEnumValue(enumValue);
                engine.getParamList().get(0).setDisplayEnumValue(displayEnumValue);
                engine.getParamList().get(0).setDisplayEnumValueEn(displayEnumValueEn);
            }
        }
        if(engine.getEngineId() == 1076 && (userVO != null && userVO.getEmail() != null)){
            // Avatar
            List<BuilderAvatarLicenseVO> avatarList = builderService.getAvatarInfoList(userVO);
//            List<BuilderAvatarLicenseVO> filterList = avatarList.stream().filter(f -> f.getPurchased().equalsIgnoreCase("Y")).collect(Collectors.toList());
//            String avatarEnumValue = filterList.stream().map(BuilderAvatarLicenseVO::getId).map(Object::toString).collect(Collectors.joining("|"));
            String avatarEnumValue = avatarList.stream().map(BuilderAvatarLicenseVO::getId).map(Object::toString).collect(Collectors.joining("|"));
            engine.getParamList().get(0).setEnumValue(avatarEnumValue);
            engine.getParamList().get(0).setData(avatarList);

            // Background
            List<BuilderBackgroundVO> bgList = builderService.getBackgroundList();
            String bgEnumValue = bgList.stream().map(BuilderBackgroundVO::getId).map(Object::toString).collect(Collectors.joining("|"));
            engine.getParamList().get(1).setEnumValue(bgEnumValue);
            engine.getParamList().get(1).setData(Collections.singletonList(bgList));
        }
    }

    private JsonArray getUserChatbotList(String email) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HashMap<String, Object> body = new HashMap<>();
            body.put("userId", email);
            body.put("companyId", "000");
            body.put("isCompany", "N");
            HttpEntity<HashMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
            ResponseEntity<String> response = restTemplate.exchange(chatbotServerLocation, HttpMethod.POST,
                    requestEntity, String.class);
            JsonParser jsonParser = new JsonParser();
            JsonArray jsonArray = (JsonArray) jsonParser.parse(response.getBody().toString());
            return jsonArray;
        }
        catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    private void getEngine_GroupParamList(EngineVO engine, String lang) {
        engine.setGroupParamList(engineMapper.getGroupParamList(engine.getEngineId()));
        if (lang.equals("en")) {

            for (EngineGroupParamVO gparam : engine.getGroupParamList()) {
                gparam.setValueList(engineMapper.getGroupParamValueList_en(gparam.getGroupParamId()));
            }
        } else {

            for (EngineGroupParamVO gparam : engine.getGroupParamList()) {
                gparam.setValueList(engineMapper.getGroupParamValueList(gparam.getGroupParamId()));
            }
        }
    }
}
