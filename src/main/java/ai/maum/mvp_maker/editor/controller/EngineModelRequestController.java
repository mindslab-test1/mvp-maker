package ai.maum.mvp_maker.editor.controller;

import ai.maum.mvp_maker.common.utils.SendMail;
import ai.maum.mvp_maker.editor.service.EngineModelRequestService;
import ai.maum.mvp_maker.model.EngineModelRequestVO;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kirk@mindslab.ai on 2020-07-23
 */
@Slf4j
@AllArgsConstructor
@RequestMapping(value="/mvp/api")
@CrossOrigin(origins = "*")
@Controller
public class EngineModelRequestController {
    private EngineModelRequestService engineModelRequestService;
    private SendMail sendMail;

    /**
     * 모델 학습 요청
     * @param name
     * @param engine
     * @param title
     * @param content
     * @param phone
     * @param email
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/model_request/add")
    @ResponseBody
    @CrossOrigin
    public int modelRequestAdd(@RequestParam (value = "name") String name,
                      @RequestParam (value = "engine") String engine,
                      @RequestParam (value = "title") String title,
                      @RequestParam (value = "content") String content,
                      @RequestParam (value = "phone") String phone,
                      @RequestParam (value = "userNo") String userNo,
                      @RequestParam (value = "email") String email) throws Exception {
        log.info("modelRequestAdd >> name: {}, engine: {}, title: {}, content: {}, phone: {}, userNo: {}, email: {}", name, engine, title, content, phone, userNo, email);

        EngineModelRequestVO vo = new EngineModelRequestVO();
        vo.setName(name);
        vo.setEngine(engine);
        vo.setTitle(title);
        vo.setContent(content);
        vo.setPhone(phone);
        vo.setEmail(email);
        vo.setFlowId(0);
        vo.setUserNo(Integer.parseInt(userNo));
        vo.setState("SUBMIT");

        String fromAddr = email;
        String toAddr = "hello@mindslab.ai";
        String subject = "AI Human Builder: 모델 학습 요청";
        String msg = "name: " + name + "<br>"
                   + "userNo: " + userNo + "<br>"
                   + "phone: " + phone + "<br>"
                   + "email: " + email + "<br>"
                   + "engine: " + engine + "<br>"
                   + "model name: " + title + "<br>"
                   + "comment: " + content + "<br>";

        String mailResult = sendMail.sendPostWithForm(fromAddr, toAddr, subject, msg);
        log.info("modelRequestAdd sendMail: {}", mailResult);

        return engineModelRequestService.insert(vo);
    }
}
