package ai.maum.mvp_maker.editor.controller;

import ai.maum.mvp_maker.editor.service.FlowService;
import ai.maum.mvp_maker.model.FlowUpdateVO;
import ai.maum.mvp_maker.model.FlowVO;
import ai.maum.mvp_maker.model.UserVO;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Slf4j
@Controller
@RequestMapping(value = "/mvp/api")
@CrossOrigin(origins = "*")
public class FlowController {

    @Autowired
    private FlowService service;

    /*
     ** 지정 사용자의 Flow 목록 조회
     */
    @RequestMapping(value = "/Flows/User/{user_id}", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public List<FlowVO> getFlowList(HttpServletRequest request,
                                    @PathVariable final int user_id, @RequestParam(name = "lang", required = false, defaultValue = "en") String lang) {
        log.info("# FlowController.getFlowList >> user_id = {}", user_id);

        List<FlowVO> flow_list = service.getFlowList(user_id, lang);

        log.info("# FlowController.getFlowList >> result = {}", flow_list.toString());
        return flow_list;
    }

    /*
     ** Flow 조회
     */
    @RequestMapping(value = "/Flows/{flow_id}", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public FlowVO getFlow(HttpServletRequest request,
                          @PathVariable final int flow_id, @RequestParam(name = "lang", required = false, defaultValue = "en") String lang) {
        log.info("# FlowController.getFlow >> flow_id = {}", flow_id);

        FlowVO flow = service.getFlow(flow_id, lang);

        log.info("# FlowController.getFlow >> result = {}", flow.toString());
        return flow;
    }

    /*
     ** Flow 등록
     */
    @RequestMapping(value = "/Flows", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    @CrossOrigin
    public ResponseEntity addFlow(HttpServletRequest request,
                          @Valid @RequestBody FlowVO flow, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(bindingResult.getAllErrors());
        }

        UserVO user = (UserVO) request.getSession().getAttribute("user");

        log.info("# FlowController.addFlow >> user={} >> {}", user.toString(), flow.toString());

        flow = service.addFlow(user.getUserNo(), flow);

        log.info("# FlowController.addFlow >> result = {}", flow.toString());
        return ResponseEntity.ok().body(flow);
    }

    /*
     ** Flow 변경
     */
    @RequestMapping(value = "/Flows/{flow_id}", method = RequestMethod.PUT, consumes = "application/json")
    @ResponseBody
    @CrossOrigin
    public FlowVO updateFlow(HttpServletRequest request,
                             @RequestBody FlowVO flow,
                             @PathVariable final int flow_id) {
        log.info("# FlowController.updateFlow >> {}", flow.toString());

        flow = service.updateFlow(flow);

        log.info("# FlowController.updateFlow >> result = {}", flow.toString());
        return flow;
    }

    /*
     ** Flows display ordering 변경
     */
    @RequestMapping(value = "/Flows/displayOrder", method = RequestMethod.PUT, consumes = "application/json")
    @ResponseBody
    @CrossOrigin
    public void updateFlowDisplayOrder(HttpServletRequest request, @RequestBody String inputData) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode[] jsonDataArr = mapper.readValue(inputData, JsonNode[].class);
        log.info("# FlowController.updateFlowDisplayOrder >> {}", inputData);
        log.info("# FlowController.updateFlowDisplayOrder >> {}", inputData);
        UserVO user = (UserVO) request.getSession().getAttribute("user");
        service.updateFlowDisplayOrder(jsonDataArr, user.getUserNo());

        log.info("# FlowController.updateFlowDisplayOrder >> result = {}", inputData);
    }

    @RequestMapping(value = "/Flows/name", method = RequestMethod.PUT, consumes = "application/json")
    @ResponseBody
    @CrossOrigin
    public void updateFlowName(HttpServletRequest request, @RequestBody List<FlowVO> flows) throws JsonProcessingException {

        log.info("# FlowController.updateFlowName >> {}", flows);
        UserVO user = (UserVO) request.getSession().getAttribute("user");
        service.updateFlowName(flows, user.getUserNo());

        log.info("# FlowController.updateFlowName >> result = {}", flows);
    }

    /*
     ** Flow 삭제
     */
    @RequestMapping(value = "/Flows/delete", method = RequestMethod.DELETE, consumes = "application/json")
    @ResponseBody
    @CrossOrigin
    public void deleteFlow(HttpServletRequest request, @RequestBody String flow_id) {
        log.info("# FlowController.deleteFlow request delete flow_id >> {}", flow_id);

        service.deleteFlow(flow_id);

        log.info("# FlowController.deleteFlow >> Success = {}", flow_id);
    }

    /*
     ** 정해져 있는 application flow 호출
     */
    @RequestMapping(value = "/Flows/appFlow/{appName}", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public int getAppFlowId(HttpServletRequest request, @PathVariable String appName) {
        log.info("# FlowController.getAppFlowId request flowId >> {}", appName);

        int appFlowId = service.getAppFlowId(appName);

        log.info("# FlowController.getAppFlowId request result >> {}", appFlowId);
        return appFlowId;
    }

    /*
     ** 빌더 Flow 삭제
     */
    @RequestMapping(value = "/Flows/delete/{flowId}", method = RequestMethod.DELETE, consumes = "application/json")
    @ResponseBody
    @CrossOrigin
    public void deleteFlow(HttpServletRequest request,  @PathVariable int flowId) {
        service.deleteBuilderScript(flowId);
        service.deleteFlow(flowId);
    }


    /*
     ** 빌더 Flow 삭제
     */
    @RequestMapping(value = "/Flows/delete/sidebar", method = RequestMethod.DELETE, consumes = "application/json")
    @ResponseBody
    @CrossOrigin
    public void deleteFlow(HttpServletRequest request, @RequestBody List<Integer> templateIdList ) {

        for (int flowId : templateIdList) {
            service.deleteBuilderScript(flowId);
            service.deleteFlow(flowId);
        }

    }

    /*
     ** 빌더 Flow 삭제
     */
    @RequestMapping(value = "/Flows/update/sidebar", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    @CrossOrigin
    public void updateFlow(HttpServletRequest request, @RequestBody FlowUpdateVO flowUpdateVO) {

        for (int i = 0; i < flowUpdateVO.getIdList().size(); i++) {
            int id = flowUpdateVO.getIdList().get(i);
            String title = flowUpdateVO.getTitleList().get(i);

            service.updateFlow(title, id);

        }

    }
}
