package ai.maum.mvp_maker.editor.controller;

import ai.maum.mvp_maker.editor.service.EngineService;
import ai.maum.mvp_maker.model.EngineGroupVO;
import ai.maum.mvp_maker.model.EngineVO;
import ai.maum.mvp_maker.model.UserVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@Controller
@RequestMapping(value="/mvp/api")
@CrossOrigin
public class EngineController {

    @Autowired
    private EngineService service;

    /*
    ** 엔진 그룹 목록 조회
    */
    @RequestMapping(value="/EngineGroups", method= RequestMethod.GET)
    @ResponseBody
    public List<EngineGroupVO> getEngineGroupList(@RequestParam(name="lang", required = false, defaultValue = "en") String lang) {
        List<EngineGroupVO> listGroup = service.getEngineGroupList(lang);

        //log.info("EngineController.getEngineGroupList >> {}", listGroup.toString());
        return listGroup;
    }

    /*
    ** 지정 엔진 그룹의 엔진 목록 조회
    */
    @RequestMapping(value="/EngineGroups/{group_id}", method= RequestMethod.GET)
    @ResponseBody
    public List<EngineVO> getEngineList(HttpServletRequest request, @PathVariable final int group_id, @RequestParam(name="lang", required = false, defaultValue = "en") String lang) {
        UserVO userVO = (UserVO) request.getSession().getAttribute("user");
        List<EngineVO> listEngine = service.getEngineList(group_id, lang, userVO);

        //log.info("EngineController.getEngineList >> {}", listEngine.toString());
        return listEngine;
    }
}
