package ai.maum.mvp_maker.common.enums;

import lombok.Getter;

import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

@Getter
public enum AvatarModelName {
    MODEL_SORA("short_lipsync_server"),
    MODEL_KYEONGSOO("lipsync_teacher_man"),
    MODEL_CHULMIN("pcm"),
    MODEL_MIGYEONG("mktv"),
    MODEL_LCJ("lcj"),
    MODEL_JESONG("jesong"),
    MODEL_INSOOK("busan"),
    MODEL_MICHAEL("michael"),
    MODEL_SEUNGMIN("smyoo"),
    MODEL_HEEJU("heeju"),
    MODEL_LOGAN("logan"),
    MODEL_YESUNG("YYS"),
    MODEL_JKW1("kwjeon01"),
    MODEL_JKW2("kwjeon02"),
    MODEL_JKW3("kwjeon03"),
    MODEL_JKW4("kwjeon04"),
    MODEL_JKW5("kwjeon05"),
    MODEL_KYJ("yoojinkim");


    private static final Map<String, AvatarModelName> AVATAR_MODEL_NAME_MAP = Stream.of(values()).collect(toMap(e -> e.getName(), e -> e));

    private String name;

    AvatarModelName(String name) {
        this.name = name;
    }

    public static AvatarModelName fromValue(String value) {
        return AVATAR_MODEL_NAME_MAP.get(value);
    }

}
