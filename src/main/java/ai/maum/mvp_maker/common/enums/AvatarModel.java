package ai.maum.mvp_maker.common.enums;

import ai.maum.mvp_maker.exception.ModelNotExistException;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.Optional;


@Getter
public enum AvatarModel {
    MODEL_SORA_AVATAR("7drt-6tdf-9ek8-dkr8", 1, AvatarModelName.MODEL_SORA),
    MODEL_KYEONGSOO_AVATAR("8ikn-76f5-ctgf-6lj0", 2, AvatarModelName.MODEL_KYEONGSOO),
    MODEL_CHULMIN_AVATAR("0lug-gfrt-654e-dos4", 3, AvatarModelName.MODEL_CHULMIN),
    MODEL_MIGYEONG_AVATAR("temp-mktv", 4, AvatarModelName.MODEL_MIGYEONG),
    MODEL_LCJ_AVATAR("3kji-i39j-f9jd-3j9j", 5, AvatarModelName.MODEL_LCJ),
    MODEL_JESONG_AVATAR("djf9-3bub-d28d-j393", 6, AvatarModelName.MODEL_JESONG),
    MODEL_INSOOK_AVATAR("dk2s-awo1-2ldi-ejdw", 7, AvatarModelName.MODEL_INSOOK),
    MODEL_MICHAEL_AVATAR("d33k-j80s-2k1s-hd59", 8, AvatarModelName.MODEL_MICHAEL),
    MODEL_SEUNGMIN_AVATAR("swr3-rdfg-5gsd-22e3",9,AvatarModelName.MODEL_SEUNGMIN),
    MODEL_HEEJU_AVATAR("dip2-skmu-td11-po32",10,AvatarModelName.MODEL_HEEJU),
    MODEL_LOGAN_AVATAR("39df-n03j-7u28-ef3h", 11 ,AvatarModelName.MODEL_LOGAN),
    MODEL_YESUNG_AVATAR("dkls-132h-hj1k-znp0",12,AvatarModelName.MODEL_YESUNG),
    MODEL_JKW1_AVATAR("hpdk-zjcq-llrd-z0w6",13,AvatarModelName.MODEL_JKW1),
    MODEL_JKW2_AVATAR("kw51-7vhk-yt2n-2lny",14,AvatarModelName.MODEL_JKW2),
    MODEL_JKW3_AVATAR("b0v7-g5ii-7mmn-vs01",15,AvatarModelName.MODEL_JKW3),
    MODEL_JKW4_AVATAR("w73x-5um7-cd5k-lm8h",16,AvatarModelName.MODEL_JKW4),
    MODEL_JKW5_AVATAR("odva-lr95-jdqb-kw0t",17,AvatarModelName.MODEL_JKW5),
    MODEL_KYJ_AVATAR("zjf9-3nm0-390w-y23q",18,AvatarModelName.MODEL_KYJ);


    private String licenseKey;
    private int modelId;
    private AvatarModelName modelName;

    AvatarModel(String licenseKey, int modelId, AvatarModelName modelName) {
        this.licenseKey = licenseKey;
        this.modelId = modelId;
        this.modelName = modelName;
    }

    public static AvatarModel convertToAvatarModel(AvatarModelName modelName) {
        Optional.ofNullable(modelName).orElseThrow(() -> new ModelNotExistException(HttpStatus.INTERNAL_SERVER_ERROR, "Not Exist Model"));

        switch (modelName) {
            case MODEL_SORA:
                return AvatarModel.MODEL_SORA_AVATAR;
            case MODEL_KYEONGSOO:
                return AvatarModel.MODEL_KYEONGSOO_AVATAR;
            case MODEL_CHULMIN:
                return AvatarModel.MODEL_CHULMIN_AVATAR;
            case MODEL_MIGYEONG:
                return AvatarModel.MODEL_MIGYEONG_AVATAR;
            case MODEL_LCJ:
                return AvatarModel.MODEL_LCJ_AVATAR;
            case MODEL_JESONG:
                return AvatarModel.MODEL_JESONG_AVATAR;
            case MODEL_INSOOK:
                return AvatarModel.MODEL_INSOOK_AVATAR;
            case MODEL_MICHAEL:
                return AvatarModel.MODEL_MICHAEL_AVATAR;
            case MODEL_SEUNGMIN:
                return AvatarModel.MODEL_SEUNGMIN_AVATAR;
            case MODEL_HEEJU:
                return AvatarModel.MODEL_HEEJU_AVATAR;
            case MODEL_LOGAN:
                return AvatarModel.MODEL_LOGAN_AVATAR;
            case MODEL_YESUNG:
                return AvatarModel.MODEL_YESUNG_AVATAR;
            case MODEL_JKW1:
                return AvatarModel.MODEL_JKW1_AVATAR;
            case MODEL_JKW2:
                return AvatarModel.MODEL_JKW2_AVATAR;
            case MODEL_JKW3:
                return AvatarModel.MODEL_JKW3_AVATAR;
            case MODEL_JKW4:
                return AvatarModel.MODEL_JKW4_AVATAR;
            case MODEL_JKW5:
                return AvatarModel.MODEL_JKW5_AVATAR;
            case MODEL_KYJ:
                return AvatarModel.MODEL_KYJ_AVATAR;
            default:
                throw new ModelNotExistException(HttpStatus.INTERNAL_SERVER_ERROR, "Not Exist Model");
        }
    }
}
