package ai.maum.mvp_maker.common.service;

import ai.maum.mvp_maker.editor.mapper.User_Mapper;
import ai.maum.mvp_maker.model.UserSecurityVO;
import ai.maum.mvp_maker.model.UserVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService {

    private final User_Mapper mapper;

    public UserVO getUser(String email) {
        return mapper.getUser(email);
    }

    public UserVO loadUserByUserEmail(String email) {
        UserVO user = mapper.getUser(email);
        log.info("USER : {}", user.getEmail());

        if (user == null){
            throw new UsernameNotFoundException(email);
        }else{
            return user;
        }
    }
}
