package ai.maum.mvp_maker.common.service;

import ai.maum.mvp_maker.editor.mapper.Avatar_Mapper;
import ai.maum.mvp_maker.editor.mapper.Template_Mapper;
import ai.maum.mvp_maker.model.BuilderAvatarVO;
import ai.maum.mvp_maker.model.BuilderTemplateVO;
import groovy.util.logging.Slf4j;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class TemplateService {

    private final Template_Mapper templateMapper;

    public List<BuilderTemplateVO> getTemplateList(String lang) {

        List<BuilderTemplateVO> templateList = templateMapper.getTemplateList();

        if (lang == null || lang.equalsIgnoreCase("ko")) {
            for (int i = 0; i < templateList.size(); i++) {
                templateList.get(i).setDescriptionList(getDescriptionList(templateList.get(i).getDescription()));
            }
        } else {
            templateList.forEach(template -> {
                template.setTitle(template.getTitleEN());
                template.setDescription(template.getDescriptionEN());
                template.setDescriptionList(getDescriptionList(template.getDescriptionEN()));
            });
        }

        return templateList;
    }

    public List<String> getDescriptionList(String description) {

        List<String> descriptionList = new ArrayList<>();

        for (String descriptions : description.split(",")) {
            descriptionList.add(descriptions);
        }

        return descriptionList;

    }

}
