package ai.maum.mvp_maker.common.controller;

import ai.maum.mvp_maker.common.service.TemplateService;
import ai.maum.mvp_maker.model.UserVO;
import ai.maum.mvp_maker.runner.service.BuilderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 메인 컨트롤러
 *
 * @author unongko
 * @version 1.0
 */

@Slf4j
@Controller
@RequiredArgsConstructor
public class HomeController {
    @Value("${url.maum-ai}")
    String mUrl_Maum;

    @Value("${human.media.download.location}")
    String mediaServerDownloadLocation;

    @Value("${human.media.upload.location}")
    String mediaServerUploadLocation;

    private final BuilderService builderService;

    private final TemplateService templateService;

    @GetMapping("/")
    public ModelAndView intro(HttpServletRequest request, HttpServletResponse response, Model model) {
        ModelAndView modelAndView = new ModelAndView();

        UserVO user = (UserVO) request.getSession().getAttribute("user");
        model.addAttribute("user", user);
        model.addAttribute("maum_url", mUrl_Maum);
        model.addAttribute("flowList", builderService.getFlowListForBuilder(user.getUserNo()));
        model.addAttribute("templateList", templateService.getTemplateList(request.getParameter("lang")));
        modelAndView.setViewName("intro");
        return modelAndView;
    }

    @GetMapping("/ai-builder")
    public ModelAndView aiBuilder(HttpServletRequest request, HttpServletResponse response, Model model) {
        ModelAndView modelAndView = new ModelAndView();

        UserVO user = (UserVO) request.getSession().getAttribute("user");

        if (request.getParameter("flowId") != null) {
            // 현재 세션 유저 번호와 flowId에 해당하는 userNo가 다를 경우 Redirect 처리
            Boolean redirectFlag = builderService.checkFlowOwnerUser(user.getUserNo(), request.getParameter("flowId"));
            if (!redirectFlag) {
                modelAndView.setViewName("redirect:/");
                return modelAndView;
            }
        }

        model.addAttribute("user", user);
        model.addAttribute("maum_url", mUrl_Maum);
        model.addAttribute("template", request.getParameter("template"));
        model.addAttribute("templateId", request.getParameter("templateId"));
        model.addAttribute("projectName", request.getParameter("projectName"));
        model.addAttribute("flag", request.getParameter("flag"));
        model.addAttribute("flowId", request.getParameter("flowId"));
        modelAndView.setViewName("home");
        return modelAndView;
    }

    @GetMapping("/avatar-builder")
    public ModelAndView avatarBuilder(HttpServletRequest request, HttpServletResponse response, Model model) {
        ModelAndView modelAndView = new ModelAndView();

        UserVO user = (UserVO) request.getSession().getAttribute("user");

        if (request.getParameter("flowId") != null) {
            // 현재 세션 유저 번호와 flowId에 해당하는 userNo가 다를 경우 Redirect 처리
            Boolean redirectFlag = builderService.checkFlowOwnerUser(user.getUserNo(), request.getParameter("flowId"));
            if (!redirectFlag) {
                modelAndView.setViewName("redirect:/");
                return modelAndView;
            }
        }

        model.addAttribute("user", user);
        model.addAttribute("maum_url", mUrl_Maum);
        model.addAttribute("avatarList", builderService.getAvatarInfoList(user));
        model.addAttribute("templateId", request.getParameter("template"));
        model.addAttribute("projectName", request.getParameter("projectName"));

        model.addAttribute("background", builderService.getBackgroundList());
        model.addAttribute("userBackground", builderService.getUserBackgroundList(user.getUserNo()));
        model.addAttribute("builderFlowId", request.getParameter("flowId"));

        modelAndView.setViewName("avatar");
        return modelAndView;
    }

    @GetMapping("/landing")
    public String landing(HttpServletRequest request, HttpServletResponse response) {
        return "landing";
    }

    @GetMapping("/enLanding")
    public String enLanding(HttpServletRequest request, HttpServletResponse response) {
        return "landing_en";
    }
}