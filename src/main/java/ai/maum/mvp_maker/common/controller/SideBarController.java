package ai.maum.mvp_maker.common.controller;


import ai.maum.mvp_maker.common.service.TemplateService;
import ai.maum.mvp_maker.model.BuilderTemplateVO;
import ai.maum.mvp_maker.model.FlowVO;
import ai.maum.mvp_maker.model.UserVO;
import ai.maum.mvp_maker.runner.service.BuilderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/sideBar")
public class SideBarController {

    private final TemplateService templateService;

    private final BuilderService builderService;

    @PostMapping("/init")
    @ResponseBody
    public List<BuilderTemplateVO> sideBarInit(HttpServletRequest request) {
        return templateService.getTemplateList(request.getParameter("lang"));
    }

    @PostMapping("/flow")
    @ResponseBody
    public List<FlowVO> sideBarInitFlow(HttpServletRequest request) {
        UserVO user = (UserVO) request.getSession().getAttribute("user");
        return builderService.getFlowListForBuilder(user.getUserNo());
    }

}
