package ai.maum.mvp_maker.common.controller;

import ai.maum.mvp_maker.exception.BaseException;
import ai.maum.mvp_maker.media.Media;
import ai.maum.mvp_maker.model.*;
import ai.maum.mvp_maker.runner.service.BuilderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/builder")
public class BuilderRestController {

    private final Media media;
    private final BuilderService builderService;

    @PostMapping("/flow:save")
    public ResponseEntity saveFlow(HttpServletRequest request, @Valid @RequestBody BuilderFlowVO builderFlowVO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(bindingResult.getAllErrors());
        }
        UserVO user = (UserVO) request.getSession().getAttribute("user");
        builderFlowVO.setUserNo(user.getUserNo());
        builderService.saveFlow(builderFlowVO);
        return ResponseEntity.ok().body(builderFlowVO.getFlowId());
    }

    @GetMapping("/flow")
    public ResponseEntity getFlow(int flowId) {
        BuilderFlowVO flowVO = new BuilderFlowVO();
        flowVO.setScriptList(builderService.getScriptAsFlowId(flowId));
        flowVO.setProjectName(builderService.getProjectName(flowId));
        return ResponseEntity.ok().body(flowVO);
    }

    @PostMapping("/background")
    @ResponseBody
    public ResponseEntity getBackgroundList(HttpServletRequest request) {
        return ResponseEntity.ok().body(builderService.getBackgroundList());
    }

    @PostMapping("/userBackground")
    @ResponseBody
    public ResponseEntity getUserBackgroundList(HttpServletRequest request) {
        UserVO user = (UserVO) request.getSession().getAttribute("user");
        return ResponseEntity.ok().body(builderService.getUserBackgroundList(user.getUserNo()));
    }

    @PostMapping("/userBackground:save")
    @ResponseBody
    public ResponseEntity saveUserBackground(HttpServletRequest request, @RequestParam MultipartFile file) {
        UserVO user = (UserVO) request.getSession().getAttribute("user");
        builderService.saveUserBackground(user.getUserNo(), file);

        return ResponseEntity.ok().body(builderService.getUserBackgroundList(user.getUserNo()));
    }

    @PostMapping("userBackground:delete")
    public ResponseEntity deleteUserBackground(HttpServletRequest request, @RequestParam Integer id) {
        UserVO user = (UserVO) request.getSession().getAttribute("user");
        builderService.deleteUserBackground(user.getUserNo(), id);

        return ResponseEntity.ok().body(builderService.getUserBackgroundList(user.getUserNo()));
    }

    @PostMapping("/play")
    public ResponseEntity playScript(HttpServletRequest request, @RequestBody BuilderScriptDTO builderScriptDTO) {
        try {
            UserVO user = (UserVO) request.getSession().getAttribute("user");
            ResponsePlayScriptVO responseVO = builderService.playScript(builderScriptDTO, user.getEmail(), false);
            return ResponseEntity.ok().body(responseVO);
        } catch (BaseException e) {
            log.error(e.getMessage());
            return ResponseEntity.status(e.getStatusCode()).body(BuilderRestResponseError.builder().code(e.getCode()).message(e.getMessage()).build());
        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @PostMapping("/play:all")
    public ResponseEntity playScript(HttpServletRequest request, @RequestBody List<BuilderScriptDTO> builderScriptDTOList) {
        try {
            UserVO user = (UserVO) request.getSession().getAttribute("user");
            List<ResponsePlayScriptVO> responseList = builderService.playAllScript(builderScriptDTOList, user.getEmail());
            return ResponseEntity.ok().body(responseList);
        } catch (BaseException e) {
            log.error(e.getMessage());
            return ResponseEntity.status(e.getStatusCode()).body(BuilderRestResponseError.builder().code(e.getCode()).message(e.getMessage()).build());
        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @PostMapping("/checkLicense")
    public ResponseEntity checkLicense(HttpServletRequest request) {
        UserVO user = (UserVO) request.getSession().getAttribute("user");

        return ResponseEntity.ok().body(builderService.checkLicense(user.getEmail()));
    }
}
