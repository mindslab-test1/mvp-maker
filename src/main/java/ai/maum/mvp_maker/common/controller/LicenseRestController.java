package ai.maum.mvp_maker.common.controller;

import ai.maum.mvp_maker.model.LicenseRequsetDTO;
import ai.maum.mvp_maker.model.LicenseResponseDTO;
import ai.maum.mvp_maker.model.UserVO;
import ai.maum.mvp_maker.runner.service.LicenseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/license")
public class LicenseRestController {

    //TODO RestException Handler 정의 필요

    private final LicenseService licenseService;

    @PostMapping("/valid")
    public ResponseEntity validLicenseKey(@RequestBody LicenseRequsetDTO licenseRequsetDTO) {
        log.info("valid LicenseKey => " + licenseRequsetDTO.getLicenseKey());
        LicenseResponseDTO response = licenseService.validLicenseKey(licenseRequsetDTO.getLicenseKey());
        return ResponseEntity.ok().body(response);
    }

    @PostMapping("/insert")
    public ResponseEntity insertLicenseKey(HttpServletRequest request, @RequestBody LicenseRequsetDTO licenseRequsetDTO) {
        log.info("insert LicenseKey => " + licenseRequsetDTO.getLicenseKey());
        UserVO user = (UserVO) request.getSession().getAttribute("user");
        licenseRequsetDTO.setEmail(user.getEmail());
        LicenseResponseDTO response = licenseService.insertLicenseKey(licenseRequsetDTO);
        log.info("insert LicenseKey response => " + response);
        return ResponseEntity.ok().body(response);
    }

}
