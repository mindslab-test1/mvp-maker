package ai.maum.mvp_maker.common.controller;

import ai.maum.mvp_maker.common.model.AvatarUsageUpdateReq;
import ai.maum.mvp_maker.common.model.ConsultUsageUpdateReq;
import ai.maum.mvp_maker.common.utils.WaterMarkAnImage;
import ai.maum.mvp_maker.editor.mapper.License_Mapper;
import ai.maum.mvp_maker.exception.BaseException;
import ai.maum.mvp_maker.exception.NotInsertUserLicenseKeyException;
import ai.maum.mvp_maker.model.*;
import ai.maum.mvp_maker.runner.service.AiBuilderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/ex-api")
public class AiBuilderController {

    @Value("${spring.profiles.active}")
    private String profile;

    private final AiBuilderService aiBuilderService;
    private final License_Mapper license_mapper;
    private final WaterMarkAnImage waterMark;

    @PostMapping("/flowParam")
    public ResponseEntity getFlowParam(HttpServletRequest request, @RequestBody FlowParamReqVo req) {
        return ResponseEntity.ok().body(aiBuilderService.getFlowParam(req));
    }

    @GetMapping("/purchasedList")
    public ResponseEntity<List<PurchasedVO>> getPurchasedList(HttpServletRequest request, @RequestParam(value = "email") String email) {
        return ResponseEntity.ok().body(aiBuilderService.getPurchasedList(email));
    }

    //TODO Only work on localhost
    @PostMapping(value = "/watermark")
    public ResponseEntity getWaterMark(@RequestParam MultipartFile file) {
        if (profile.equalsIgnoreCase("local")) {
            String filename = FilenameUtils.getBaseName(file.getOriginalFilename());
            String extension = FilenameUtils.getExtension(file.getOriginalFilename());

            ByteArrayOutputStream baos = waterMark.drawWaterMarkToUserBackgroundAnImage(file, extension);

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename + ".png")
                    .contentType(MediaType.IMAGE_PNG)
                    .body(baos.toByteArray());
        }
        return null;
    }

    //TODO 시간 주입을 위한 임시 API입니다. 곧 제거 예정입니다.
    @PostMapping(value = "/usage:update", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity updateRemainedUsage(@RequestBody AvatarUsageUpdateReq req) {
        String email = req.getEmail();
        String licenseKey = req.getLicenseKey();
        Integer usage = req.getUsage();

        try {
            log.error("usage:update request => " + email);
            Integer id = license_mapper.selectRecentInsertLicenseUserId(email, licenseKey).orElseThrow(() -> new NotInsertUserLicenseKeyException(HttpStatus.NOT_FOUND, "최근에 라이센스 키를 등록하지 않았습니다."));
            license_mapper.insertUsage(id, usage);
            return ResponseEntity.ok().body(license_mapper.getLicenseUsageList(email, licenseKey));
        } catch (NotInsertUserLicenseKeyException e) {
            log.error(e.getLocalizedMessage());
            return ResponseEntity.status(e.getStatusCode()).body(BuilderRestResponseError.builder().code(e.getCode()).message(e.getMessage()).build());
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("담당자에게 문의 주세요.");
        }
    }

    //TODO 상담형 템플릿 사용량 체크를시 필요한 남은 시간, 기간을 주입하기 위한 API입니다. 곧 제거 예정입니다.
    @PostMapping(value = "/consult/usage:update", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity updateConsultUsage(@RequestBody ConsultUsageUpdateReq req) {
        String email = req.getEmail();
        String licenseKey = req.getLicenseKey();
        Integer usage = req.getUsage();
        Integer period = req.getPeriod();
        try {
            log.error("consult-usage:update request => " + email);
            Integer id = license_mapper.selectRecentInsertLicenseUserId(email, licenseKey).orElseThrow(() -> new NotInsertUserLicenseKeyException(HttpStatus.NOT_FOUND, "최근에 라이센스 키를 등록하지 않았습니다."));
            List<ShopifyLicenseDTO> activeConsults = license_mapper.getActiveConsultList(email, licenseKey);
            if (activeConsults.size() < 1) {
                license_mapper.insertConsultUsage(id, usage, period, null);
            } else {
                ShopifyLicenseDTO latestActiveConsult = activeConsults.get(activeConsults.size() - 1);
                LocalDateTime startDate = latestActiveConsult.getConsultStartDate().plusMonths(latestActiveConsult.getConsultPeriod());
                license_mapper.insertConsultUsage(id, usage, period, startDate);
            }
            return ResponseEntity.ok().body(license_mapper.getLicenseUsageList(email, licenseKey));
        } catch (NotInsertUserLicenseKeyException e) {
            log.error(e.getLocalizedMessage());
            return ResponseEntity.status(e.getStatusCode()).body(BuilderRestResponseError.builder().code(e.getCode()).message(e.getMessage()).build());
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("담당자에게 문의 주세요.");
        }
    }

    @PostMapping(value = "/consult/usage")
    public ResponseEntity consultUsage(HttpServletRequest request, @RequestBody FlowParamReqVo req) {
        try {
            BaseResponse response = aiBuilderService.consultUsage(req.getFlowId());
            return ResponseEntity.ok().body(response);
        } catch (BaseException e) {
            log.error(e.getMessage());
            return ResponseEntity.status(e.getStatusCode()).body(BuilderRestResponseError.builder().code(e.getCode()).message(e.getMessage()).build());
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("담당자에게 문의 주세요.");
        }
    }

    // human.maum.ai 에서 사용시 사용량 차감을 위한 api
    @PostMapping(value = "/consult/deduct")
    public ResponseEntity consultDeduct(HttpServletRequest request, @RequestBody FlowParamReqVo req) {
        try {
            BaseResponse response = aiBuilderService.consultDeductUsage(req.getFlowId());
            return ResponseEntity.ok().body(response);
        } catch (BaseException e) {
            log.error(e.getMessage());
            return ResponseEntity.status(e.getStatusCode()).body(BuilderRestResponseError.builder().code(e.getCode()).message(e.getMessage()).build());
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("담당자에게 문의 주세요.");
        }
    }
}
