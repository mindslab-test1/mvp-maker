package ai.maum.mvp_maker.common.controller;

import ai.maum.mvp_maker.common.utils.SendMail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

@Slf4j
@Controller
@RequestMapping(value = "/sendmail")
public class SendMailController {

    @Autowired
    private SendMail sendMail;

    @RequestMapping(value = "/reqMail")
    @ResponseBody
    public String reqMail(@RequestParam (value = "fromAddr") String fromAddr,
                          @RequestParam (value = "toAddr")String toAddr,
                          @RequestParam (value = "subject")String subject,
                          @RequestParam (value = "msg")String msg) throws Exception {
        log.debug(" @ Hello SendMailController >> fromAddr: {}, toAddr: {}, Subject: {}", fromAddr, toAddr, subject);

        return sendMail.sendPostWithForm(fromAddr, toAddr, subject, msg);
    }
}
