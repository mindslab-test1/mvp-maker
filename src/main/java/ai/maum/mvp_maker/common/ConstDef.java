package ai.maum.mvp_maker.common;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class ConstDef {

    @Value("${url.ai-builder}")
    private String myDomain;

    @Value("${maum.SigningKey}")
    private String singKey;

    @Value("${url.hq}")
    private String ssoUrl;

    @Value("${sso.client-id}")
    private String clientId;

    @Value("${hq.logoutPath}")
    private String logoutPath;

}
