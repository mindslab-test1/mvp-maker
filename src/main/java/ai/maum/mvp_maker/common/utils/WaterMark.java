package ai.maum.mvp_maker.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Slf4j
@Component
public class WaterMark {

    public ByteArrayOutputStream drawWaterMarkToUserBackground(MultipartFile image, String extension) {
        String waterMarkText = "MindslabMindslabMindslab";
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        String[] splitArr = image.getOriginalFilename().split("\\.");

        try {
            ByteArrayResource inputImageResource = new ByteArrayResource(image.getBytes());
            BufferedImage bufferedImage = ImageIO.read(inputImageResource.getInputStream());
            // Generate Graphic Context And Anti-aliasing
            Graphics2D g2d = (Graphics2D) bufferedImage.getGraphics();
            g2d.scale(1, 1);
            g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));
            g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

            // create watermark text shape for rendering
            Font font = new Font(Font.SANS_SERIF, Font.PLAIN, 18);
            GlyphVector fontGV = font.createGlyphVector(g2d.getFontRenderContext(), waterMarkText);
            Rectangle size = fontGV.getPixelBounds(g2d.getFontRenderContext(), 0, 0);
            Shape textShape = fontGV.getOutline();
            double textWidth = size.getWidth();
            double textHeight = size.getHeight() * 3; // text spacing
            AffineTransform rotate45 = AffineTransform.getRotateInstance(Math.PI / 5d);
            Shape rotatedText = rotate45.createTransformedShape(textShape);

            // Use Gradient that repeats 4 times
            g2d.setPaint(new GradientPaint(0 ,0, new Color(0f, 0f, 0f, 0.1f), bufferedImage.getWidth()/2, bufferedImage.getHeight()/2
                    , new Color(0f, 0f, 0f, 0.1f)));
            g2d.setStroke(new BasicStroke(1f));

            // using pythagoras + 5 pixel adding(y 방향으로 스텝을 조정)
            double yStep = Math.sqrt(textWidth * textWidth / 2);
            log.info("======> yStep: " + yStep);

            for (double x = -textHeight * 3; x < bufferedImage.getWidth(); x += (textHeight * 3)) {
                double y = -yStep;
                for (; y < bufferedImage.getHeight(); y += yStep) {
                    g2d.draw(rotatedText);
                    g2d.fill(rotatedText);
                    g2d.translate(0, yStep);
                }
                g2d.translate(textHeight * 3, -(y + yStep));
            }

            ImageIO.write(bufferedImage, extension, byteArrayOutputStream);
            g2d.dispose();

        } catch (IOException e) {
            log.error(e.getLocalizedMessage());
            throw new RuntimeException("Failed to draw watermark to user background");
        }

        return byteArrayOutputStream;
    }

}
