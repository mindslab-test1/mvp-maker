package ai.maum.mvp_maker.common.utils;

public class UsageUtil {

    private static final String MATCH = "[^\uAC00-\uD7A3xfe0-9a-zA-Z]";

    public static int convertStringToUsage(String text) {
        String regMatchTxt = text.replaceAll(MATCH, "");
        return regMatchTxt.length();
    }

    private UsageUtil() {}

}
