package ai.maum.mvp_maker.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class FilterWordsUtil {

    private static List<String> swearWordList = null;
    private static List<String> allowedWordList = null;
    private static final String matchRegEx = "[^가-힣a-zA-Z0-9]";

    static {
        try {
            ClassPathResource classPathResource = new ClassPathResource("words.txt");
            InputStreamReader isr = new InputStreamReader(classPathResource.getInputStream(), StandardCharsets.UTF_8);
            swearWordList = new BufferedReader(isr).lines().collect(Collectors.toList());

            ClassPathResource allowedWordsPath = new ClassPathResource("words_allowed.txt");
            InputStreamReader allowedIsr = new InputStreamReader(allowedWordsPath.getInputStream(), StandardCharsets.UTF_8);
            allowedWordList = new BufferedReader(allowedIsr).lines().collect(Collectors.toList());
        } catch (IOException e) {
            log.error(e.getLocalizedMessage());
        }
    }

    // 욕설 필터
    public static boolean checkSwearWord(String text) {
        String filterRegExpText = text.replaceAll(matchRegEx, "");
        for(String t: allowedWordList){
            filterRegExpText = filterRegExpText.replaceAll(t, "");
        }
        String swearWords = swearWordList.stream().filter(filterRegExpText::contains).distinct().collect(Collectors.joining(","));
        boolean result = swearWords.isEmpty();
        if (!result) log.error("[욕설 필터링] => " + swearWords);
        return result;
    }

    // 미완성 단어 필터
    public static boolean checkIncompleteWord(String text){
        String filterRegExpText = text.replaceAll(matchRegEx, "");
        return filterRegExpText.isEmpty();
    }

    private FilterWordsUtil() {}
}
