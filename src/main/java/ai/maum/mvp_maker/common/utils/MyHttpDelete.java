package ai.maum.mvp_maker.common.utils;

import org.apache.http.annotation.NotThreadSafe;
import org.apache.http.client.methods.HttpPost;

/*
** HttpDelete의 경우, setEntity()를 지원하지 않는 문제가 있음.
*/
@NotThreadSafe
public class MyHttpDelete extends HttpPost {
    public MyHttpDelete(String url){
        super(url);
    }
    @Override
    public String getMethod() {
        return "DELETE";
    }
}
