package ai.maum.mvp_maker.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;

@Slf4j
@Component
public class WaterMarkAnImage {

    @Value("${watermark.url}")
    String watermarkImgUrl;

    public ByteArrayOutputStream drawWaterMarkToUserBackgroundAnImage(MultipartFile file, String extension) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ByteArrayResource inputImageResource = new ByteArrayResource(file.getBytes());
            BufferedImage inputImage = ImageIO.read(inputImageResource.getInputStream());
            BufferedImage overlay = resize(ImageIO.read(new URL(watermarkImgUrl)), inputImage.getHeight(), inputImage.getWidth());

            // determine image type and handle correct transparency
            int imageType = "png".equalsIgnoreCase(extension) ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB;
            BufferedImage watermarked = new BufferedImage(inputImage.getWidth(), inputImage.getHeight(), imageType);

            // initializes necessary graphic properties
            Graphics2D w = (Graphics2D) watermarked.getGraphics();
            w.drawImage(inputImage, 0, 0, null);
            AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f);
            w.setComposite(alphaChannel);

            // calculates the coordinate where the String is painted
            int centerX = (inputImage.getWidth() / 2) -  (overlay.getWidth() / 2);
            int centerY = (inputImage.getHeight() / 2) - (overlay.getHeight() / 2);

            // add text watermark to the image
            w.drawImage(overlay, centerX, centerY, null);
            ImageIO.write(watermarked, extension, byteArrayOutputStream);
            w.dispose();

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to draw watermark to user background");
        }

        return byteArrayOutputStream;
    }

    private BufferedImage resize(BufferedImage img, int height, int width) {
        Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = resized.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
        return resized;
    }

}
