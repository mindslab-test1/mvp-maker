package ai.maum.mvp_maker.common.model;

import lombok.Data;

@Data
public class AvatarUsageUpdateReq {
    private String email;
    private String licenseKey;
    private Integer usage;
}
