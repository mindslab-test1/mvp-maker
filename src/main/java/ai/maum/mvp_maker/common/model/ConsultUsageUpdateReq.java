package ai.maum.mvp_maker.common.model;

import lombok.Data;

@Data
public class ConsultUsageUpdateReq {
    private String email;
    private String licenseKey;
    private Integer usage;
    private Integer period;
}
