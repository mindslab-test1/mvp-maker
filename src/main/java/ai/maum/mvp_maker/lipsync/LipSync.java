package ai.maum.mvp_maker.lipsync;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class LipSync {

    @Value("${api.id}")
    String API_ID;

    @Value("${api.key}")
    String API_KEY;
    @Autowired
    RestTemplate restTemplate;


    private final Logger logger = LoggerFactory.getLogger(LipSync.class);

    public LipSyncUploadResponse runUpload(String text, String model, String backgroundUrl) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        ByteArrayResource resource = getBackground(backgroundUrl);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("apiId", API_ID);
        body.add("apiKey", API_KEY);
        body.add("text", text);
        if (resource != null) {
            body.add("image", resource);
        }
        if (model != null) {
            body.add("model", model);
        }
        body.add("transparent", "false");
        body.add("resolution", "FHD");
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
        return restTemplate.postForObject("https://api.maum.ai/lipsync/upload", requestEntity, LipSyncUploadResponse.class);
    }

    public LipSyncCheckResponse runCheck(String requestKey) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        LipSyncCheckRequest body = new LipSyncCheckRequest(API_ID, API_KEY, requestKey);
        HttpEntity<LipSyncCheckRequest> requestEntity = new HttpEntity<>(body, headers);
        ResponseEntity<LipSyncCheckResponse> response = restTemplate.exchange("https://api.maum.ai/lipsync/statusCheck", HttpMethod.POST, requestEntity, new ParameterizedTypeReference<LipSyncCheckResponse>() {
        });

        return response.getBody();
    }

    public Resource runDownload(String requestKey) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        LipSyncDownloadRequest body = new LipSyncDownloadRequest(API_ID, API_KEY, requestKey);
        HttpEntity<LipSyncDownloadRequest> requestEntity = new HttpEntity<>(body, headers);

        byte[] response = restTemplate.postForObject("https://api.maum.ai/lipsync/download", requestEntity, byte[].class);
        if (response == null)
            throw new NullPointerException();

        return new ByteArrayResource(response) {
            @Override
            public String getFilename() {
                return requestKey + ".mp4";
            }
        };
    }

    private ByteArrayResource getBackground(String backgroundUrl) {
        if (backgroundUrl == null)
            return null;

        byte[] response = restTemplate.getForObject(backgroundUrl, byte[].class);
        if (response == null)
            throw new NullPointerException();

        String[] backgroundUrlArr = backgroundUrl.split("\\.");
        String extension = backgroundUrlArr[backgroundUrlArr.length - 1];

        return new ByteArrayResource(response) {
            @Override
            public String getFilename() {
                return "background." + extension;
            }
        };
    }
}
