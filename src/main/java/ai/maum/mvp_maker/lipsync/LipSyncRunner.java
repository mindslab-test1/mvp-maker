package ai.maum.mvp_maker.lipsync;

import ai.maum.mvp_maker.media.Media;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class LipSyncRunner {

    @Autowired
    LipSync lipSync;

    @Autowired
    Media media;

    private static final Long _processing = 1L;
    private static final Long _finished = 2L;

    private static final Long LIPSYNC_STATUS_STATUS_SLEEP_MILLIS = 5000L;

    public Resource run(String answerString, String model, String backgroundUrl) throws IllegalStateException, InterruptedException {
        LipSyncUploadResponse lipSyncUploadResult = lipSync.runUpload(answerString, model, backgroundUrl);

        String requestKey = lipSyncUploadResult.payload.requestKey;

        log.error(requestKey);

        Long prevWaiting = 0L;
        Long lastStatusCode = 0L;
        while (true) {
            LipSyncCheckResponse lipSyncCheckResult = lipSync.runCheck(requestKey);
            LipSyncCheckResponse.Payload payload = lipSyncCheckResult.getPayload();
            Long statusCode = payload.getStatusCode();
            Long waiting = payload.getWaiting();

            if (waiting >= 0 && !waiting.equals(prevWaiting)) {
                prevWaiting = waiting;
            }

            if (statusCode.equals(_processing)) {
                lastStatusCode = _processing;
                break; // 처리 중
            }
            if (statusCode.equals(_finished)) {
                lastStatusCode = _finished;
                break; // 완료됨
            }
            if (statusCode > _finished) {
                throw new IllegalStateException("Unexpected status code from LipSync status check #1");
            }

            Thread.sleep(LIPSYNC_STATUS_STATUS_SLEEP_MILLIS);
        }

        while (!lastStatusCode.equals(_finished)) {
            LipSyncCheckResponse lipSyncCheckResult = lipSync.runCheck(requestKey);
            LipSyncCheckResponse.Payload payload = lipSyncCheckResult.getPayload();
            lastStatusCode = payload.getStatusCode();
            if (lastStatusCode > _finished)
                throw new IllegalStateException("Unexpected status code from LipSync status check #2");

            Thread.sleep(LIPSYNC_STATUS_STATUS_SLEEP_MILLIS);
        }

        return lipSync.runDownload(requestKey);
    }
}
