package ai.maum.mvp_maker.lipsync;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LipSyncCheckResponse {
    private Message message;
    private Payload payload;

    @Getter
    @Setter
    public static class Message {
        private String message;
        private Long status;
    }

    @Getter
    @Setter
    public static class Payload {
        private Long statusCode;
        private String message;
        private Long waiting;
    }
}
