package ai.maum.mvp_maker.lipsync;

import lombok.Data;

@Data
public class LipSyncUploadResponse {
    public Message message;
    public Payload payload;

    @Data
    public static class Message {
        public String message;
        public Long status;
    }

    @Data
    public static class Payload {
        public String requestKey;
    }
}
