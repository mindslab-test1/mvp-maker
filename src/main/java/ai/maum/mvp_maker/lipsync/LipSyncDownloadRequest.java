package ai.maum.mvp_maker.lipsync;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LipSyncDownloadRequest {
    private String apiId;
    private String apiKey;
    private String requestKey;
}
