package ai.maum.mvp_maker.lipsync;

public class LipSyncCheckRequest {
    public String apiId;
    public String apiKey;
    public String requestKey;

    public LipSyncCheckRequest(String apiId, String apiKey, String requestKey) {
        this.apiId = apiId;
        this.apiKey = apiKey;
        this.requestKey = requestKey;
    }
}
