package ai.maum.mvp_maker.lipsync;

import java.io.File;

public class LipSyncUploadRequest {
    public String apiId;
    public String apiKey;
    public String text;
    public File image;
    public String resolution;

    public LipSyncUploadRequest(String apiId, String apiKey, String text, File image, String resolution) {
        this.apiId = apiId;
        this.apiKey = apiKey;
        this.text = text;
        this.image = image;
        this.resolution = resolution;
    }
}
