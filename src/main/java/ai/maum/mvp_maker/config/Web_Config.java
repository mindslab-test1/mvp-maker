package ai.maum.mvp_maker.config;

import org.apache.tomcat.util.http.LegacyCookieProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.resource.VersionResourceResolver;

import java.util.Locale;

/**
 * 스프링부트 WebMVC Config
 *
 * @author unongko
 * @version 1.0
 */

@Configuration
@EnableWebMvc
public class Web_Config implements WebMvcConfigurer {

    @Autowired
    @Qualifier(value = "loginInterceptor")
    private HandlerInterceptor interceptor;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/css/**").addResourceLocations("/css/").resourceChain(false).addResolver(new VersionResourceResolver().addContentVersionStrategy("/**"));
        registry.addResourceHandler("/js/**").addResourceLocations("/js/").resourceChain(false).addResolver(new VersionResourceResolver().addContentVersionStrategy("/**"));
        registry.addResourceHandler("/font/**").addResourceLocations("/font/");
        registry.addResourceHandler("/webfonts/**").addResourceLocations("/webfonts/");
        registry.addResourceHandler("/audio/**").addResourceLocations("/audio/");
        registry.addResourceHandler("/image/**").addResourceLocations("/image/");
        registry.addResourceHandler("/favicon.ico").addResourceLocations("/");
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

    @Bean
    public WebServerFactoryCustomizer<TomcatServletWebServerFactory> cookieProcessorCustomizer() {
        return (factory) -> factory.addContextCustomizers(
                (context) -> context.setCookieProcessor(new LegacyCookieProcessor()));
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(interceptor)
                .addPathPatterns("/**")
                .excludePathPatterns(
                        "/security/**",
                        "/landing",
                        "/enlanding",
                        "/js/**",
                        "/font/**",
                        "/webfonts/**",
                        "/audio/**",
                        "/image/**",
                        "/favicon.ico",
                        "/mvp/api/**",
                        "/css/**",
                        "/ex-api/**")
//                .excludePathPatterns("/**")
        ;
    }


//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/**") //모든 요청에 대해서
//                .allowedOrigins("*"); //허용할 오리진들
//    }

}
