package ai.maum.mvp_maker.config;

import ai.maum.mvp_maker.auth.exif.SSOExternalAPI;
import ai.maum.mvp_maker.auth.filter.JwtAuthenticationFilter;
import ai.maum.mvp_maker.common.ConstDef;
import ai.maum.mvp_maker.common.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * 스프링부트 스프링시큐리티 Config
 *
 * @author unongko
 * @version 1.0
 */

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class Security_Config extends WebSecurityConfigurerAdapter {

    private final UserService userService;
    private final SSOExternalAPI ssoExternalAPI;
    private final ConstDef constDef;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.headers().frameOptions().sameOrigin();
        http
            .csrf().disable()
            .authorizeRequests()
                .antMatchers("/login").permitAll()
                .antMatchers("/admin/**").hasAuthority("ROLE_ADMIN")
            .anyRequest()
                .authenticated()
                .and()
            .formLogin()
                .loginPage("/landing")
                .failureUrl("/landing")
                .usernameParameter("loginId")
                .passwordParameter("password")
//                .successHandler(loginSuccessHandler)
//                .failureHandler(loginFailureHandler)
            .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .invalidateHttpSession(true)
            .and()
                .exceptionHandling()
                .accessDeniedPage("/accessDenied");
        http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
                .antMatchers(
                        "/js/**",
                        "/css/**",
                        "/font/**",
                        "/image/**",
                        "/webfonts/**",
                        "/favicon**",
                        "/landing",
                        "/security/**",
                        "/ex-api/**")
        ;
    }

    public JwtAuthenticationFilter jwtAuthenticationFilter() {
        JwtAuthenticationFilter filter = new JwtAuthenticationFilter(userService, ssoExternalAPI, constDef);
        return filter;
    }
}