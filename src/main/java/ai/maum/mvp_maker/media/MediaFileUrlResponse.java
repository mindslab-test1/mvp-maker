package ai.maum.mvp_maker.media;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class MediaFileUrlResponse {
    private String fullPathUrl;
    private String pathUrl;
}
