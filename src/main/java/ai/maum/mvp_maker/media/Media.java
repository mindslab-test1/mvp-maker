package ai.maum.mvp_maker.media;

import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;

@Service
public class Media {
    private final Logger logger = LoggerFactory.getLogger(Media.class);

    @Autowired
    RestTemplate restTemplate;

    @Value("${human.media.location}")
    String mediaServerLocation;

    @Value("${human.media.download.location}")
    String mediaServerDownloadLocation;

    @Value("${human.media.upload.location}")
    String mediaServerUploadLocation;

    public String convertDownloadUrl(String uploadUrl) {
        return uploadUrl.replace(mediaServerLocation, mediaServerDownloadLocation);
    }

    public MediaUrl getUrl(String filename) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HashMap<String, Object> body = new HashMap<>();
        body.put("filename", filename);
        body.put("uploadDirectory", mediaServerUploadLocation);
        HttpEntity<HashMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
        ResponseEntity<MediaFileUrlResponse> response = restTemplate.exchange(mediaServerLocation + "/media/file/path:get", HttpMethod.POST, requestEntity, new ParameterizedTypeReference<MediaFileUrlResponse>() {});
        MediaFileUrlResponse rbody = response.getBody();
        if (rbody == null) throw new NullPointerException();
        logger.info(rbody.toString());
        return new MediaUrl(rbody.getPathUrl(), rbody.getFullPathUrl());
    }

    public Boolean uploadToMediaServer(MultipartFile file, String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.set("file", file.getResource());
        body.set("uploadDirectory", url.substring(6));
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
        ResponseEntity<String> response = restTemplate.exchange(mediaServerLocation + "/media/file:upload", HttpMethod.POST, requestEntity, new ParameterizedTypeReference<String>() {});
        String rbody = response.getBody();
        if (rbody == null) throw new NullPointerException();
        logger.info(rbody);
        return response.getStatusCode() == HttpStatus.CREATED;
    }

    public Boolean uploadToMediaServer(Resource file, String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.set("file", file);
        body.set("uploadDirectory", url.substring(6));
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
        ResponseEntity<String> response = restTemplate.exchange(mediaServerLocation + "/media/file:upload", HttpMethod.POST, requestEntity, new ParameterizedTypeReference<String>() {});
        String rbody = response.getBody();
        if (rbody == null) throw new NullPointerException();
        logger.info(rbody);
        return response.getStatusCode() == HttpStatus.CREATED;
    }
}
