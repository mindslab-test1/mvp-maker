package ai.maum.mvp_maker.media;

public class MediaUrl {
    public String pathUrl;
    public String fullUrl;

    public MediaUrl(String pathUrl, String fullUrl) {
        this.pathUrl = pathUrl;
        this.fullUrl =  fullUrl;
    }
}