package ai.maum.mvp_maker.media;

import ai.maum.mvp_maker.common.utils.GetMediaUrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MediaFileUrl implements WebMvcConfigurer {

    @Value("${human.media.full-path}")
    String mediaUrl;

    @Autowired
    public void getUrl() {
        GetMediaUrl.imageUrl = mediaUrl;
    }
}
