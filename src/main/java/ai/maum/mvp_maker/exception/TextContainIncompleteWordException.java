package ai.maum.mvp_maker.exception;

import org.springframework.http.HttpStatus;

public class TextContainIncompleteWordException extends BaseException {
    private HttpStatus statusCode;
    private String message;
    private static final String EXCEPTION_CODE = "contain_incomplete";

    public TextContainIncompleteWordException(HttpStatus statusCode, String message) {
        super(statusCode, EXCEPTION_CODE, message);
        this.statusCode = statusCode;
        this.message = message;
    }
}
