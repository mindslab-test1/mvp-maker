package ai.maum.mvp_maker.exception;


import org.springframework.http.HttpStatus;

public class UsageCalcException extends BaseException {
    private final HttpStatus statusCode;
    private final String message;

    public UsageCalcException(HttpStatus statusCode, String message) {
        super(statusCode, "can't_calc", message);
        this.statusCode = statusCode;
        this.message = message;
    }
}
