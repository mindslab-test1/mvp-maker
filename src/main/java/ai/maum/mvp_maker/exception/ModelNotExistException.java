package ai.maum.mvp_maker.exception;

import org.springframework.http.HttpStatus;

public class ModelNotExistException extends BaseException{
    private HttpStatus statusCode;
    private String message;

    public ModelNotExistException(HttpStatus statusCode, String message) {
        super(statusCode, "not_exist", message);
        this.statusCode = statusCode;
        this.message = message;
    }
}
