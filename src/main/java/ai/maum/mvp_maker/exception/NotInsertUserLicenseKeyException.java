package ai.maum.mvp_maker.exception;

import org.springframework.http.HttpStatus;

public class NotInsertUserLicenseKeyException extends BaseException {
    private HttpStatus statusCode;
    private String msg;
    public NotInsertUserLicenseKeyException(HttpStatus statusCode,String msg) {
        super(statusCode, "not_insert", msg);
        this.statusCode = statusCode;
        this.msg = msg;
    }
}
