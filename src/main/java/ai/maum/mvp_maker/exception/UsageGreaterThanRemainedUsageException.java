package ai.maum.mvp_maker.exception;

import org.springframework.http.HttpStatus;

public class UsageGreaterThanRemainedUsageException extends BaseException {
    private HttpStatus statusCode;
    private String message;

    public UsageGreaterThanRemainedUsageException(HttpStatus statusCode, String message) {
        super(statusCode, "remainedUsageException", message);
        this.statusCode = statusCode;
        this.message = message;
    }
}
