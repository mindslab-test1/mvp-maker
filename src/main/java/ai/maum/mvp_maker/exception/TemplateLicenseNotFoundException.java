package ai.maum.mvp_maker.exception;

import org.springframework.http.HttpStatus;

public class TemplateLicenseNotFoundException extends BaseException{
    private final HttpStatus statusCode;
    private final String message;

    public TemplateLicenseNotFoundException(HttpStatus statusCode, String message) {
        super(statusCode, "not_found", message);
        this.statusCode = statusCode;
        this.message = message;
    }
}
