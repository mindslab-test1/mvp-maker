package ai.maum.mvp_maker.exception;

import org.springframework.http.HttpStatus;

public class TextContainSwearWordException extends BaseException {
    private HttpStatus statusCode;
    private String message;
    private static final String EXCEPTION_CODE = "contain_swear";

    public TextContainSwearWordException(HttpStatus statusCode, String message) {
        super(statusCode, EXCEPTION_CODE, message);
        this.statusCode = statusCode;
        this.message = message;
    }
}
