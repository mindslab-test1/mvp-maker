package ai.maum.mvp_maker.runner.controller;

import ai.maum.mvp_maker.runner.service.EngineApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping(value="/mvp/runner/api/utils")
public class UtilApiController {

    @Autowired
    private EngineApiService service;

    /*
    ** 일정 시간 대기 후, 응답
    */
    @RequestMapping(value="/delay", produces = "application/text; charset=utf8")
    @ResponseBody
    public String runDelay(@RequestParam(value = "delay_msec") int delay_msec) throws Exception {
        Thread.sleep(delay_msec, 0);
        return "succ";
    }

}
