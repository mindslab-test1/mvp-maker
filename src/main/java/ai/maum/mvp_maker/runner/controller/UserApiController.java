package ai.maum.mvp_maker.runner.controller;

import ai.maum.mvp_maker.runner.service.AudioFileConverter_Service;
import ai.maum.mvp_maker.runner.service.EngineApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Slf4j
@Controller
@RequestMapping(value="/mvp/runner/api")
public class UserApiController {

    @Autowired
    private EngineApiService service;

    @Autowired
    private AudioFileConverter_Service audioFileConverter_service;

    @RequestMapping(value="/textConv", produces = "application/text; charset=utf8")
    @ResponseBody
    public String runApi_GPT(   @RequestParam(value = "inputData") String input_data,
                                @RequestParam(value = "pattern") String pattern
    ) throws Exception {
        return pattern.replace("#{input}", input_data);
    }


    @RequestMapping(value = "/audioFileConverter")
    @ResponseBody
    public File runApi_AudioFileConverter(  @RequestParam(value = "file") MultipartFile file,
                                            @RequestParam(value = "sampleRate") int sampleRate,
                                            @RequestParam(value = "channel") int channel,
                                            @RequestParam(value = "bitDepth") int bitDepth) throws IOException {
        log.info(" @ Hello audioFileConverter in UserApiController ! ");

        return audioFileConverter_service.convertFile(file, sampleRate, channel, bitDepth);
    }

}
