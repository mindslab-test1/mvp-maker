package ai.maum.mvp_maker.runner.service;

import ai.maum.mvp_maker.model.LicenseRequsetDTO;
import ai.maum.mvp_maker.model.LicenseResponseDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;


@Slf4j
@Service
@RequiredArgsConstructor
public class LicenseService {

    @Value("${url.hq}")
    String MAUM_SSO_URL;

    private final RestTemplate restTemplate;

    public LicenseResponseDTO validLicenseKey(String licenseKey) {
        MultiValueMap<String, String> requestValueMap = new LinkedMultiValueMap<>();
        requestValueMap.add("license_key", licenseKey);
        return restTemplate.postForObject(MAUM_SSO_URL + "/shopify/validateLicense", requestValueMap, LicenseResponseDTO.class);
    }

    public LicenseResponseDTO insertLicenseKey(LicenseRequsetDTO licenseRequsetDTO) {
        MultiValueMap<String, String> requestValueMap = new LinkedMultiValueMap<>();
        requestValueMap.add("email", licenseRequsetDTO.getEmail());
        requestValueMap.add("license_key", licenseRequsetDTO.getLicenseKey());
        return restTemplate.postForObject(MAUM_SSO_URL + "/shopify/insertLicense", requestValueMap, LicenseResponseDTO.class);
    }



}
