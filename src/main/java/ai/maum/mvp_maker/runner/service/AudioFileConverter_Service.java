package ai.maum.mvp_maker.runner.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Slf4j
@Service
public class AudioFileConverter_Service {

    @Value("${upload.dir}")
    String UPLOAD_DIR;

    @Value("${api.id}")
    String API_ID;

    @Value("${api.key}")
    String API_KEY;

    public File convertFile(MultipartFile file, int originSampleRate, int channel, int bitDepth) throws IOException {
        log.info(" @ Hello convertFile service ! ");

        log.info("Sampling rate : {} / Channel(s) : {} / Bit depth : {}", originSampleRate, channel, bitDepth );

        String sampleRate = "";
        if(originSampleRate == 8000)        sampleRate = "8k";
        else if(originSampleRate == 16000)  sampleRate = "16k";
        else if(originSampleRate == 22000)  sampleRate = "22k";
        else                                sampleRate = "44k";

        String uploadPath = UPLOAD_DIR + "/convertAudioFile";
        File varfile = new File(uploadPath);
        if (varfile.exists() == false) {
            varfile.mkdirs();
        }
        varfile = new File((uploadPath +"/"+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));
        file.transferTo(varfile);

        /* ------------------------------------------------------------- */
        //프로젝트 파일 외의 서버 프로세스 실행
        /* ------------------------------------------------------------- */
        Runtime rt = Runtime.getRuntime();
        Process pc = null;

        try {
            log.info(" @ convertFile service ==> run SOX process : " + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1) );

            String resultPath = UPLOAD_DIR + "/soxResult";

            File result = new File(resultPath);
            if (result.exists() == false) {
                result.mkdirs();
            }
            result = new File((resultPath +"/"+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));;

            /* 리눅스 계열 --> 실제 리눅스 코드 예시 : sox ACY896_20190812150706_193.wav -b 16 -c 1 -r 22k -t wav convertTest.wav */
            String cmdStr = "sox " + varfile + " -b " + bitDepth + " -c " + channel + " -r " + sampleRate + " -t wav " + result;
            /* 윈도우 local test */
            //String cmdStr = "C:\\Program Files (x86)\\sox-14-4-2\\sox.exe " + varfile + " -b " + bitDepth + " -c " + channel + " -r " + sampleRate + " -t wav " + result;

            pc = rt.exec(cmdStr);
            pc.waitFor();
            pc.destroy();

            log.info(" @ convertFile service ==> finish SOX process : " + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1) );

            return result;

        } catch (Exception e) {
            log.error(" @ convertFile service ==> Exception : " + e.getMessage());
            e.printStackTrace();

            return null;
        }
    }
}