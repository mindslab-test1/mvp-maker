package ai.maum.mvp_maker.runner.service;

import ai.maum.mvp_maker.editor.mapper.EngineGroup_Mapper;
import ai.maum.mvp_maker.editor.mapper.Engine_Mapper;
import ai.maum.mvp_maker.model.EngineGroupParamVO;
import ai.maum.mvp_maker.model.EngineGroupVO;
import ai.maum.mvp_maker.model.EngineVO;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class EngineApiService {
    final String API_SERVER = "https://api.maum.ai";

    @Value("${upload.dir}")
    String UPLOAD_DIR;

    @Value("${api.id}")
    String API_ID;

    @Value("${api.key}")
    String API_KEY;

    public String runApi_GPT(String context, String lang) throws Exception {
        String url = API_SERVER + "/api/gpt/";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("apiId", "minds-api-performance-test");
        paramMap.put("apiKey", "4SBRGrq9");
        paramMap.put("lang", lang);
        paramMap.put("context", context);

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(paramMap);
        String resultMsg = sendPost(url, json);

        return resultMsg;
    }


    private String sendPost(String sendUrl, String jsonValue) throws Exception {
        StringBuffer outResult = new StringBuffer();
        StringBuffer result = new StringBuffer();

        URL url = new URL(sendUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Accetp-Charset", "UTF-8");
        conn.setConnectTimeout(10000);
        conn.setReadTimeout(60000);

        OutputStream os = conn.getOutputStream();
        os.write(jsonValue.getBytes("UTF-8"));
        os.flush();

        int nResponseCode = conn.getResponseCode();
        if(nResponseCode == HttpURLConnection.HTTP_OK) {

            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"), 8);
            String inputLine = "";
            while((inputLine = br.readLine()) != null) {
                outResult.append(inputLine);
                inputLine = inputLine.replace("\\n", "<br>");
                try {
                    result.append(new String(URLDecoder.decode(inputLine, "UTF-8")));
                } catch(Exception ex) {
                    result.append(inputLine);
                }
            }
            System.out.println(result.toString());
            System.out.println(outResult.toString());
            br.close();
        } else {
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"), 8);
            String inputLine = "";
            while((inputLine = br.readLine()) != null) {
                outResult.append(inputLine);
                inputLine = inputLine.replace("\\n", "<br>");
                try {
                    result.append(new String(URLDecoder.decode(inputLine, "UTF-8")));
                } catch(Exception ex) {
                    result.append(inputLine);
                }
            }
            System.out.println("Response Result : " + result.toString().replace('+',' '));
            System.out.println("API 호출 에러 발생 : 에러코드=" + nResponseCode);

            throw new Exception("api error");
        }
        conn.disconnect();

        return result.toString();
    }



    public String getApiStt(MultipartFile file, String lang, String level, String sampling) {
        try {
            String url = "https://api.maum.ai/api/stt/";

            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);

            /* 음원 데이타를 Multipart로 등록하기 위한 준비 */
            String uploadPath = UPLOAD_DIR + "/stt";
            File varfile = new File(uploadPath);
            if (varfile.exists() == false) {
                varfile.mkdirs();
            }
            varfile = new File((uploadPath +"/"+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));
            file.transferTo(varfile);
            FileBody fileBody = new FileBody(varfile);

            /* Multipart 등록 */
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("file", fileBody);
            builder.addPart("lang", new StringBody(lang, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("level", new StringBody(level, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("sampling", new StringBody(sampling, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("ID", new StringBody(API_ID, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("key", new StringBody(API_KEY, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("cmd", new StringBody("runFileStt", ContentType.MULTIPART_FORM_DATA));
            HttpEntity entity = builder.build();
            post.setEntity(entity);

            /* STT API 실행 */
            HttpResponse response = client.execute(post);

            int responseCode = response.getStatusLine().getStatusCode();
            /* 정상 응답 수신 */
            if (responseCode == 200) {
                String result = EntityUtils.toString(response.getEntity(), "UTF-8");
                log.info("# EngineApiService.getApiStt >> succ >> result >> {}", result);
                return result;

            }
            /* 에러 응답 수신 */
            else {
                String result = EntityUtils.toString(response.getEntity(), "UTF-8");

                log.info("# EngineApiService.getApiStt >> fail >> code = {} >> result >> {}", responseCode, result);

                System.out.println("Response Result : " + result.replace('+', ' '));
                System.out.println("API 호출 에러 발생 : 에러코드=" + responseCode);
                return "{ \"status\": \"error\" }";
            }

        } catch (Exception e) {
            log.info("# EngineApiService.getApiStt >> fail >> exception >> {}", e.getMessage());
            e.printStackTrace();
            return "{ \"status\": \"error\" }";
        }
    }
}
