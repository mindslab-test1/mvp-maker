package ai.maum.mvp_maker.runner.service;

import ai.maum.mvp_maker.editor.mapper.Avatar_Mapper;
import ai.maum.mvp_maker.editor.mapper.Flow_Mapper;
import ai.maum.mvp_maker.editor.mapper.License_Mapper;
import ai.maum.mvp_maker.exception.UsageGreaterThanRemainedUsageException;
import ai.maum.mvp_maker.model.*;
import ai.maum.mvp_maker.runner.model.FlowInfoResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class AiBuilderService {

    private final Avatar_Mapper avatarMapper;
    private final Flow_Mapper flowMapper;
    private final License_Mapper license_mapper;


    public List<PurchasedVO> getPurchasedList(String email) {
        try {
            List<PurchasedVO> list = avatarMapper.getPurchasedList(email);
            return list;
        } catch (Exception e) {
            return null;
        }
    }

    @Transactional
    public FlowInfoResponse getFlowParam(FlowParamReqVo req) {
        try {
            FlowInfoResponse flowInfoResponse = new FlowInfoResponse();

            if (req.getFlowId() == -100) {
                flowInfoResponse.setAvatar("short_lipsync_server");
                flowInfoResponse.setBackground(1L);
                flowInfoResponse.setScenario(529L);
                return flowInfoResponse;
            }

            if (req.getFlowId() == -200) {
                flowInfoResponse.setAvatar("short_lipsync_server");
                flowInfoResponse.setBackground(2L);
                flowInfoResponse.setScenario(628L);
                return flowInfoResponse;
            }

            if (req.getFlowId() == -300) {
                flowInfoResponse.setAvatar("lipsync_teacher_man");
                flowInfoResponse.setBackground(3L);
                flowInfoResponse.setScenario(529L);
                return flowInfoResponse;
            }

            if (req.getFlowId() == -400) {
                flowInfoResponse.setAvatar("pcmcolor_lipsync_server");
                flowInfoResponse.setBackground(6L);
                flowInfoResponse.setScenario(529L);
                return flowInfoResponse;
            }

            if (req.getFlowId() == -2000) {
                flowInfoResponse.setAvatar("short_lipsync_server");
                flowInfoResponse.setBackground(2L);
                flowInfoResponse.setScenario(684L);
                return flowInfoResponse;
            }

            if (req.getFlowId() == -2100) {
                flowInfoResponse.setAvatar("short_lipsync_server");
                flowInfoResponse.setBackground(1L);
                flowInfoResponse.setScenario(684L);
                return flowInfoResponse;
            }

            if (req.getFlowId() == -5000) {
                flowInfoResponse.setAvatar("short_lipsync_server");
                flowInfoResponse.setBackground(1L);
                flowInfoResponse.setScenario(716L);
                return flowInfoResponse;
            }

            List<FlowParamVO> flowParamVO = flowMapper.getFlowParam(req.getFlowId());
            for (FlowParamVO value : flowParamVO) {
                if (value.getEngineId() == 1075) {
                    flowInfoResponse.setScenario(Long.parseLong(value.getValue()));
                }
                if (value.getEngineId() == 1076) {
                    if (value.getName().equals("model")) {
                        BuilderAvatarVO builderAvatarVO = avatarMapper.getAvatar(Integer.parseInt(value.getValue()));
                        flowInfoResponse.setAvatar(builderAvatarVO.getModel());
                    }
                    if (value.getName().equals("image")) {
                        flowInfoResponse.setBackground(Long.parseLong(value.getValue()));
                    }
                }
            }

            String email = flowMapper.getEmailByFlowId(req.getFlowId());
            flowInfoResponse.setEmail(email);

            return flowInfoResponse;
        } catch (Exception e) {
            return null;
        }
    }

    @Transactional
    public BaseResponse consultUsage(Integer flowId) {
        UserLicenseDTO user = license_mapper.getUserByFlowId(flowId);
        int remainingUsage = getRemainUsage(user.getEmail(), user.getLicenseKey(), user.getAvatarId());
        if (remainingUsage <= 0) {
            throw new UsageGreaterThanRemainedUsageException(HttpStatus.BAD_REQUEST, "Usage GreaterThan RemainedUsage");
        }
        return new BaseResponse("success", "", new RemainUsageResponse(remainingUsage));
    }

    @Transactional
    public BaseResponse consultDeductUsage(Integer flowId) {
        UserLicenseDTO user = license_mapper.getUserByFlowId(flowId);
        int remainingUsage = getRemainUsage(user.getEmail(), user.getLicenseKey(), user.getAvatarId());
        if (remainingUsage <= 0) {
            throw new UsageGreaterThanRemainedUsageException(HttpStatus.BAD_REQUEST, "Usage GreaterThan RemainedUsage");
        }
        license_mapper.insertConsultUsageLog(user.getEmail(), user.getAvatarId());
        return new BaseResponse("success", "", new RemainUsageResponse(remainingUsage - 1));
    }

    @Transactional
    public Integer getRemainUsage(String email, String licenseKey, int avatarId) {
        List<ShopifyLicenseDTO> activeConsult = license_mapper.getActiveConsultList(email, licenseKey);
        if (activeConsult.size() > 0) {
            int usageLimit = activeConsult.get(0).getConsultUsageLimit();
            LocalDateTime localDate = LocalDateTime.now();
            LocalDateTime usageDate = activeConsult.get(0).getConsultStartDate();
            LocalDate startDate = localDate.getDayOfMonth() >= usageDate.getDayOfMonth() ? LocalDate.of(localDate.getYear(), localDate.getMonthValue(), usageDate.getDayOfMonth()) : LocalDate.of(localDate.getYear(), localDate.getMonthValue() - 1, usageDate.getDayOfMonth());

            ConsultUsageDTO consultUsage = license_mapper.getConsultUsage(email, avatarId, startDate.atStartOfDay());
            int remainUsage = usageLimit - consultUsage.getUsage();
            return remainUsage < 0 ? 0 : remainUsage;
        }
        return 0;
    }

}
