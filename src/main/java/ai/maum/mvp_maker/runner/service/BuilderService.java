package ai.maum.mvp_maker.runner.service;

import ai.maum.mvp_maker.common.consts.BuilderConstData;
import ai.maum.mvp_maker.common.enums.AvatarModel;
import ai.maum.mvp_maker.common.enums.AvatarModelName;
import ai.maum.mvp_maker.common.utils.FilterWordsUtil;
import ai.maum.mvp_maker.common.utils.UsageUtil;
import ai.maum.mvp_maker.common.utils.WaterMarkAnImage;
import ai.maum.mvp_maker.editor.mapper.*;
import ai.maum.mvp_maker.exception.*;
import ai.maum.mvp_maker.lipsync.LipSyncRunner;
import ai.maum.mvp_maker.media.Media;
import ai.maum.mvp_maker.media.MediaUrl;
import ai.maum.mvp_maker.model.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class BuilderService {

    private final LipSyncRunner lipSyncRunner;
    private final Avatar_Mapper avatarMapper;
    private final Background_Mapper backgroundMapper;
    private final User_Background_Mapper userBackgroundMapper;
    private final Flow_Mapper flow_mapper;
    private final Script_Mapper script_mapper;
    private final Media media;
    private final WaterMarkAnImage waterMark;
    private final License_Mapper license_mapper;

    @Value("${watermark.transparent-url}")
    String transparentWatermarkUrl;


    @Transactional(readOnly = true)
    public Boolean checkFlowOwnerUser(int userNo, String flowId) {
        FlowVO flow = flow_mapper.getFlow(Integer.parseInt(flowId));
        return userNo == flow.getUserNo();
    }

    @Transactional(readOnly = true)
    public String getProjectName(int flowId) {
        return flow_mapper.getFlow(flowId).getName();
    }

    @Transactional(readOnly = true)
    public List<BuilderAvatarLicenseVO> getAvatarInfoList(UserVO user) {
        List<BuilderAvatarLicenseVO> list = avatarMapper.getAvatarInfoList(user.getEmail());
        List<BuilderAvatarLicenseVO> filterList = list.stream().filter(e -> !e.getPurchased().equalsIgnoreCase("N") || !e.getPublicAvatar().equalsIgnoreCase("N")).collect(Collectors.toList());
        filterList.forEach(avatar -> {
            if (avatar.getPurchased().equalsIgnoreCase("Y")) {
                int remainedUsage = license_mapper.selectAvatarUsage(user.getEmail(), avatar.getLicenseKey(), AvatarModel.convertToAvatarModel(AvatarModelName.fromValue(avatar.getModel())).getModelId()).get();
                if (remainedUsage > 0) {
                    avatar.setRemainedUsage(NumberFormat.getNumberInstance().format(remainedUsage));
                }
            }
        });
        return filterList;
    }

    @Transactional(readOnly = true)
    public List<BuilderBackgroundVO> getBackgroundList() {
        List<BuilderBackgroundVO> bgList = backgroundMapper.getBackgroundList();
        for (int i = 0; i < bgList.size(); i++) {
            if (bgList.get(i).getDisplayName().equalsIgnoreCase("transparent")) {
                BuilderBackgroundVO temp = bgList.get(i);
                bgList.remove(i);
                bgList.add(temp);
            }
        }
        return bgList;
    }

    @Transactional(readOnly = true)
    public List<BuilderUserBackgroundVO> getUserBackgroundList(int userNo) {
        return userBackgroundMapper.getUserBackgroundList(userNo);
    }

    @Transactional
    public void saveUserBackground(Integer userNo, MultipartFile file) {
        MediaUrl uploadUrl = media.getUrl(file.getOriginalFilename());
        media.uploadToMediaServer(file, uploadUrl.pathUrl);
        String downloadUrl = media.convertDownloadUrl(uploadUrl.fullUrl);

        String[] splitArr = file.getOriginalFilename().split("\\.");
        String extension = splitArr[splitArr.length - 1];
        String fileName = "waterMark_" + file.getOriginalFilename() + "." + extension;
        ByteArrayOutputStream byteArrayOutputStream = waterMark.drawWaterMarkToUserBackgroundAnImage(file, extension);
        MediaUrl mediaUrl = media.getUrl(fileName);
        media.uploadToMediaServer(new ByteArrayResource(byteArrayOutputStream.toByteArray()) {
            @Override
            public String getFilename() {
                return fileName;
            }
        }, mediaUrl.pathUrl);
        String waterMarkDownloadUrl = media.convertDownloadUrl(mediaUrl.fullUrl);

        BuilderUserBackgroundVO background = new BuilderUserBackgroundVO();
        background.setUrl(downloadUrl);
        background.setWaterMarkUrl(waterMarkDownloadUrl);
        background.setUserNo(userNo);
        userBackgroundMapper.insertBackground(background);
    }

    @Transactional
    public void deleteUserBackground(Integer userNo, Integer id) {
        BuilderUserBackgroundVO userBackgroundVO = new BuilderUserBackgroundVO();
        userBackgroundVO.setId(id);
        userBackgroundVO.setUserNo(userNo);
        userBackgroundMapper.deleteUserBackground(userBackgroundVO);
    }

    @Transactional(readOnly = true)
    public List<BuilderScriptVO> getScriptAsFlowId(int flowId) {
        return script_mapper.getScriptAsFlowId(flowId);
    }

    @Transactional(readOnly = true)
    public List<FlowVO> getFlowListForBuilder(int userId) {
        return flow_mapper.getFlowListForBuilder(userId);
    }

    @Transactional
    public void saveFlow(BuilderFlowVO builderFlowVO) {
        LocalDateTime currentDate = LocalDateTime.now();
        builderFlowVO.setCreateDate(currentDate);
        builderFlowVO.setUpdateDate(currentDate);

        boolean existFlowId = builderFlowVO.getFlowId() != 0;

        log.info("existFlowId => " + existFlowId);

        // script 제거
        script_mapper.deleteScriptAsFlowId(builderFlowVO.getFlowId());

        if (existFlowId) {
            flow_mapper.updateBuilderFlow(currentDate, builderFlowVO.getFlowId());
        } else {
            flow_mapper.insertBuilderFlow(builderFlowVO);
        }

        // script insert
        builderFlowVO.getScriptList().forEach(script -> {
            script_mapper.insertScript(builderFlowVO.getFlowId(), script.getAvatarId(), script.getBackgroundUrl(), currentDate, script.getText());
        });

    }

    @Transactional(rollbackFor = {Exception.class})
    public ResponsePlayScriptVO playScript(BuilderScriptDTO builderScriptDTO, String email, boolean checkSwearFlag) {
        ResponsePlayScriptVO response = new ResponsePlayScriptVO();

        if (!checkSwearFlag) {
            if (!FilterWordsUtil.checkSwearWord(builderScriptDTO.getText())) {
                throw new TextContainSwearWordException(HttpStatus.BAD_REQUEST, "request text contain swear words");
            }
        }

        if (FilterWordsUtil.checkIncompleteWord(builderScriptDTO.getText())) {
            throw new TextContainIncompleteWordException(HttpStatus.BAD_REQUEST, "request text is not completed");
        }

        if (builderScriptDTO.getBehavior() != null && builderScriptDTO.getBehavior().equalsIgnoreCase("play")) {
            builderScriptDTO = modifyWaterMark(builderScriptDTO);
        }

        if (builderScriptDTO.getBehavior() != null && builderScriptDTO.getBehavior().equalsIgnoreCase("download")) {
            license_mapper.checkUserTemplateLicenseKey(email, BuilderConstData.AVATAR_TEMPLATE_KEY).orElseThrow(() -> new TemplateLicenseNotFoundException(HttpStatus.NOT_FOUND, "User Does Not Have a Key"));

            AvatarModel thisAvatar = AvatarModel.convertToAvatarModel(AvatarModelName.fromValue(builderScriptDTO.getModel()));

            int remainedUsage = license_mapper.selectAvatarUsage(email, thisAvatar.getLicenseKey(), thisAvatar.getModelId()).orElseThrow(() -> new UsageCalcException(HttpStatus.INTERNAL_SERVER_ERROR, "Can't Calc Limit Data"));
            log.info("User: " + email + " => remainedUsage = " + remainedUsage);

            int convertTextToUsage = UsageUtil.convertStringToUsage(builderScriptDTO.getText());
            log.info("User: " + email + " => convertTextUsage = " + convertTextToUsage);

            // 사용량이 없을 경우
            if (remainedUsage <= 0) {
                builderScriptDTO = modifyWaterMark(builderScriptDTO);
//                throw new RemainedUsageNotExistException(HttpStatus.INTERNAL_SERVER_ERROR, "remained usage is nothing left");
            }
            // 사용량이 부족 할 경우
            if (remainedUsage < convertTextToUsage) {
                builderScriptDTO = modifyWaterMark(builderScriptDTO);
//                throw new UsageGreaterThanRemainedUsageException(HttpStatus.INTERNAL_SERVER_ERROR, "Usage GreaterThan RemainedUsage");
            }

            license_mapper.insertUsageLog(email, 1, thisAvatar.getModelId(), convertTextToUsage);

            response.setRemainedUsage(NumberFormat.getNumberInstance().format(remainedUsage - convertTextToUsage));
        }

        Resource file = null;

        try {
            file = lipSyncRunner.run(builderScriptDTO.getText(), builderScriptDTO.getModel(), builderScriptDTO.getBackgroundUrl());
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
            throw new RuntimeException("Failed to generate avatar video");
        }

        MediaUrl uploadUrl = media.getUrl(file.getFilename());
        media.uploadToMediaServer(file, uploadUrl.pathUrl);
        String downloadUrl = media.convertDownloadUrl(uploadUrl.fullUrl);
        response.setDownloadUrl(downloadUrl);
        return response;
    }

    private BuilderScriptDTO modifyWaterMark(BuilderScriptDTO builderScriptDTO) {
        if (builderScriptDTO.getBackgroundUrl() == null) builderScriptDTO.setBackgroundUrl(transparentWatermarkUrl);

        Optional<String> defaultBackground = backgroundMapper.getWaterMarkUrl(builderScriptDTO.getBackgroundUrl());
        String waterMarkUrl = defaultBackground.orElseGet(() -> userBackgroundMapper.getUserWaterMarkUrl(builderScriptDTO.getBackgroundUrl()).get());
        builderScriptDTO.setBackgroundUrl(waterMarkUrl);
        return builderScriptDTO;
    }

    @Transactional(rollbackFor = {Exception.class})
    public List<ResponsePlayScriptVO> playAllScript(List<BuilderScriptDTO> builderScriptDTOList, String email) {
        List<ResponsePlayScriptVO> downloadList = new ArrayList<>();
        int swearCheckResult = builderScriptDTOList.stream().filter(dto -> FilterWordsUtil.checkSwearWord(dto.getText()) == false).collect(Collectors.counting()).intValue();
        if (swearCheckResult != 0) {
            throw new TextContainSwearWordException(HttpStatus.BAD_REQUEST, "request text contain swear words");
        }
        builderScriptDTOList.forEach(vo -> {
            ResponsePlayScriptVO responseVO = playScript(vo, email, true);
            downloadList.add(responseVO);
        });
        return downloadList;
    }

    public boolean checkLicense(String email) {
        return license_mapper.checkUserTemplateLicenseKey(email, "jhdj-kh8h-eegt-661q").isPresent() ? true : false;
    }
}
