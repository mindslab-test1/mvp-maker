package ai.maum.mvp_maker.runner.model;

import ai.maum.mvp_maker.model.FlowParamVO;
import lombok.Data;

import java.util.List;

@Data
public class FlowInfoResponse {
    private String avatar;
    private Long scenario;
    private Long background;
    private String email;
}
