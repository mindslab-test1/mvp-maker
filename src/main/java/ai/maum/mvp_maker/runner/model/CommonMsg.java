package ai.maum.mvp_maker.runner.model;

import lombok.Data;

@Data
public class CommonMsg {
    private String message;
    private Integer status;
}
