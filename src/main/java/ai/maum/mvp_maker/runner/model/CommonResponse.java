package ai.maum.mvp_maker.runner.model;

import lombok.Data;

@Data
public class CommonResponse {
    private CommonMsg commonMsg;
    private Object result;
}
