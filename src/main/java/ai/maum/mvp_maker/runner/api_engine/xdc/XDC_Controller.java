package ai.maum.mvp_maker.runner.api_engine.xdc;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Slf4j
@Controller
@RequestMapping(value="/mvp/runner/api")
public class XDC_Controller {

    @Autowired
    private XDC_Service service;

    @RequestMapping(value="/xdc", produces = "application/text; charset=utf8")
    @ResponseBody
    public String runApi_XDC(   @RequestParam(value = "context") String context
    ) throws Exception {

        return service.runApi_XDC(context);

    }
}
