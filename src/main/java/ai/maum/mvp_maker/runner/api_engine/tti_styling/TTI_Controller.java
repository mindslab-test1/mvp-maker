package ai.maum.mvp_maker.runner.api_engine.tti_styling;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping(value="/mvp/runner/api")
public class TTI_Controller {

    @Autowired
    private TTI_Service service;

    @RequestMapping("/tti")
    @ResponseBody
    public ResponseEntity<byte[]> getApiTti(@RequestParam(value = "reqText") String reqText) {

        return service.getApiTti(reqText);
    }

}
