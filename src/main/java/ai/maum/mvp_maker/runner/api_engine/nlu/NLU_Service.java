package ai.maum.mvp_maker.runner.api_engine.nlu;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class NLU_Service {
    final String API_SERVER = "https://api.maum.ai";

    @Value("${upload.dir}")
    String UPLOAD_DIR;

    @Value("${api.id}")
    String API_ID;

    @Value("${api.key}")
    String API_KEY;

    public String getApiNlu(String lang, String reqText) {
        try {
            String url = API_SERVER + "/api/nlu";

            ObjectMapper mapper = new ObjectMapper();
            Map<String, String> map = new HashMap<>();

            map.put("lang", lang);
            map.put("reqText", reqText);
            map.put("apiId", API_ID);
            map.put("apiKey", API_KEY);

            String jsonStr = mapper.writeValueAsString(map);

            CloseableHttpClient client = HttpClientBuilder.create().build();

            HttpPost post = new HttpPost(url);
            post.setHeader("Content-Type", "application/json");
            StringEntity entity = new StringEntity(jsonStr,"UTF-8");

            post.setEntity(entity);

            HttpResponse response = client.execute(post);

            int responseCode = response.getStatusLine().getStatusCode();
            String resData = EntityUtils.toString(response.getEntity(), "UTF-8");

            /* 정상 응답 수신 */
            if (responseCode == 200) {
                log.info("# EngineApiService.getApiNlu >> succ >> result >> {}", resData);
                return resData;
            }
            /* 에러 응답 수신 */
            else {
                log.info("# EngineApiService.getApiNlu >> fail >> code = {} >> result >> {}", responseCode, resData);
                return "{ \"status\": \"error\" }";
            }

        } catch (Exception e) {
            log.info("# EngineApiService.getApiNlu >> fail >> exception >> {}", e.getMessage());
            e.printStackTrace();
        }

        return null;
    }
}
