package ai.maum.mvp_maker.runner.api_engine.subtitle_recog;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

@Slf4j
@Service
public class SubtitleRecog_Service {
    final String API_SERVER = "https://api.maum.ai";

    @Value("${upload.dir}")
    String UPLOAD_DIR;

    @Value("${api.id}")
    String API_ID;

    @Value("${api.key}")
    String API_KEY;

    String getApiSubtitRecog(MultipartFile file){

        log.info("[Engine Subtitle Recognition]");

        try{
            String url = API_SERVER + "/Vca/VCASubtitleExtract";

            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);

            File varFile = new File(UPLOAD_DIR);

            if(!varFile.exists()) {
                varFile.mkdirs();
            }

            varFile = new File((UPLOAD_DIR + "/" + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));
            file.transferTo(varFile);

            FileBody fileBody = new FileBody(varFile);
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();

            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("scene_img", fileBody);
            builder.addPart("apiId", new StringBody(API_ID, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody(API_KEY, ContentType.MULTIPART_FORM_DATA));

            HttpEntity entity = builder.build();

            post.setEntity(entity);
            HttpResponse response = client.execute(post);

            int responseCode = response.getStatusLine().getStatusCode();

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            StringBuilder result = new StringBuilder();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            if (responseCode == 200) {
//                log.info("Response Result : " + result);
                log.info("Response code : " + responseCode);
                return result.toString();

            } else {

                log.error("API 호출 에러 발생 : 에러코드=" + responseCode);
                log.error("Response Result : " + result);
                return "{ \"status\": \"error\" }";

            }

        } catch (Exception e) {
            log.error(" @ SubtitleExtract Exception : {0}", e);
            return "{ \"status\": \"error\" }";
        }

    }
}
