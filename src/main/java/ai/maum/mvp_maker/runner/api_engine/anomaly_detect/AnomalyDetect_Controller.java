package ai.maum.mvp_maker.runner.api_engine.anomaly_detect;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Controller
@RequestMapping(value = "/mvp/runner/api")
public class AnomalyDetect_Controller {

    @Autowired
    private AnomalyDetect_Service service;

    @RequestMapping("/anomalyDetect")
    @ResponseBody
    public String runApi_anomalyDetect(@RequestParam(value = "file") MultipartFile file) {
        return service.runApi_anomalyDetect(file);
    }
}
