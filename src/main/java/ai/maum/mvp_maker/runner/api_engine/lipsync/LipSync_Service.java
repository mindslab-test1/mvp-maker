package ai.maum.mvp_maker.runner.api_engine.lipsync;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class LipSync_Service {

    final String API_SERVER = "https://api.maum.ai";
    final Gson gson = new Gson();

    @Value("${api.id}")
    String API_ID;

    @Value("${api.key}")
    String API_KEY;

    public String getApiLipSync(String text, String model, MultipartFile image) {
        try {
            String url = API_SERVER + "/lipsync/upload";

            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);

            byte[] imageBytes = image.getBytes();

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(API_ID, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("apiKey", new StringBody(API_KEY, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("text", new StringBody(text, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("model", new StringBody(model, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addBinaryBody("image", imageBytes, ContentType.create(image.getContentType()), image.getOriginalFilename());

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);

            int responseCode = response.getStatusLine().getStatusCode();

            log.info("Response Code : " + response.getEntity());

            /* 정상 응답 수신 */
            if (responseCode == 200) {
                Map responseBody = parseResponse(response);
                String result = String.valueOf(((Map) responseBody.get("payload")).get("requestKey"));
                return result;
            }
            /* 에러 응답 수신 */
            else {
                log.info("# EngineApiService.getApiTts >> fail >> code = {} >> result >> {}", responseCode, EntityUtils.toString(response.getEntity(), "UTF-8"));
                return null;
            }

        } catch (Exception e) {
            log.info("# EngineApiService.getApiTts >> fail >> exception >> {}", e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    private Map parseResponse(HttpResponse response) throws IOException {
        BufferedReader bufferedReader = new BufferedReader((new InputStreamReader(response.getEntity().getContent())));

        StringBuilder stringBuilder = new StringBuilder();
        String line = "";
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }

        return gson.fromJson(stringBuilder.toString(), HashMap.class);
    }
}