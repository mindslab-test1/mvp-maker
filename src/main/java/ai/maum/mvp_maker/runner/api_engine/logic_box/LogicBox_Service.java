package ai.maum.mvp_maker.runner.api_engine.logic_box;

import ai.maum.mvp_maker.runner.api_engine.logic_box.model.LogicBox_JsonPath;
import ai.maum.mvp_maker.runner.api_engine.logic_box.model.LogicBox_JsonRequest;
import ai.maum.mvp_maker.runner.api_engine.logic_box.model.LogicBox_ReqParam;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ClassUtils;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@Service
public class LogicBox_Service {
    /* 외부 API request 실행 */
    public String sendRequest(LogicBox_JsonRequest request) {
        String jsonResult = "";
        try {
            /* 외부 API response */
            if (request.getReqMethod().equals("get"))
                jsonResult = sendGet(request);
            else if (request.getReqMethod().equals("post"))
                jsonResult = sendPost(request);
            else
                return null;
            
            /* 외부 API에서 오류가 발생한 경우 그대로 출력 */
            if (jsonResult.contains("_apiRequestError_")) {
                return jsonResult;
            }
            
            /* 사용자 JSON Path/Mapper Param 적용 */
            if (request.getJsonPathList().size() > 0) {
                Map<String, String> mappedResults = mapJsonPath(jsonResult, request.getJsonPathList());
                Object mappedValue = mappedResults.values().toArray()[0];
                if (isSuitableOutputType(mappedValue)) { // 매핑된 결과값이 (정상적으로) 텍스트나 숫자일경우
                    return mappedValue.toString();
                }
                else { // 매핑된 결과값이 Array나 Object일경우 JSON 형식대로 꾸밈
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode node = mapper.convertValue(mappedValue, JsonNode.class);
                    jsonResult = node.toPrettyString();
                }
            }
            /* 사용자 매핑이 일체 없거나, 매핑된 값이 Array나 Object일시 JSON Map Error를 출력 */
            return "_jsonMapError_" + jsonResult;
        } catch (JsonProcessingException e) {
            log.info("# LogicBox_service >> fail >> mappedResults --> Json string >> {}", e.getMessage());
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            log.info("# LogicBox_service >> fail >> API request >> {}", e.getMessage());
            return "_jsonProcessingError_" + jsonResult;
        }
    }
    
    // Checks if mapped value is either a primitive type or a string. (false if value is an object or an array)
    private boolean isSuitableOutputType (Object mappedValue) {
        return ClassUtils.isPrimitiveOrWrapper(mappedValue.getClass()) || mappedValue instanceof String;
    }

    private String sendGet(LogicBox_JsonRequest request) throws Exception {
        /* Request Param 추가 */
        URI uri = new URI(request.getEndpoint());
        if (request.getReqParamList().size() > 0) {
            uri = appendUri(request.getEndpoint(), request.getReqParamList());
        }

        HttpURLConnection conn = (HttpURLConnection) uri.toURL().openConnection();

        /* Header Param 추가 */
        request.getReqHeaderList().forEach(reqHeader -> {
            conn.setRequestProperty(reqHeader.getKey(), reqHeader.getVal());
        });
        log.info("# LogicBox_service >> API request >> {} >> {}", request.getEndpoint(), request.getReqMethod());

        ObjectMapper mapper = new ObjectMapper();
        String jsonString;

        try {
            /* Get response data*/
            JsonNode node = mapper.readTree(conn.getInputStream());
            jsonString = node.toPrettyString();
            log.info("# LogicBox_service >> API request >> succ >> {}", jsonString);
            return jsonString;
        } catch (IOException e) {
            /* Request 실패시 API Request Error 태그 추가*/
            JsonNode node = mapper.readTree(conn.getErrorStream());
            jsonString = node.toPrettyString();
            log.info("# LogicBox_service >> API request >> fail >> {}", jsonString);
            return "_apiRequestError_" + jsonString;
        }
    }


    private String sendPost(LogicBox_JsonRequest request) throws Exception {
        URL url = new URL(request.getEndpoint());
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", request.getContentType());

        /* Header Param 추가 */
        request.getReqHeaderList().forEach(reqHeader -> {
            conn.setRequestProperty(reqHeader.getKey(), reqHeader.getVal());
        });

        // TODO: Enum extract
        conn.setConnectTimeout(10000);
        conn.setReadTimeout(60000);

        String reqBody = "";

        /* Request Body 추가 */
        if (request.getContentType().equals("application/x-www-form-urlencoded")) {
            reqBody = setReqParam(request);
            conn.setRequestProperty("Content-Length", Integer.toString(reqBody.length()));
        } else if (request.getContentType().equals("application/json")) {
            reqBody = request.getReqBody();
            conn.setRequestProperty("Content-Length", Integer.toString(reqBody.length()));
        }

        try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
            wr.write(reqBody.getBytes());
            wr.flush();
        }

        ObjectMapper mapper = new ObjectMapper();
        String jsonString;

        try {
            /* Get response data */
            JsonNode node = mapper.readTree(conn.getInputStream());
            jsonString = node.toPrettyString();
            log.info("# LogicBox_service >> API request >> succ >> {}", jsonString);
            return jsonString;
        } catch (IOException e) {
            /* Request 실패시 API Request Error 태그 추가*/
            JsonNode node = mapper.readTree(conn.getErrorStream());
            jsonString = node.toPrettyString();
            log.info("# LogicBox_service >> API request >> fail >> {}", jsonString);
            return "_apiRequestError_" + jsonString;
        }
    }

    private String setReqParam(LogicBox_JsonRequest request) {
        String reqParamStr = request.getReqParamList().stream()
                .map(reqParam -> reqParam.getKey() + "=" + reqParam.getVal())
                .collect(Collectors.joining("&"));

        return reqParamStr;
    }

    /* 외부 API response mapping */
    private Map<String, String> mapJsonPath(String jsonString, List<LogicBox_JsonPath> jsonPathList) {
        Map<String, String> mappedResults = new HashMap();
        ArrayList<String> pathResults = new ArrayList<>();
        DocumentContext jsonContext = JsonPath.parse(jsonString);

        IntStream.range(0, jsonPathList.size())
                .forEach(idx -> {
                    pathResults.add(jsonContext.read(jsonPathList.get(idx).getJsonPath()));

                    mappedResults.put(jsonPathList.get(idx).getMappingParam(), pathResults.get(idx));
                });
        return mappedResults;
    }

    /* URL에 Request Param 추가 */
    private URI appendUri(String endpoint, List<LogicBox_ReqParam> reqParamList) {
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(endpoint);

        reqParamList.forEach(reqParam -> {
            uriComponentsBuilder.queryParam(reqParam.getKey(), reqParam.getVal());
        });

        return uriComponentsBuilder.build(false).encode().toUri();
    }
}
