package ai.maum.mvp_maker.runner.api_engine.poseExtract;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Controller
@RequestMapping(value = "/mvp/runner/api")
public class PoseExtract_Controller {

    @Autowired
    private PoseExtract_Service service;

    @RequestMapping(value = "/poseExtract")
    @ResponseBody
    public ResponseEntity<byte[]> runApi_PoseExtract(@RequestPart(value = "pose_img") MultipartFile pose_img) {
        System.out.println(" @ Hello Pose Extract Controller ! ");

        if(pose_img.isEmpty())
            return null;

        return service.runApi_PoseExtract(pose_img);
    }
}
