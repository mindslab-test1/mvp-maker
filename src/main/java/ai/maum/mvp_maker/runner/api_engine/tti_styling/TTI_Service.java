package ai.maum.mvp_maker.runner.api_engine.tti_styling;


import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class TTI_Service {

    final String API_SERVER = "https://api.maum.ai";

    @Value("${api.id}")
    String API_ID;

    @Value("${api.key}")
    String API_KEY;

    public ResponseEntity<byte[]> getApiTti(String reqText) {
        System.out.println("======reqText======:: "+reqText);

        try {
            String url = API_SERVER + "/api/tti_image";

            ObjectMapper mapper = new ObjectMapper();
            Map<String, String> map = new HashMap<>();
            map.put("reqText", reqText);
            map.put("apiId", API_ID);
            map.put("apiKey", API_KEY);

            String jsonStr = mapper.writeValueAsString(map);

            System.out.println("getTtiImage json :::::: " + jsonStr);

            CloseableHttpClient client = HttpClientBuilder.create().build();

            HttpPost post = new HttpPost(url);
            post.setHeader("Content-Type", "application/json");
            StringEntity entity = new StringEntity(jsonStr);

            post.setEntity(entity);

            HttpResponse response = client.execute(post);

//			int responseCode = response.getStatusLine().getStatusCode();
            System.out.println("Sending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + post.getEntity());
            System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

            HttpEntity responseEntity = response.getEntity();

            HttpHeaders headers = new HttpHeaders();
            InputStream in = responseEntity.getContent();
            byte[] imageArray = IOUtils.toByteArray(in);

            ResponseEntity<byte[]> resultEntity = new ResponseEntity<byte[]>(imageArray, headers, HttpStatus.OK);

            return resultEntity;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
