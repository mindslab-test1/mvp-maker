package ai.maum.mvp_maker.runner.api_engine.lipsync;

import ai.maum.mvp_maker.lipsync.LipSync;
import ai.maum.mvp_maker.media.Media;
import ai.maum.mvp_maker.media.MediaUrl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Controller
@RequestMapping(value = "/mvp/runner/api")
@RequiredArgsConstructor
public class LipSync_Controller {

    private final LipSync_Service service;
    private final LipSync lipSync;
    private final Media media;


    @RequestMapping("/avatar")
    @ResponseBody
    public String getApiLipSync(@RequestParam(value = "text") String text,
                                @RequestParam(value = "model") String model,
                                @RequestParam(value = "image") MultipartFile image) {
        return service.getApiLipSync(text, model, image);
    }

    @PostMapping("/avatar-url")
    @ResponseBody
    public String getApiLipSyncUrl(@RequestParam(value = "key") String key) {
        try {
            Resource file = lipSync.runDownload(key);
            MediaUrl uploadUrl = media.getUrl(file.getFilename());
            media.uploadToMediaServer(file, uploadUrl.pathUrl);
            return media.convertDownloadUrl(uploadUrl.fullUrl);
        } catch (Exception e) {
            throw new RuntimeException("Failed to generate avatar video");
        }
    }
}