package ai.maum.mvp_maker.runner.api_engine.hotel_chatbot;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping(value = "/mvp/runner/api")
public class HotelChatbot_Controller {

    @Autowired
    private HotelChatbot_Service service;

    @RequestMapping(value = "/hotelChatbot", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String runApi_hotelChatbot(@RequestParam(value = "utter") String utter) throws Exception {
        System.out.println(" @ Hello hotelChatbot Controller ! ");
        return service.runApi_hotelChatbot(utter);
    }
}
