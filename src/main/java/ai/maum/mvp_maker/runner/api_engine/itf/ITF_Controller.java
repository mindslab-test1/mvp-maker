package ai.maum.mvp_maker.runner.api_engine.itf;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping(value = "/mvp/runner/api")
public class ITF_Controller {

    @Autowired
    private ITF_Service service;

    @RequestMapping(value = "/itf", produces = "application/text; charset=utf8")
    @ResponseBody
    public String runApi_ITF(   @RequestParam(value = "utter") String utter,
                                @RequestParam(value = "lang") String lang
    ) throws Exception {

        String result = service.runApi_ITF(utter, lang);
        return result;
    }
}
