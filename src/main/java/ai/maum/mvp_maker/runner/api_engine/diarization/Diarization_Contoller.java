package ai.maum.mvp_maker.runner.api_engine.diarization;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Controller
@RequestMapping(value="/mvp/runner/api")
public class Diarization_Contoller {

    @Autowired
    private Diarization_Service service;

    @PostMapping("/diarization")
    @ResponseBody
    public String getApiDiarize(@RequestParam("file") MultipartFile file){
        return service.getApiDiarize(file);
    }

}
