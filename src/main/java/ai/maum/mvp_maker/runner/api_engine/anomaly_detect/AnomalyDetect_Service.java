package ai.maum.mvp_maker.runner.api_engine.anomaly_detect;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

@Slf4j
@Service
public class AnomalyDetect_Service {

    final String API_SERVER = "https://api.maum.ai";

    @Value("${upload.dir}")
    String UPLOAD_DIR;

    @Value("${api.id}")
    String API_ID;

    @Value("${api.key}")
    String API_KEY;

    public String runApi_anomalyDetect(MultipartFile file) {
        log.info(" @ Hello AnomalyDetect_Service ! ");

        try {
            String url = API_SERVER + "/smartXLoad/anomalyDetect";

        HttpClient client = HttpClients.createDefault();
        HttpPost post = new HttpPost(url);

        String uploadPath = UPLOAD_DIR + "/anomalyDetect";
        File varfile = new File(uploadPath);

        if(!varfile.exists())
            varfile.mkdirs();

        varfile = new File((uploadPath + "/" + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));
        file.transferTo(varfile);
        FileBody fileBody = new FileBody(varfile);

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.addPart("apiId", new StringBody(API_ID, ContentType.MULTIPART_FORM_DATA));
        builder.addPart("apiKey", new StringBody(API_KEY, ContentType.MULTIPART_FORM_DATA));
        builder.addPart("video", fileBody);

        HttpEntity entity = builder.build();
        post.setEntity(entity);
        HttpResponse response = client.execute(post);

        int responseCode = response.getStatusLine().getStatusCode();
        String result = EntityUtils.toString(response.getEntity(), "UTF-8");

        if(responseCode == 200) {
            log.info("# EngineApiService.AnomalyDetect >> succ >> result >> {}", "너무 길어서 안 찍음");
            varfile.delete();
            return result;
        }else{
            log.info("# EngineApiService.AnomalyDetect >> fail >> code = {} >> result >> {}", responseCode, result);
            varfile.delete();
            return "{ \"status\": \"error\" }";
        }

        } catch (Exception e) {
            log.info("# EngineApiService.AnomalyDetect >> fail >> exception >> {}", e.getMessage());
            e.printStackTrace();
            return "{ \"status\": \"error\" }";
        }
    }
}
