package ai.maum.mvp_maker.runner.api_engine.denoise;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Controller
@RequestMapping(value="/mvp/runner/api")
public class Denoise_Controller {

    @Autowired
    private Denoise_Service service;

    @PostMapping("/denoise")
    @ResponseBody
    public ResponseEntity<byte[]> getApiDenoise(@RequestParam("noiseFile")MultipartFile noiseFile) {

        return service.getApiDenoise(noiseFile);
    }
}
