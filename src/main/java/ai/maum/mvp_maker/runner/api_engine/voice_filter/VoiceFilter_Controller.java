package ai.maum.mvp_maker.runner.api_engine.voice_filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Controller
@RequestMapping(value = "/mvp/runner/api")
public class VoiceFilter_Controller {

    @Autowired
    private VoiceFilter_Service service;

    @RequestMapping("/voiceFilter")
    @ResponseBody
    public ResponseEntity<byte[]> runApi_VoiceFilter(@RequestParam("mixedVoice") MultipartFile mixedVoice,
                                                     @RequestParam("reqVoice") MultipartFile reqVoice) {

        return service.runApi_VoiceFilter( mixedVoice, reqVoice );
    }
}
