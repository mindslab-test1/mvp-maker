package ai.maum.mvp_maker.runner.api_engine.chatbot;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping(value = "/mvp/runner/api")
public class Chatbot_Controller {

    @Autowired
    private Chatbot_Service chatbot_service;

    @RequestMapping(value = "/chatbot", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String runApi_chatbot(@RequestParam(value = "question") String question,
                                      @RequestParam(value = "ntop") int ntop,
                                      @RequestParam(value = "domain") String domain) throws Exception {
        System.out.println(" @ Hello chatbot Controller ! ");
        return chatbot_service.runApi_chatbot(question, ntop, domain);
    }
}
