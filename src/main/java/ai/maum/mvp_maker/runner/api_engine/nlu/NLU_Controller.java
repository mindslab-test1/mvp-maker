package ai.maum.mvp_maker.runner.api_engine.nlu;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping(value="/mvp/runner/api")
public class NLU_Controller {

    @Autowired
    private NLU_Service service;

    @RequestMapping(value="/nlu", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String getApiNlu(@RequestParam(value = "lang") String lang,
                            @RequestParam(value = "reqText") String reqText) {

        return service.getApiNlu(lang, reqText);
    }
}
