package ai.maum.mvp_maker.runner.api_engine.stt;

import ai.maum.mvp_maker.runner.model.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Slf4j
@Service
public class STT_Service {
    final String API_SERVER = "https://api.maum.ai";

    @Autowired
    RestTemplate restTemplate;

    @Value("${upload.dir}")
    String UPLOAD_DIR;

    @Value("${api.id}")
    String API_ID;

    @Value("${api.key}")
    String API_KEY;

    String getApiStt(MultipartFile file, String lang, String level, String sampling) {
        try {
            String url = API_SERVER + "/api/stt/";

            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);

            /* 음원 데이타를 Multipart로 등록하기 위한 준비 */
            String uploadPath = UPLOAD_DIR + "/stt";
            File varfile = new File(uploadPath);
            if (varfile.exists() == false) {
                varfile.mkdirs();
            }
            varfile = new File((uploadPath + "/" + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));
            file.transferTo(varfile);
            FileBody fileBody = new FileBody(varfile);

            /* Multipart 등록 */
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("file", fileBody);
            builder.addPart("lang", new StringBody(lang, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("level", new StringBody(level, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("sampling", new StringBody(sampling, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("ID", new StringBody(API_ID, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("key", new StringBody(API_KEY, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("cmd", new StringBody("runFileStt", ContentType.MULTIPART_FORM_DATA));
            org.apache.http.HttpEntity entity = builder.build();
            post.setEntity(entity);

            /* STT API 실행 */
            HttpResponse response = client.execute(post);

            int responseCode = response.getStatusLine().getStatusCode();
            /* 정상 응답 수신 */
            if (responseCode == 200) {
                String result = EntityUtils.toString(response.getEntity(), "UTF-8");
                log.info("# EngineApiService.getApiStt >> succ >> result >> {}", result);
                return result;

            }
            /* 에러 응답 수신 */
            else {
                String result = EntityUtils.toString(response.getEntity(), "UTF-8");

                log.info("# EngineApiService.getApiStt >> fail >> code = {} >> result >> {}", responseCode, result);

                System.out.println("Response Result : " + result.replace('+', ' '));
                System.out.println("API 호출 에러 발생 : 에러코드=" + responseCode);
                return "{ \"status\": \"error\" }";
            }
        } catch (Exception e) {
            log.info("# EngineApiService.getApiStt >> fail >> exception >> {}", e.getMessage());
            e.printStackTrace();
            return "{ \"status\": \"error\" }";
        }
    }

    String getApiCnnStt(String model, MultipartFile file) {
        try {
            String url = API_SERVER + "/stt/cnnSttSimple";

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);

            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
            body.set("apiId",API_ID);
            body.set("apiKey",API_KEY);
            body.set("model",model);
            body.set("file", file.getResource());
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
            ResponseEntity<CommonResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, new ParameterizedTypeReference<CommonResponse>() {});
            String data = (String) response.getBody().getResult();

            if(data != null) return data;
            return "{ \"status\": \"error\" }";
        } catch (Exception e) {
            log.info("# EngineApiService.getApiCnnStt >> fail >> exception >> {}", e.getMessage());
            e.printStackTrace();
            return "{ \"status\": \"error\" }";
        }
    }
}
