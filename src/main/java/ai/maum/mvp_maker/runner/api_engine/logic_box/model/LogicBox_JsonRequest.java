package ai.maum.mvp_maker.runner.api_engine.logic_box.model;


import lombok.Data;

import java.util.List;

@Data
public class LogicBox_JsonRequest {
    private String endpoint;
    private String reqMethod;
    private String contentType;
    private List<LogicBox_ReqParam> reqParamList;
    private List<LogicBox_ReqHeader> reqHeaderList;
    private String reqBody;
    private List<LogicBox_JsonPath> jsonPathList;

}