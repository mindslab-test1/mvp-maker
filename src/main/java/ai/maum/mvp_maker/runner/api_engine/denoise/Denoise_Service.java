package ai.maum.mvp_maker.runner.api_engine.denoise;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;

@Slf4j
@Service
public class Denoise_Service {
    final String API_SERVER = "https://api.maum.ai";

    @Value("${upload.dir}")
    String UPLOAD_DIR;

    @Value("${api.id}")
    String API_ID;

    @Value("${api.key}")
    String API_KEY;

    ResponseEntity<byte[]> getApiDenoise(MultipartFile noiseFile) {

        log.info("[Engine Denoise]");

        if (noiseFile == null) {
            throw new RuntimeException("You must select the a file for uploading");
        }

        try {
            String url = API_SERVER + "/api/dap/denoise";
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            File noiseVarFile = new File(UPLOAD_DIR);


            if (!noiseVarFile.exists()) {
                noiseVarFile.mkdirs();
            }
            noiseVarFile = new File((UPLOAD_DIR +"/"+ noiseFile.getOriginalFilename().substring(noiseFile.getOriginalFilename().lastIndexOf("\\") + 1)));
            noiseFile.transferTo(noiseVarFile);

            FileBody noiseFileBody = new FileBody(noiseVarFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();

            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

            builder.addPart("file", noiseFileBody);
            builder.addPart("apiId", new StringBody(API_ID, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody(API_KEY, ContentType.MULTIPART_FORM_DATA));

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);

            int responseCode = response.getStatusLine().getStatusCode();
            log.info("------" + responseCode);
            if (responseCode == 200) {
                HttpEntity responseEntity = response.getEntity();

                HttpHeaders headers = new HttpHeaders();
                InputStream in = responseEntity.getContent();
                byte[] audioArray = IOUtils.toByteArray(in);

                headers.setCacheControl(CacheControl.noCache().getHeaderValue());

                ResponseEntity<byte[]> resultEntity = new ResponseEntity<>(audioArray,headers, HttpStatus.OK);
                noiseVarFile.delete();

                log.info("------" + responseCode);
                return resultEntity;

            }else{
                log.error("Denoise Server error : " + responseCode + ", " + response.getEntity());
                return null;
            }

        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }
}
