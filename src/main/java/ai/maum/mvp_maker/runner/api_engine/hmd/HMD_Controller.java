package ai.maum.mvp_maker.runner.api_engine.hmd;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping(value = "/mvp/runner/api")
public class HMD_Controller {

    @Autowired
    private HMD_Service service;

    @RequestMapping(value = "/hmd", produces = "application/text; charset=utf8")
    @ResponseBody
    public String runApi_HMD(   @RequestParam(value = "lang") String lang,
                                @RequestParam(value = "reqText") String reqText
    ) throws Exception {

        String result = service.runApi_HMD(lang, reqText);
        return result;
    }
}
