package ai.maum.mvp_maker.runner.api_engine.engEdu;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@Slf4j
@Service
public class EngEdu_Service {

    final String EngEdu_API_SERVER = "https://maieng.maum.ai:7777";
    final String EngEdu_STT = "/engedu/v1/stt";
    final String EngEdu_PRON = "/engedu/v1/pron";
    final String EngEdu_PHONICS = "/engedu/v1/phonics";

    @Value("${api.id}")
    String API_ID;

    @Value("${api.key}")
    String API_KEY;

    @Value("${upload.dir}")
    String UPLOAD_DIR;

    String engEdu(MultipartFile file, String userId, String model, String answerText, String apiName){

        String url = EngEdu_API_SERVER;
        switch (apiName){
            case "stt":
                url += EngEdu_STT; break;
            case "pron":
                url += EngEdu_PRON; break;
            case "phonics":
                url += EngEdu_PHONICS; break;
            default:
                log.info("apiName이 일치하지 않습니다.");
        }

        try {

            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);

            /* 음원 데이타를 Multipart로 등록하기 위한 준비 */
            String uploadPath = UPLOAD_DIR + "/" + apiName;
            File varfile = new File(uploadPath);

            if(!varfile.exists()) {
                varfile.mkdirs();
            }

            varfile = new File((uploadPath +"/"+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));
            file.transferTo(varfile);
            FileBody fileBody = new FileBody(varfile);

            /* Multipart 등록 */
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(API_ID, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody(API_KEY, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("userId", new StringBody(userId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("model", new StringBody(model, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("answerText", new StringBody(answerText, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("file", fileBody);
            HttpEntity entity = builder.build();
            post.setEntity(entity);

            /* API 실행 */
            HttpResponse response = client.execute(post);

            int responseCode = response.getStatusLine().getStatusCode();

            String result = EntityUtils.toString(response.getEntity(), "UTF-8");

            /* 정상 응답 수신 */
            if (responseCode == 200) {
                log.info("# EngineApiService.{} >> succ >> result >> {}", apiName, result);
                return result;
            }
            /* 에러 응답 수신 */
            else {
                log.info("# EngineApiService.{} >> fail >> code = {} >> result >> {}", apiName, responseCode, result);
                return "{ \"status\": \"error\" }";
            }

        } catch (Exception e) {
            log.info("# EngineApiService.{} >> fail >> exception >> {}", apiName, e.getMessage());
            e.printStackTrace();
            return "{ \"status\": \"error\" }";
        }
    }

}
