package ai.maum.mvp_maker.runner.api_engine.poseExtract;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
@Slf4j
@Service
public class PoseExtract_Service {

    final String API_SERVER = "https://api.maum.ai";

    @Value("${upload.dir}")
    String UPLOAD_DIR;

    @Value("${api.id}")
    String API_ID;

    @Value("${api.key}")
    String API_KEY;

    public ResponseEntity<byte[]> runApi_PoseExtract(MultipartFile file) {
        System.out.println(" @ Hello PoseExtract Service ! ");

        try {
            String url = "/smartXLoad/poseExtract/";

            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(API_SERVER + url);

            /* 이미지 데이터를 Multipart로 등록하기 위한 준비 */
            String uploadPath = UPLOAD_DIR + "/poseExtract";
            File varfile = new File(uploadPath);
            if (varfile.exists() == false) {
                varfile.mkdirs();
            }
            varfile = new File((uploadPath + "/" + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));
            file.transferTo(varfile);
            FileBody fileBody = new FileBody(varfile);

            /* Multipart 등록 */
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(API_ID, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody(API_KEY, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("pose_img", fileBody);
            HttpEntity entity = builder.build();
            post.setEntity(entity);

            /* API 실행 */
            HttpResponse response = client.execute(post);

            int responseCode = response.getStatusLine().getStatusCode();
            /* 정상 응답 수신 */
            if (responseCode == 200) {

                log.info("# EngineApiService.runApi_PoseExtract >> success >> code = {}", responseCode);

                HttpEntity responseEntity = response.getEntity();

                HttpHeaders headers = new HttpHeaders();
                InputStream in = responseEntity.getContent();
                byte[] imageArray = IOUtils.toByteArray(in);

                ResponseEntity<byte[]> resultEntity = new ResponseEntity<byte[]>(imageArray, headers, HttpStatus.OK);

                return resultEntity;
            }
            /* 에러 응답 수신 */
            else {
                String result = EntityUtils.toString(response.getEntity(), "UTF-8");

                log.info("# EngineApiService.runApi_PoseExtract >> fail >> code = {} >> result >> {}", responseCode, result);

                System.out.println("Response Result : " + result.replace('+', ' '));
                System.out.println("API 호출 에러 발생 : 에러코드=" + responseCode);
                return null;
            }
        } catch (IOException e) {
            log.info("# EngineApiService.runApi_PoseExtract >> fail >> exception >> {}", e.getMessage());
            e.printStackTrace();
            return null;
        }
    }
}
