package ai.maum.mvp_maker.runner.api_engine.logic_box.model;


import lombok.*;

@Data
public class LogicBox_JsonPath {
    private String jsonPath;
    private String mappingParam;
}