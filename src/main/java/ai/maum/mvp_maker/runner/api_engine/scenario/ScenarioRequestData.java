package ai.maum.mvp_maker.runner.api_engine.scenario;

import lombok.Data;

@Data
public class ScenarioRequestData {
    private String host;
    private String session;
    private SdsData data;
    private String lang;
    
    public ScenarioRequestData(String host, String session, String utter, String lang) {
        this.host = host;
        this.session = session;
        this.data = new SdsData(utter);
        this.lang = lang;
    }
}

@Data
class SdsData {
    private String utter;
    
    public SdsData(String utter) {
        this.utter = utter;
    }
}
