package ai.maum.mvp_maker.runner.api_engine.hotel_chatbot;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class HotelChatbot_Service {

    @Value("${api.id}")
    String API_ID;

    @Value("${api.key}")
    String API_KEY;

    String runApi_hotelChatbot( String utter ) throws Exception {
        String url = "http://13.125.179.153:6941/collect/run/utter";
        //String url = "http://182.162.19.19:6961/collect/run/utter"; //test server

        System.out.println(utter);
        String jsonStr = "{" + "'utter' : " + utter + "}";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("host", "2");
        paramMap.put("session", "mvp_" + API_ID);
        paramMap.put("data", jsonStr);
        paramMap.put("lang", "1");

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(paramMap);
        String resultMsg = sendPost(url, json);

        return resultMsg;
    }

    private String sendPost(String sendUrl, String jsonValue) throws Exception{
        StringBuilder result = new StringBuilder();
        URL url = new URL(sendUrl);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Accept-Charset", "UTF-8");
        conn.setConnectTimeout(10000);
        conn.setReadTimeout(60000);

        OutputStream os = conn.getOutputStream();
        os.write(jsonValue.getBytes("UTF-8"));
        os.flush();

        int nResponseCode = conn.getResponseCode();

        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"), 8);
        String inputLine = "";

        while((inputLine = br.readLine()) != null) {
            inputLine = inputLine.replace("\\n", "<br>");
            try {
                result.append(URLDecoder.decode(inputLine, "UTF-8"));
            } catch(Exception ex) {
                result.append(inputLine);
            }
        }
        if(nResponseCode == HttpURLConnection.HTTP_OK) {
            System.out.println(result.toString());
        } else {
            System.out.println("Response Result : " + result.toString().replace('+',' '));
            System.out.println("API 호출 에러 발생 : 에러코드=" + nResponseCode);
            throw new Exception("api error");
        }
        br.close();
        conn.disconnect();
        return result.toString();
    }
}
