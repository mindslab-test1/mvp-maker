package ai.maum.mvp_maker.runner.api_engine.subtitle_recog;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Controller
@RequestMapping(value="/mvp/runner/api")
public class SubtitleRecog_Contoller {

    @Autowired
    private SubtitleRecog_Service service;

    @PostMapping(value="/subtitRecog", produces = "application/json; charset=utf8")
    @ResponseBody
    public String getApiSubtitRecog(@RequestParam("file") MultipartFile file) {

        return service.getApiSubtitRecog(file);
    }
}
