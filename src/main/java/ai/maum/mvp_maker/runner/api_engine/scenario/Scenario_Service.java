package ai.maum.mvp_maker.runner.api_engine.scenario;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class Scenario_Service {

    @Value("${api.scenario.url}")
    private String scenarioUrl;
    
    @Value("${api.scenario.session}")
    private String scenarioSession;
    
    @Value("${api.scenario.lang}")
    private String scenarioLang;
    
    private final RestTemplate restTemplate;
    
    public Scenario_Service(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
    
    public String getApiScenario (String utter, String scenarioHost) {
        ScenarioRequestData requestData = new ScenarioRequestData(scenarioHost, scenarioSession, utter, scenarioLang);
        ScenarioResponseData responseData = restTemplate.postForObject(scenarioUrl, requestData, ScenarioResponseData.class);
        return String.valueOf(responseData.getAnswer().get("answer")).replace("<br>", "");
    }
}
