package ai.maum.mvp_maker.runner.api_engine.gpt;

import ai.maum.mvp_maker.runner.service.EngineApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Controller
@RequestMapping(value="/mvp/runner/api")
public class GPT_Controller {

    @Autowired
    private GPT_Service service;

    @RequestMapping(value="/gpt", produces = "application/text; charset=utf8")
    @ResponseBody
    public String runApi_GPT(   @RequestParam(value = "context") String context,
                                @RequestParam(value = "lang") String lang
    ) throws Exception {
        String result = service.runApi_GPT(context, lang);
        return result;
    }

}
