package ai.maum.mvp_maker.runner.api_engine.voice_recog;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;


@Slf4j
@Service
public class VoiceRecog_Service {

    final String API_SERVER = "https://api.maum.ai";

    @Value("${api.id}")
    String API_ID;

    @Value("${api.key}")
    String API_KEY;

    @Value("${upload.dir}")
    String UPLOAD_DIR;

    String getVoice(String dbId) {
        try {
            String url = API_SERVER + "/dap/app/getVoiceList";

            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);

            /* Multipart 등록 */
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(API_ID, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody(API_KEY, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("dbId", new StringBody(dbId,ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            HttpEntity entity = builder.build();
            post.setEntity(entity);

            /* getVoice API 실행 */
            HttpResponse response = client.execute(post);

            int responseCode = response.getStatusLine().getStatusCode();

            /* 정상 응답 수신 */
            if (responseCode == 200) {
                String result = EntityUtils.toString(response.getEntity(), "UTF-8");
                log.info("# EngineApiService.getVoice >> succ >> result >> {}", result);
                return result;
            }
            /* 에러 응답 수신 */
            else {
                String result = EntityUtils.toString(response.getEntity(), "UTF-8");

                log.info("# EngineApiService.getVoice >> fail >> code = {} >> result >> {}", responseCode, result);

                System.out.println("Response Result : " + result.replace('+', ' '));
                System.out.println("API 호출 에러 발생 : 에러코드=" + responseCode);
                return "{ \"status\": \"error\" }";
            }

        } catch (Exception e) {
            log.info("# EngineApiService.getVoice >> fail >> exception >> {}", e.getMessage());
            e.printStackTrace();
            return "{ \"status\": \"error\" }";
        }
    }



    String setVoice(MultipartFile file, String voiceId, String dbId){
        try {
            String url = API_SERVER + "/dap/app/setVoice";

            HttpClient client = HttpClients.createDefault();
            HttpPut put = new HttpPut(url);

            /* 음원 데이타를 Multipart로 등록하기 위한 준비 */
            String uploadPath = UPLOAD_DIR + "/voiceRecog";
            File varfile = new File(uploadPath);

            if(!varfile.exists()) {
                varfile.mkdirs();
            }

            varfile = new File((uploadPath +"/"+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));
            file.transferTo(varfile);
            FileBody fileBody = new FileBody(varfile);

            /* Multipart 등록 */
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(API_ID, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody(API_KEY, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("dbId", new StringBody(dbId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("voiceId", new StringBody(voiceId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("file", fileBody);
            HttpEntity entity = builder.build();
            put.setEntity(entity);

            /* setVoice API 실행 */
            HttpResponse response = client.execute(put);

            int responseCode = response.getStatusLine().getStatusCode();

            /* 정상 응답 수신 */
            if (responseCode == 200) {
                String result = EntityUtils.toString(response.getEntity(), "UTF-8");
                log.info("# EngineApiService.setVoice >> succ >> result >> {}", result);
                return result;
            }
            /* 에러 응답 수신 */
            else {
                String result = EntityUtils.toString(response.getEntity(), "UTF-8");

                log.info("# EngineApiService.setVoice >> fail >> code = {} >> result >> {}", responseCode, result);

                System.out.println("Response Result : " + result.replace('+', ' '));
                System.out.println("API 호출 에러 발생 : 에러코드=" + responseCode);
                return "{ \"status\": \"error\" }";
            }

        } catch (Exception e) {
            log.info("# EngineApiService.setVoice >> fail >> exception >> {}", e.getMessage());
            e.printStackTrace();
            return "{ \"status\": \"error\" }";
        }
    }


    String deleteVoice(String voiceId, String dbId){

        String apiUrl = API_SERVER + "/dap/app/deleteVoice";
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "3FS4PKI7YH9S";

        try{
            URL connectURL = new URL(apiUrl);
            HttpURLConnection conn = (HttpURLConnection) connectURL.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("DELETE");
            conn.setRequestProperty("Connection", "Keep-Alive");    // connection을 위함
            conn.setRequestProperty("Content-type", "multipart/form-data;boundary=" + boundary);
            conn.setConnectTimeout(15000);

            DataOutputStream dos = new DataOutputStream(conn.getOutputStream());

            StringBuffer sb = new StringBuffer();
            sb.append(twoHyphens+boundary+lineEnd);
            sb.append("Content-Disposition: form-data; name=\"apiId\"" + lineEnd);

            sb.append(lineEnd);
            sb.append(API_ID+lineEnd);

            sb.append(twoHyphens+boundary+lineEnd);
            sb.append("Content-Disposition: form-data; name=\"apiKey\"" + lineEnd);
            sb.append(lineEnd);
            sb.append(API_KEY+lineEnd);

            sb.append(twoHyphens+boundary+lineEnd);
            sb.append("Content-Disposition: form-data; name=\"voiceId\"" + lineEnd);
            sb.append(lineEnd);
            sb.append(voiceId+lineEnd);

            sb.append(twoHyphens+boundary+lineEnd);
            sb.append("Content-Disposition: form-data; name=\"dbId\"" + lineEnd);
            sb.append(lineEnd);
            sb.append(dbId+lineEnd);

            sb.append(twoHyphens+boundary+twoHyphens+lineEnd);

            dos.writeUTF(sb.toString());
            dos.flush();

            int responseCode = conn.getResponseCode();
            StringBuffer outResult = new StringBuffer();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"), 8);
            String inputLine;
            while((inputLine = br.readLine()) != null){
                try{
                    outResult.append(URLDecoder.decode(inputLine, "UTF-8"));
                }catch(Exception e){
                    outResult.append(inputLine);
                }
            }
            if (responseCode == 200) {

                log.info("# EngineApiService.deleteVoice >> succ >> result >> {}", outResult);
                br.close();
                return outResult.toString();
            }
            /* 에러 응답 수신 */
            else {
                String result = outResult.toString();

                log.info("# EngineApiService.deleteVoice >> fail >> code = {} >> result >> {}", responseCode, result);
                return "{ \"status\": \"error\" }";
            }
        } catch (Exception e) {
            log.info("# EngineApiService.deleteVoice >> fail >> exception >> {}", e.getMessage());
            e.printStackTrace();
            return "{ \"status\": \"error\" }";
        }
    }


    String recogVoice(MultipartFile file, String dbId){
        try {
            String url = API_SERVER + "/dap/app/recogVoice";

            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);

            /* 음원 데이타를 Multipart로 등록하기 위한 준비 */
            String uploadPath = UPLOAD_DIR + "/voiceRecog";
            File varfile = new File(uploadPath);

            if(!varfile.exists()) {
                varfile.mkdirs();
            }

            varfile = new File((uploadPath +"/"+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));
            file.transferTo(varfile);
            FileBody fileBody = new FileBody(varfile);

            /* Multipart 등록 */
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(API_ID, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody(API_KEY, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("dbId", new StringBody(dbId,ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("file", fileBody);
            HttpEntity entity = builder.build();
            post.setEntity(entity);

            /* getVoice API 실행 */
            HttpResponse response = client.execute(post);

            int responseCode = response.getStatusLine().getStatusCode();

            /* 정상 응답 수신 */
            if (responseCode == 200) {
                String result = EntityUtils.toString(response.getEntity(), "UTF-8");
                log.info("# EngineApiService.recogVoice >> succ >> result >> {}", result);
                return result;
            }
            /* 에러 응답 수신 */
            else {
                String result = EntityUtils.toString(response.getEntity(), "UTF-8");

                log.info("# EngineApiService.recogVoice >> fail >> code = {} >> result >> {}", responseCode, result);

                System.out.println("Response Result : " + result.replace('+', ' '));
                System.out.println("API 호출 에러 발생 : 에러코드=" + responseCode);
                return "{ \"status\": \"error\" }";
            }

        } catch (Exception e) {
            log.info("# EngineApiService.recogVoice >> fail >> exception >> {}", e.getMessage());
            e.printStackTrace();
            return "{ \"status\": \"error\" }";
        }
    }
}
