package ai.maum.mvp_maker.runner.api_engine.voice_recog;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller
@RequestMapping(value="/mvp/runner/api")
public class VoiceRecog_Controller {

    @Autowired
    private VoiceRecog_Service service;

    @RequestMapping(value = "/voiceRecog/get", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String getVoice(@RequestParam(value = "dbId") String dbId) {
        return service.getVoice(dbId);
    }


    @RequestMapping("/voiceRecog/set")
    @ResponseBody
    public String setVoice(@RequestParam("file") MultipartFile file, @RequestParam(value = "voiceId") String voiceId, @RequestParam(value = "dbId") String dbId) {
        return service.setVoice(file, voiceId, dbId);
    }


    @RequestMapping("/voiceRecog/delete")
    @ResponseBody
    public String deleteVoice(@RequestParam(value = "voiceId") String voiceId, @RequestParam(value = "dbId") String dbId) {
        return service.deleteVoice(voiceId, dbId);
    }


    @RequestMapping(value = "/voiceRecog/recog", produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String recogVoice(@RequestParam("file") MultipartFile file, @RequestParam(value = "dbId") String dbId) {
        return service.recogVoice(file, dbId);
    }
}
