package ai.maum.mvp_maker.runner.api_engine.logic_box;

import ai.maum.mvp_maker.runner.api_engine.logic_box.model.LogicBox_JsonRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping(value="mvp/runner/api")
public class LogicBox_Controller {
    @Autowired
    private LogicBox_Service service;

    @RequestMapping(value = "/logicBox", produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String getApiLogicBox(@RequestBody LogicBox_JsonRequest reqBody) throws Exception {
        log.info("LogicBox.getApiLogicBox>> {}", reqBody.toString());
        return service.sendRequest(reqBody);
    }



}