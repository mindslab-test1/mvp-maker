package ai.maum.mvp_maker.runner.api_engine.scenario;

import groovy.util.logging.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping(value="/mvp/runner/api")
public class Scenario_Controller {
    @Autowired
    private Scenario_Service service;
    
    @RequestMapping(value="/scenario", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String getApiScenario(@RequestParam(value = "utter") String utter, @RequestParam(value = "scenarioHost", required = true) String scenarioHost) {
        return service.getApiScenario(utter, scenarioHost);
    }
}
