package ai.maum.mvp_maker.runner.api_engine.logic_box.model;

import lombok.Data;

@Data
public class LogicBox_ReqHeader {
    private String key;
    private String val;
    private String desc;
}
