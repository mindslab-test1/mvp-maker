package ai.maum.mvp_maker.runner.api_engine.mrc;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping(value = "/mvp/runner/api")
public class MRC_Controller {

    @Autowired
    private MRC_Service service;

    @RequestMapping(value = "/mrc", produces = "application/text; charset=utf8")
    @ResponseBody
    public String runApi_MRC(@RequestParam(value = "lang") String lang,
                             @RequestParam(value = "context") String context,
                             @RequestParam(value = "question") String question
    ) throws Exception {

        return service.runApi_MRC(lang, context, question);
    }
}
