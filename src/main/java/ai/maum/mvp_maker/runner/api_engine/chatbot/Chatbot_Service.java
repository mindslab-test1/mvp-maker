package ai.maum.mvp_maker.runner.api_engine.chatbot;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class Chatbot_Service {

    @Value("${url.api}")
    String API_SERVER;

    @Value("${api.id}")
    String API_ID;

    @Value("${api.key}")
    String API_KEY;

    String runApi_chatbot(String question, int ntop, String domain){

        String url = API_SERVER + "/qa/simpleQa";
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = new HashMap<>();

        map.put("question", question);
        map.put("ntop", ntop);
        map.put("domain", domain);
        map.put("apiId", API_ID);
        map.put("apiKey", API_KEY);

        try{
            String jsonStr = mapper.writeValueAsString(map);

            CloseableHttpClient client = HttpClientBuilder.create().build();

            HttpPost post = new HttpPost(url);
            post.setHeader("Content-Type", "application/json");
            StringEntity entity = new StringEntity(jsonStr,"UTF-8");

            post.setEntity(entity);

            HttpResponse response = client.execute(post);

            int responseCode = response.getStatusLine().getStatusCode();
            String resData = EntityUtils.toString(response.getEntity(), "UTF-8");

            /* 정상 응답 수신 */
            if (responseCode == 200) {
                log.info("# EngineApiService.runApi_chatbot >> succ >> result >> {}", resData);
                return resData;
            }
            /* 에러 응답 수신 */
            else {
                log.info("# EngineApiService.runApi_chatbot >> fail >> code = {} >> result >> {}", responseCode, resData);
                return "{ \"status\": \"error\" }";
            }

        } catch (Exception e) {
            log.info("# EngineApiService.runApi_chatbot >> fail >> exception >> {}", e.getMessage());
            e.printStackTrace();
        }

        return null;
    }
}
