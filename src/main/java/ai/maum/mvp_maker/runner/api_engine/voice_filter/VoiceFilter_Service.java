package ai.maum.mvp_maker.runner.api_engine.voice_filter;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;

@Slf4j
@Service
public class VoiceFilter_Service {
    final String API_SERVER = "https://api.maum.ai";

    @Value("${upload.dir}")
    String UPLOAD_DIR;

    @Value("${api.id}")
    String API_ID;

    @Value("${api.key}")
    String API_KEY;

    ResponseEntity<byte[]> runApi_VoiceFilter(MultipartFile mixedVoice, MultipartFile reqVoice) {
        log.info(" @ Hello Voice Filter Service ! ");

        if (mixedVoice == null || reqVoice == null)
            throw new RuntimeException("You must select files for uploading");

        try {
            String url = API_SERVER + "/api/dap/voiceFilter/";
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);

            File mixVarFile = new File(UPLOAD_DIR);
            File reqVarFile = new File(UPLOAD_DIR);
            if (!mixVarFile.exists()) {
                mixVarFile.mkdirs();
            }
            if (!reqVarFile.exists()) {
                reqVarFile.mkdirs();
            }

            mixVarFile = new File((UPLOAD_DIR + "/" + mixedVoice.getOriginalFilename().substring(mixedVoice.getOriginalFilename().lastIndexOf("\\") + 1)));
            mixedVoice.transferTo(mixVarFile);
            reqVarFile = new File((UPLOAD_DIR + "/" + reqVoice.getOriginalFilename().substring(reqVoice.getOriginalFilename().lastIndexOf("\\") + 1)));
            reqVoice.transferTo(reqVarFile);

            FileBody mixedFileBody = new FileBody(mixVarFile);
            FileBody reqFileBody = new FileBody(reqVarFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(API_ID, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody(API_KEY, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("mixedVoice", mixedFileBody);
            builder.addPart("reqVoice", reqFileBody);

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);

            int responseCode = response.getStatusLine().getStatusCode();
            System.out.println("------ " + responseCode);
            if (responseCode == 200) {
                HttpEntity responseEntity = response.getEntity();

                HttpHeaders headers = new HttpHeaders();
                InputStream in = responseEntity.getContent();
                byte[] audioArray = IOUtils.toByteArray(in);

                headers.setCacheControl(CacheControl.noCache().getHeaderValue());

                ResponseEntity<byte[]> resultEntirty = new ResponseEntity<>(audioArray, headers, HttpStatus.OK);
                mixVarFile.delete();
                reqVarFile.delete();

                return resultEntirty;
            } else {
                log.error("VoiceFilter Server Error : " + responseCode + ", " + response.getEntity());
                System.out.println("API 호출 에러 발생 : Error code = " + responseCode);
                return null;
            }

        } catch (Exception e) {
            log.info("# EngineApiService.setVoice >> fail >> exception >> {}", e.getMessage());
            e.printStackTrace();
            return null;
        }
    }
}