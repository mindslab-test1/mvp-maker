package ai.maum.mvp_maker.runner.api_engine.engEdu;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Controller
@RequestMapping(value="/mvp/runner/api")
public class EngEdu_Controller {

    @Autowired
    private EngEdu_Service service;

    @RequestMapping("/engedu/stt")
    @ResponseBody
    public String engEduStt(@RequestParam("file") MultipartFile file,
                            @RequestParam(value = "userId") String userId,
                            @RequestParam(value = "model") String model,
                            @RequestParam(value = "answerText") String answerText) {


        return service.engEdu(file, userId, model, answerText, "stt");
    }

    @RequestMapping("/engedu/pron")
    @ResponseBody
    public String engEduPron(@RequestParam("file") MultipartFile file,
                            @RequestParam(value = "userId") String userId,
                            @RequestParam(value = "model") String model,
                            @RequestParam(value = "answerText") String answerText) {
        return service.engEdu(file, userId, model, answerText, "pron");
    }

    @RequestMapping("/engedu/phonics")
    @ResponseBody
    public String engEduPhonics(@RequestParam("file") MultipartFile file,
                                  @RequestParam(value = "userId") String userId,
                                  @RequestParam(value = "model") String model,
                                  @RequestParam(value = "answerText") String answerText) {
        log.debug("@ /engedu/phonics : userId={} / model={} / answerText={}", userId, model, answerText);


        return service.engEdu(file, userId, model, answerText, "phonics");
    }

}
