package ai.maum.mvp_maker.runner.api_engine.textRemoval;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Controller
@RequestMapping(value = "/mvp/runner/api")
public class TextRemoval_Controller {

    @Autowired
    private TextRemoval_Service service;

    @RequestMapping("/textRemoval")
    @ResponseBody
    public ResponseEntity<byte[]> runApi_TextRemoval(@RequestParam(value = "file") MultipartFile file) {
        System.out.println(" @ Hello TextRemoval Controller ! ");

        return service.runApi_TextRemoval(file);
    }
}
