package ai.maum.mvp_maker.model;

import lombok.Data;

@Data
public class LicenseResponseDTO {
    private int code;
    private String msg;
}
