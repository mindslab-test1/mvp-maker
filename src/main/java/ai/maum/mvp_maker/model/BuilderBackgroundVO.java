package ai.maum.mvp_maker.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BuilderBackgroundVO {
    private Integer id;
    private String url;
    private String displayName;
    private String displayNameEn;
    private String waterMarkUrl;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
}
