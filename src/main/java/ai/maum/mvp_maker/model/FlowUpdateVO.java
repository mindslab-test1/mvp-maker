package ai.maum.mvp_maker.model;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class FlowUpdateVO {
    private List<Integer> idList;
    private List<String> titleList;
}
