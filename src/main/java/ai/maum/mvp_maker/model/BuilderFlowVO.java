package ai.maum.mvp_maker.model;

import lombok.Data;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class BuilderFlowVO {
    private int userNo;
    private int flowId; // flowId
    private int templateId;
    @Size(min=1, max = 25, message = "The project name must be 25 characters or less.")
    private String projectName;
    private List<BuilderScriptVO> scriptList;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
}
