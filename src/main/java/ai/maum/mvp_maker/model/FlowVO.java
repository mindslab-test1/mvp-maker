package ai.maum.mvp_maker.model;

import ai.maum.mvp_maker.common.utils.GetMediaUrl;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Data
public class FlowVO {
    private int flowId;
    private int userNo;
    @Size(min=1, max = 25, message = "The project name must be 25 characters or less.")
    private String name;
    private String description;
    private Date createDate;
    private int createUser;
    private Date updateDate;
    private int updateUser;
    private char delYn;
    private int displayOrder;
    private long templateId;
    private List<FlowNodeVO> nodeList;

    private String thumbnailImage;
    private String title;
    private String type;

    public String getThumbnailImage() {
        return GetMediaUrl.imageUrl +  thumbnailImage;
    }

}
