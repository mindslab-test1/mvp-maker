package ai.maum.mvp_maker.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BaseResponse {
    private String code;
    private String message;
    private Object payload;
}
