package ai.maum.mvp_maker.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BuilderUserBackgroundVO {
    private Integer id;
    private String url;
    private String waterMarkUrl;
    private Integer userNo;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
}
