package ai.maum.mvp_maker.model;

import lombok.Data;

@Data
public class FlowParamVO {
    private Integer engineId;
    private String name;
    private String value;
}
