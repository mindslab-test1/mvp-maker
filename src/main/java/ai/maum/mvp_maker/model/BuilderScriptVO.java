package ai.maum.mvp_maker.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BuilderScriptVO {
    private Integer id;
    private Integer flowId;
    private Integer avatarId;
    private String backgroundUrl;
    private String text;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
}
