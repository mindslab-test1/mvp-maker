package ai.maum.mvp_maker.model;

import lombok.Data;

@Data
public class FlowParamReqVo {
    private Integer flowId;
}
