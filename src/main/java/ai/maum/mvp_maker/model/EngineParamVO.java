package ai.maum.mvp_maker.model;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Data
public class EngineParamVO {
    private int id;
    private int engineId;
    private String name;
    private String displayName;
    private String displayNameEn;
    private String type;
    private String enumValue;
    private String displayEnumValue;
    private String displayEnumValueEn;
    private String defaultValue;
    private Object data;
    private int display;
}
