package ai.maum.mvp_maker.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BuilderAvatarVO {
    private int id;
    private String thumbnailUrl;
    private String previewUrl;
    private String displayName;
    private String displayNameEn;
    private String model;
    private String productCode;
    private int ord;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
}
