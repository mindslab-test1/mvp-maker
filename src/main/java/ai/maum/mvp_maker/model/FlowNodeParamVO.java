package ai.maum.mvp_maker.model;

import lombok.Data;

import java.util.List;

@Data
public class FlowNodeParamVO {
    private int id;
    private int flowNodeId;
    private String name;
    private String value;
}
