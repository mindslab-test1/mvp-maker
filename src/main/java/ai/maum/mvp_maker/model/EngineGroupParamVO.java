package ai.maum.mvp_maker.model;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Data
public class EngineGroupParamVO {
    private int groupParamId;
    private int engineId;
    private String name;
    private String params;

    private List<EngineGroupParamValueVO> valueList;
}
