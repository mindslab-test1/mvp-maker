package ai.maum.mvp_maker.model;

import ai.maum.mvp_maker.common.utils.GetMediaUrl;
import lombok.Data;

import java.util.List;

@Data
public class BuilderTemplateVO {

    private long id;
    private String type;
    private String title;
    private String titleEN;
    private String description;
    private String descriptionEN;
    private String thumbnailImage;
    private List<String> descriptionList;

    public String getThumbnailImage() {
        return GetMediaUrl.imageUrl +  thumbnailImage;
    }

}
