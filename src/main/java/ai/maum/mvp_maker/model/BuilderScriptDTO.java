package ai.maum.mvp_maker.model;

import lombok.Data;

/**
 *  play Script DTO
 */

@Data
public class BuilderScriptDTO {
    private String model;
    private String text;
    private String backgroundUrl;
    private String behavior;
    private String template;
}
