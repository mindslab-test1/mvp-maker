package ai.maum.mvp_maker.model;

import lombok.Data;

/**
 * Created by kirk@mindslab.ai on 2020-07-23
 */
@Data
public class EngineModelRequestVO {
    private int id;
    private int userNo;
    private String name;
    private String phone;
    private String email;
    private String engine;
    private int flowId;
    private String title;
    private String content;
    private String state;
}
