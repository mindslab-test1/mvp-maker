package ai.maum.mvp_maker.model;

import lombok.Data;

import java.util.List;

@Data
public class FlowNodeVO {
    private int flowNodeId;
    private int flowId;
    private int engineId;
    private String engineName;
    private String inputMethod;
    private List<FlowNodeParamVO> paramList;
}
