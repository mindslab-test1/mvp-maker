package ai.maum.mvp_maker.model;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;

@Data
public class EngineInputVO {
    private int id;
    private int engineId;
    private String name;
    private String type;
    private String format;
}
