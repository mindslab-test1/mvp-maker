package ai.maum.mvp_maker.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ShopifyLicenseUsageLogDTO {
    private String email;
    private int templateId;
    private int modelId;
    private int usage;
    private LocalDateTime usageDate;
}
