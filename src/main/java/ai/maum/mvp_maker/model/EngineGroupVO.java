package ai.maum.mvp_maker.model;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;

@Data
public class EngineGroupVO {
    private int groupId;
    private String name;
    private String nameEn;
    private String color;
    private String maxCols;
}
