package ai.maum.mvp_maker.model;

import lombok.Data;

@Data
public class UserLicenseDTO {
    private Integer userNo;
    private String email;
    private Integer avatarId;
    private String licenseKey;
}
