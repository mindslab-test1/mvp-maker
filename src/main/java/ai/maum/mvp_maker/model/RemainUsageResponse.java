package ai.maum.mvp_maker.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RemainUsageResponse {
    private Integer remain_usage;
}
