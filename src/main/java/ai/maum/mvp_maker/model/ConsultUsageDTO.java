package ai.maum.mvp_maker.model;

import lombok.Data;

@Data
public class ConsultUsageDTO {
    private Integer usageLimit;
    private Integer usage;
}
