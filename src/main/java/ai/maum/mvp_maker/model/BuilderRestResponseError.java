package ai.maum.mvp_maker.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class BuilderRestResponseError {
    private String code;
    private String message;
}
