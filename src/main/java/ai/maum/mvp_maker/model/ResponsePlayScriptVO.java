package ai.maum.mvp_maker.model;

import lombok.Data;

@Data
public class ResponsePlayScriptVO {
    private String remainedUsage;
    private String downloadUrl;
}
