package ai.maum.mvp_maker.model;

import lombok.Data;

@Data
public class PurchasedVO {
    String model;
    String licenseKey;
    String purchased;
}
