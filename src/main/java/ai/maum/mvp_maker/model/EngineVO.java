package ai.maum.mvp_maker.model;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Data
public class EngineVO {
    private int engineId;
    private int groupId;
    private String name;
    private String nameEn;
    private String desc;
    private String descEn;
    private String apiUrl;
    private String apiMethod;
    private String requestType;
    private String responseType;
    private String iconPath;
    private String inputType;
    private String outputType;
    private String docUrl;
    private String docUrl_en;
    private int active;
    private int groupId2;
    private int depth;

    private List<EngineInputVO> inputList;
    private List<EngineParamVO> paramList;
    private List<EngineGroupParamVO> groupParamList;
}
