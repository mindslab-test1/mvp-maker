package ai.maum.mvp_maker.model;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;

@Data
public class EngineGroupParamValueVO {
    private int groupParamId;
    private String name;
    private String nameEn;
    private String data;
}
