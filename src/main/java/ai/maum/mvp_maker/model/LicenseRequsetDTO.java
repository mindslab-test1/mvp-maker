package ai.maum.mvp_maker.model;

import lombok.Data;

@Data
public class LicenseRequsetDTO {
    private String email;
    private String licenseKey;
}
