package ai.maum.mvp_maker.auth.mapper;

import ai.maum.mvp_maker.auth.model.TokenVO;
import ai.maum.mvp_maker.model.*;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface Auth_Mapper {

    /*
    ** 인증 정보 삭제
    */
    @Delete( "delete from MVP_MAKER_USER where email=#{email}" )
    int deleteAuth(String email);

    /*
    ** 인증 정보 추가
    */
    @Insert( "insert into MVP_MAKER_USER (email, access_token, access_expire_datetime, refresh_token, refresh_expire_datetime, "
            + " create_datetime, update_datetime ) "
            + " values (#{email}, #{access_token}, #{access_expire_time}, #{refresh_token}, #{refresh_expire_time}, "
            + " dateadd(hour, #{db_diff_time}, getdate()), dateadd(hour, #{db_diff_time}, getdate()) )" )
    int insertAuth(TokenVO token);

    /*
    ** 인증 정보 조회
    */

    @Select( "select * from MVP_MAKER_USER where email=#{email}" )
    TokenVO getAuth(String email);
}
