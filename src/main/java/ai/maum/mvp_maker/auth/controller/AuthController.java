package ai.maum.mvp_maker.auth.controller;

import ai.maum.mvp_maker.auth.service.AuthService;
import ai.maum.mvp_maker.model.UserVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.UUID;

@Slf4j
@Controller
public class AuthController {

    @Value("${url.hq}")
    String mUrl_HQ;

    @Value("${sso.client-id}")
    String CLIENT_ID;

    @Value("${sso.scope}")
    String SCOPE;

    @Value("${sso.callback}")
    String OAUTH_CALLBACK_URI;

    @Value("${url.maum-ai}")
    String mUrl_Maum;

    @Value("${url.ai-builder}")
    String mUrl_aiBuilder;

    @Autowired
    private AuthService service;

    /*
    ** 로그인 요청 수신
    **     - 통합 SSO 서버로 로그인 요청
    */
    @RequestMapping(value="/security/login")
    public String login(HttpServletRequest request,
                        HttpServletResponse response) {

        String log_msg = "\n:: @ REQUEST\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/security/login");
        log_msg += String.format(":: %-20s = %s\n", "desc", "로그인 요청");
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        final String RESP_TYPE = "code";

//        final String SSO_SERVER_URI = mUrl_HQ + "/maum/loginMain" + "?"
        final String SSO_SERVER_URI = mUrl_HQ + "/maum/oauthLoginMain" + "?"
                + "response_type=" + RESP_TYPE + "&"
                + "client_id=" + CLIENT_ID + "&"
                + "redirect_uri=" + URLEncoder.encode(mUrl_aiBuilder) ;

        return "redirect:" + SSO_SERVER_URI;
/*
        String state = UUID.randomUUID().toString();
        final String SSO_SERVER_URI = mUrl_HQ + "/hq/oauth/authorize";
        final String RESP_TYPE = "code";

        String state = UUID.randomUUID().toString();
        return "redirect:" + SSO_SERVER_URI + "?"
                + "response_type=" + RESP_TYPE + "&"
                + "client_id=" + CLIENT_ID + "&"
                + "redirect_uri=" + OAUTH_CALLBACK_URI + "&"
                + "scope=" + SCOPE + "&"
                + "state=" + state;
*/
    }

    /*
    ** SSO 로그인 콜백
    */
    @RequestMapping(value = "/security/oauth/callback")
    public String authCallback(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam("code") String code,
            @RequestParam("state") String state
    ) throws Exception {

        String log_msg = "\n:: @ REQUEST\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/security/oauth/callback");
        log_msg += String.format(":: %-20s = %s\n", "desc", "SSO 콜백");
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "code", code);
        log_msg += String.format(":: %-20s = %s\n", "state", state);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        UserVO user = service.getAuthToken(code, state, OAUTH_CALLBACK_URI);
        if(user != null) {
            request.getSession().setAttribute("user", user);
        }
        else {
            request.getSession().removeAttribute("user");
        }

        return "redirect:/";
    }

    /*
     ** 엔진 그룹 목록 조회
     */
    @RequestMapping(value="/security/logout")
    public void logout(HttpServletRequest request,
                        HttpServletResponse response) throws IOException {

        HttpSession session = request.getSession();
        UserVO user = (UserVO) session.getAttribute("user");
        session.removeAttribute("user");

        String deleteTokenUrl = service.makeDeleteTokenUrl(request, user);
        String returnUrl = deleteTokenUrl + "&returnUrl=" + mUrl_aiBuilder;

        service.logoutByUser(user);

        request.getSession().invalidate();
        response.setStatus(HttpServletResponse.SC_OK);
        response.sendRedirect(returnUrl);
    }
}
