package ai.maum.mvp_maker.auth.filter;

import ai.maum.mvp_maker.auth.exif.SSOExternalAPI;
import ai.maum.mvp_maker.common.ConstDef;
import ai.maum.mvp_maker.common.service.UserService;
import ai.maum.mvp_maker.model.UserVO;
import io.jsonwebtoken.*;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final UserService userService;
    private final SSOExternalAPI ssoExternalAPI;
    private final ConstDef constDef;

    @SneakyThrows
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String logString = "doFilterInternal/";
        HttpSession session = httpServletRequest.getSession(true);
        UserVO user = (UserVO) httpServletRequest.getSession().getAttribute("user");

        String accessToken;
        String refreshToken;
        Map<String, String> tokenMap = getCookie(httpServletRequest, httpServletResponse);

        if (tokenMap != null) {
            accessToken = tokenMap.get("accessToken");
            refreshToken = tokenMap.get("refreshToken");
        }
        else {
            accessToken = null;
            refreshToken = null;
        }
//        log.info(logString + "accessToken:" + accessToken);
//        log.info(logString + "refreshToken:" + refreshToken);

        // NOTE. 우선순위 token > session

        if (validateToken(accessToken)) {
            // valid access token
            if (user == null || SecurityContextHolder.getContext().getAuthentication() == null) {
                log.info(logString + "valid access token, invalid session: create session");
                // invalid session: create session
                setSessionAccessUser(accessToken, httpServletRequest);
            }
            else {
                log.info(logString + "valid access token, valid session: do nothing");
                // valid session: do nothing
            }
        }
        else {
            // invalid access token
            if(validateToken(refreshToken)) {
                log.info(logString + "invalid access token: refresh access token");
                // valid refresh token: refresh access token
                accessToken = ssoExternalAPI.refreshJwtAccessToken(refreshToken, httpServletResponse);

                if(validateToken(accessToken)) {
                    log.info(logString + "invalid access token: refresh access token: valid token");
                    // valid access token
                    if (user == null || SecurityContextHolder.getContext().getAuthentication() == null) {
                        log.info(logString + "invalid access token: refresh access token: valid token: create session");
                        // invalid session: create session
                        setSessionAccessUser(accessToken, httpServletRequest);
                    }
                    else {
                        log.info(logString + "invalid access token: refresh access token: valid token: do nothing");
                        // valid session: do nothing
                    }
                }
                else {
                    log.info(logString + "invalid access token: refresh access token: invalid token: logout");
                    // invalid access token: logout
                    session.invalidate();
                }
            }
            else {
                log.info(logString + "invalid access token: invalid refresh token: logout");
                // invalid refresh token: logout
                session.invalidate();
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);

//        if (user == null || SecurityContextHolder.getContext().getAuthentication() == null) {
//            /* --------------------------------- 세션 미존재 --------------------------------- */
//            Map<String, String> tokenMap = getCookie(httpServletRequest, httpServletResponse);
//
//            if (tokenMap == null || "".equalsIgnoreCase(tokenMap.toString())) {             // 최초 접속 시,
//                //System.out.println(" @ @ 최초 접속 !");
//                filterChain.doFilter(httpServletRequest, httpServletResponse);
//            }
//            String accessToken = tokenMap.get("accessToken");
//            String refreshToken = tokenMap.get("refreshToken");
//
//            if (accessToken == null) {
//                if (refreshToken != null) {                              // 세션 X, accessToken X, refreshToken O
//                    if (validateToken(refreshToken)) {
//                        ssoExternalAPI.refreshJwtAccessToken(refreshToken, httpServletResponse);
//                        setSessionAccessUser(refreshToken, httpServletRequest);
//                    } else {
//                        expireSessionAndLogout(httpServletRequest, httpServletResponse);
//                    }
//                } else {                                                // 세션 X, accessToken X, refreshToken X
//                    // 정상적 현상
//                }
//            }
//
//            if (accessToken != null) {
//                if(refreshToken != null) {                              // 세션 X, accessToken O, refreshToken O
//                    if (validateToken(accessToken) && validateToken(refreshToken)) {
//                        setSessionAccessUser(refreshToken, httpServletRequest);
//                    }
//                } else {                                                // 세션 X, accessToken O, refreshToken X
//                    log.warn(" @ JwtAuthenticationFilter: no session, no refreshToken");
//                    expireSessionAndLogout(httpServletRequest, httpServletResponse);
//                }
//            }
//            filterChain.doFilter(httpServletRequest, httpServletResponse);
//
//        } else {
//            /* --------------------------------- 세션 존재 --------------------------------- */
//            Map<String, String> tokenMap = getCookie(httpServletRequest, httpServletResponse);
//            if (tokenMap == null) {
//                filterChain.doFilter(httpServletRequest, httpServletResponse);
//            }
//            String accessToken = tokenMap.get("accessToken");
//            String refreshToken = tokenMap.get("refreshToken");
//
//            if (accessToken == null) {
//                if(refreshToken != null) {                              // 세션 O, accessToken X, refreshToken O
//                    ssoExternalAPI.refreshJwtAccessToken(refreshToken, httpServletResponse);
//                } else {                                                // 세션 O, accessToken X, refreshToken X
//                    session.removeAttribute("user"); // 기존 세션 만료
//                    httpServletResponse.sendRedirect(reqLogout());
//                }
//            }
//
//            if (accessToken != null) {
//                if(refreshToken != null) {                              // 세션 O, accessToken O, refreshToken O
//                    if (validateToken(accessToken) && validateToken(refreshToken)) {
//                        // 정상적 현상
//                    }
//                } else {                                                // 세션 O, accessToken O, refreshToken X
//                    expireSessionAndLogout(httpServletRequest, httpServletResponse);
//                }
//            }
//            filterChain.doFilter(httpServletRequest, httpServletResponse);
//        }
    }

    // Token 기간 만료 확인
    public boolean validateToken(String authToken) {
        
        if(authToken == null || authToken.length() == 0) {
            // 잘못된 토큰인 경우
            return false;
        }
        
        try {
            Jwts.parser().setSigningKey(constDef.getSingKey()).parseClaimsJws(authToken).getBody();
            return true;
        } catch (SignatureException ex) {
            log.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            log.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            log.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            log.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            log.error("JWT claims string is empty.");
        }
        return false;
    }

    // 로그아웃
    public String reqLogout() {
        String url = constDef.getSsoUrl() + constDef.getLogoutPath();
        String params = "?client_id=" + constDef.getClientId() + "&returnUrl=" + constDef.getMyDomain() + "/";

        return url + params;
    }

    // session 만료, 쿠키 만료
    public void expireSessionAndLogout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {

        HttpSession session = httpServletRequest.getSession(true);
        session.removeAttribute("user"); // 기존 세션 만료

        httpServletResponse.sendRedirect(reqLogout());
    }

    // session 설정
    public void setSessionAccessUser(String refreshToken, HttpServletRequest httpServletRequest) throws Exception {

        HttpSession session = httpServletRequest.getSession(true);

        Claims claims = Jwts.parser().setSigningKey(constDef.getSingKey()).parseClaimsJws(refreshToken).getBody();
        String email = claims.getSubject();

        UserVO user = userService.loadUserByUserEmail(email);
        SecurityContext sc = SecurityContextHolder.getContext();
        sc.setAuthentication(new UsernamePasswordAuthenticationToken(email, "", user.getAuthorities()));

        UserVO memberVo = userService.getUser(email);
        session.setAttribute("user", memberVo);
        session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, sc);
    }

    // 쿠키를 통해 token 확인
    public Map<String, String> getCookie(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {

        Map<String, String> result = new HashMap<>();

        Cookie[] cookies = httpServletRequest.getCookies();
        String accessToken;
        String refreshToken;

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equalsIgnoreCase("MAUM_AID")) {
                    accessToken = cookie.getValue();
                    //log.info("@ OAuth.token access_token value: {}", accessToken);

                    result.put("accessToken", accessToken);
                }
                if (cookie.getName().equalsIgnoreCase("MAUM_RID")) {
                    refreshToken = cookie.getValue();
                    //log.info("@ OAuth.token refresh_token value: {}", refreshToken);

                    result.put("refreshToken", refreshToken);
                }
            }
        } else {
            return null;
        }
        return result;
    }
}
