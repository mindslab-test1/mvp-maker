package ai.maum.mvp_maker.auth.service;

import ai.maum.mvp_maker.auth.exif.SSOInterface;
import ai.maum.mvp_maker.auth.mapper.Auth_Mapper;
import ai.maum.mvp_maker.auth.model.TokenVO;
import ai.maum.mvp_maker.editor.mapper.User_Mapper;
import ai.maum.mvp_maker.model.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Service
public class AuthService {
    @Autowired
    Auth_Mapper auth_mapper;

    @Autowired
    User_Mapper user_mapper;

    @Autowired
    SSOInterface ssoIf;

    @Value("${url.ai-builder}")
    private String myDomain;

    @Value("${sso.client-id}")
    String CLIENT_ID;

    @Value("${url.hq}")
    String hqUrl;

    @Value("${hq.logoutPath}")
    String hqCleanToken;

    /*
    ** 인증 서버로부터 AccessToken(유효시간 포함)과 RefreshToken(유효시간 포함)을 발급받는다.
    */
    public UserVO getAuthToken(String code, String state, String requestUri) {

        TokenVO token = ssoIf.publishTokens(code, requestUri);
        if(token != null) {
            auth_mapper.deleteAuth(token.getEmail());
            auth_mapper.insertAuth(token);

            UserVO userVO = user_mapper.getUser(token.getEmail());
            return userVO;
        }
        else return null;
    }

    /*
    ** 인증 서버로부터 AccessToken(유효시간 포함)과 RefreshToken(유효시간 포함)을 재발급받는다.
    */
    public String refreshAuthToken() {
//        TokenVO token = ssoIf.republishTokens(db.mRefreshToken);
//        if(token != null) {
////            db.mUserEmail = token.getEmail();
////            db.mAccessToken = token.getAccess_token();
////            db.mAccessToken_ExpireTime = token.getAccess_expire_time();
////            db.mRefreshToken = token.getRefresh_token();
////            db.mRefreshToken_ExpireTime = token.getRefresh_expire_time();
//            return token.getEmail();
//        }
//        else return null;

        return null;
    }

    /* 토큰 삭제 URL 생성 : AuthController에서 사용 - 2020.08.05 LYJ */
    public String makeDeleteTokenUrl(HttpServletRequest servletRequest, UserVO user){

        HttpSession session = servletRequest.getSession();
        TokenVO token = auth_mapper.getAuth(user.getEmail());

        String url = hqUrl + hqCleanToken;
        System.out.println(url);
        //String params = "?client_id=" + CLIENT_ID + "&access_token=" + token.getAccess_token();
        String params = "?client_id=" + CLIENT_ID;
        String resultUrl = url + params;

        return resultUrl;
    }

    /*
    ** 사용자 요청에 의한 로그아웃 실행
    */

    public void logoutByUser(UserVO user) {
        TokenVO token = auth_mapper.getAuth(user.getEmail());
        if(token == null) return;
        auth_mapper.deleteAuth(user.getEmail());

        ssoIf.cleanTokens(CLIENT_ID, token.getAccess_token());
    }
}
