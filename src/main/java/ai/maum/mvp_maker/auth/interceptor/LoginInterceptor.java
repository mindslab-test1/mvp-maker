package ai.maum.mvp_maker.auth.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Component
public class LoginInterceptor implements HandlerInterceptor {

    @Value("${url.maum-ai}")
    String mUrl_Maum;

    // 맵핑되기 전 처리를 해주면 된다.
    @Override public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
//        log.info("================ Before Method");

        if(request.getSession().getAttribute("user") != null) return true;

//        log.info("# LoginInterceptor >> missing 'user'");
        response.sendRedirect("/landing");
        return false;
    }

    // 맵핑되고난 후 처리를 해주면 된다.
    @Override
    public void postHandle( HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
//        log.info("================ Method Executed");
    }

    // 모든 작업이 완료된 후 실행된다.
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
//        log.info("================ Method Completed");
    }

}
