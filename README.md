# M1 Builder

## Docs
- [confluence](https://pms.maum.ai/confluence/display/AH/%5BM1%5D+Builder+1.0)

## START

- 로컬환경에서 build 시 아래부분 로컬환경에 맞게 설정

```bash
upload:
  dir: c:/upload
```

## Update

- 2021.06.03 <prod 적용>
  - 테이블 변경
  ```text
  SHOPIFY_LICENSE => 삼당형 템플릿 사용량 관련 컬럼 추가
  CONSULT_USAGE_LIMIT (int default 0): 월간 사용가능 횟수
  CONSULT_PERIOD (int default 0): 사용가능 기간
  CONSULT_START_DATE (datetime): 사용이 시작되는 날짜
  ```
  - 테이블 추가
  ```text
  SHOPIFY_CONSULT_USAGE_LOG => 테이블 추가
  ID (int)
  EMAIL (varchar(255))
  AVATAR_ID (int)
  USAGE_DATE (datetime default getdate())
  ```
- 2021.06.09 <prod 적용>
  - 테이블 변경
  ```text
  BuilderAvatar => 컬럼 추가
  publicAvatar (varchar(10) default 'N')    
  ```
- 2021.06.14 <prod 적용>
  - 로우 업데이트
  ```text
  EngineParam
  engineId 38 의 enumValue, displayEnumValue, displayEnumValue_en 추가
  AHB-302
  ```
- 2021.06.22 <prod 적용>
  - BuilderBackground 업데이트
- 2021.06.29
  - BuilderAvatar 추가 lcj, jesong
- 2021.07.02 <prod 적용>
  - 테이블 변경
  ```text
  BuilderAvatar => 컬럼추가
  displayNameEn (varchar(300))
  ```
- 2021.07.02 <prod 적용>
  - 테이블 변경
  ```text
  BuilderBackground => 컬럼추가
  displayNameEn (varchar(300))
  ```
- 2021.07.08 <stg 적용>
  - 테이블 변경
  ```text
  EngineGroupParamValue 업데이트
  1|영어 (8K)|eng,8000,baseline_eng_8k_default|English (8K)
  1|한국어 (8K)|kor,8000,baseline_kor_8k_default|Korean (8K)
  이외에 groupParamId는 2
  ```
